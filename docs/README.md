# Documentation de l'association ICI

Bienvenue sur le site de documentation collaborative de [l'association ICI](https://associationici.fr/).

Cette documentation est en construction, via [ce dépôt Gitlab](https://gitlab.com/association-ici1/documentation) et le panneau d'administration accessible à cette adresse : **[doc.associationici.fr/admin](https://doc.associationici.fr/admin)** (invitation requise).

L'adresse actuelle du site est temporaire et a vocation à changer prochainement (par exemple docs.associationici.fr).

![](/upload/ici_logo.jpg)

## Fonctionnement

La barre de gauche permet de naviguer dans les ressources pédagogiques, avec deux niveaux hiérarchiques : les **collections** et les **pages**.

Au niveau le plus élevé de la hiérarchie se trouvent les **collections**. Il faut imaginer ces groupes de pages comme des dossier dans lequel vous rangerez un ensemble de ressources liées entre elles. Vous retrouverez ces collection également dans le panneau admin. Une flèche permet de dérouler le contenu de chaque collection et d'afficher les pages.

A l'intérieur des collections, les **pages** accueillent le contenu lui-même : texte, image, vidéos, etc. Ces pages sont autant de fichiers écrits au format [Markdown](https://fr.wikipedia.org/wiki/Markdown) qui permet très simplement de composer des ressources riches, ouvertes et évolutives.

[Le panneau admin](https://doc.associationici.fr/admin) du site permet de créer et d'éditer des pages au format Markdown grâce à un éditeur de texte riche en mode "[WYSIWIG](https://fr.wikipedia.org/wiki/What_you_see_is_what_you_get)".

Il est également possible de créer ou de convertir du texte en Markdown grâce à de nombreux autres outils, tels que [Word to Markdown Converter](https://word2md.com/) qui comme son nom l'indique permet de convertir en Markdown (`.md`) des fichiers Microsoft Word (`.doc` ou `.docx`).

## Briques logicielles

Ce site est construit avec le générateur [Vuepress](https://vuepress.vuejs.org/) et hébergé chez [Netlify](https://netlify.com/). L'outil [Netlify CMS](https://netlifycms.org/) complète l'ensemble en offrant une interface similaire au panneau admin Wordpress pour éditer intuitivement le contenu du site.