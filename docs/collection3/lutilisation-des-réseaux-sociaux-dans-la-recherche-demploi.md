---
title: 📽️ L'utilisation des Réseaux Sociaux dans la Recherche d'Emploi
---
# **Utilisation des Réseaux Sociaux dans la recherche d'emploi**

::: tip Module APTIC : 

* *6- Utiliser les RS pour sa recherche d'emploi*
* **Résumé :** Outil de communication incontournable des entreprises aujourd'hui l'utilisation des réseaux sociaux pour la recherche d'emploi reste encore un usage minoritaire. Sans minimiser les risques cet atelier a pour but d'établir une stratégie d'utilisation de ceux-ci cohérente avec le projet professionnel des participants.
* **Nombre d'heures** : 2 heures
* **Publics** : Intermédiaire / confirmé
* **Prérequis**: Maîtrise minimum du Français : lu , écrit, parlé .
  Avoir suivi les modules Réseaux Sociaux, Recherche d'emploi .
* **Objectifs de l'atelier :**

  * Connaître les principales sociologies des RS
  * Connaître l'usage des RS par les entreprises aujourd'hui
  * Élaborer une stratégie de visibilité sur les RS

:::

**Sommaire:**

[[toc]]

## 1.Introduction au sujet

Tout d'abord reprenons ce qu'est un réseau social :

> Définition : Sites ou applications permettent la création et la publication de contenus générés par l'utilisateur et le développement de réseaux  en ligne en connectant les profils des utilisateurs.

Evolution du blog par l'intégration de l'interactivité en temps réel il en existe aujourd'hui plusieurs sortes que l'on peut regrouper par catégories :

* les informationnels comme Twitter où l'on vient y rechercher de l'information
* les discussionnels où l'on y discute avec ces amis : Snapchat, Telegram, Messenger, Whatsapp
* les affinitaires où les internautes se regroupent par passions communes : Pinterest, Discord, Instagram mais aussi Youtube
* les généralistes comme Facebook ou Baidu où l'on peut y faire quasi n'importe quoi (discuter avec ses amis, suivre de l'information, participer à des événements …)

De fait on ne retrouve pas les mêmes personnes, la même sociologie des publics selon tel ou tel réseau social.

![](/upload/sociors.png "Tableau Comparatif RS pour le Business")

![](/upload/quel-est-le-meilleur-réseau-social-2019.png "Tableau Comparatif RS 2019")

Source : VP Strat

Ainsi Snapchat a une sociologie jeune (13-26 ans en moyenne) là où Twitter est plutôt à vocation des CSP+ ou des professionnels de la communication et de l'information.

Il convient donc de savoir à qui l'on veut s'adresser et pourquoi faire.

## 2.L'utilisation des RS par les entreprises aujourd'hui

A partir de 2005/2006 quelques entreprises et administrations publiques commencent à investir dans ses sites comme *The Second Life* (à partir de 2005 aux USA, puis en 2007 en France des candidats à l'élection présidentielle présents dessus) ou Facebook à partir de 2007 en France tout d'abord pour les grandes entreprises et les grands musées nationaux avant de se généraliser.

![](/upload/rsentreprises.png "Utilisation des RS en entreprise- Source Etude LK Conseil 2017")

Force est de constater, 10 ans après que seules 41 % des entreprises ont investies les réseaux sociaux dont une grande majorité dans les très grandes entreprises (70%), principalement aussi dans le secteur de l'hébergement(Trivago …), de la communication et des nouvelles technologies ou encore de l'immobilier (Le Bon Coin …).

Parmi ceux utilisés par les entreprises on note principalement : Facebook, Twitter, Linkedin ; le premier généraliste pour être vu par tous et toutes, le second pour de la veille informationnelle et de la communication virale, le dernier pour la gestion du réseau professionnel.

Et de fait on constate 4 usages majoritaires actuellement des réseaux sociaux par les entreprises :

* pour accroître leur notoriété
* pour gérer leur réputation en ligne
* pour communiquer sur leurs produits
* pour diffuser des annonces (emploi)

Or bien qu'il existe aujourd'hui des métiers dédiés à la gestion des RS dans les entreprises (Community Manager, Content Manager, …) il ne faut pas oublier que la plupart des entreprises et administrations en France restent de tailles modestes et que, par conséquent, la maîtrise ou l'intérêt pour ceux-ci sont des atouts indéniables, que cela soit dans le processus de recrutement ou dans le développement de ses compétences.

## 3.La stratégie salariale d'utilisation des RS

Et de fait, à quoi servent les RS en tant que chercheur d'emploi ?

### 1.A accéder à des offres cachées

De plus en plus d'entreprises boudent Pôle Emploi au profit des RS comme Facebook ou Linkedin. Les annonces sont alors publiées par des salariés ce qui constitue une porte d'entrée dans l'entreprise. Il peut même s'agir d'un test d'entrée pour les candidats par exemple dans les métiers de la communication 

#### Exemple 1 : Les groupes Facebook

Souvent privés, nécessitant l'acceptation d'un modérateur, ils sont un formidable vecteur d'emploi par secteur pour tous les profils (du stage au CDI). Attention toutefois à la gestion de votre image sur ce RS !

![](/upload/gpefacebook.png "Exemple de groupe Facebook Secteur Culture")

#### Exemple 2 : Le hashtag #I4emploi sur Twitter

Dans la barre de recherche de Twitter en tapant ce mot-clef vous aurez accès à des offres d'emploi. Ce hashtag permet aussi de diffuser des offres tout en s'assurant un minimum de visibilité. Il est principalement utilisé dans les métiers de la communication, des nouvelles technologies et dans le milieu de l'intérim.

![](/upload/i4emploi2.png "Résultats hashtag #I4emploi Twitter")

### 2.Booster sa visibilité

A contrario des logiques de suppression de contenus défavorables (droit à l'oubli) ou d'anonymisation sur Internet on peut aussi utiliser les RS pour se mettre en avant, gérer son réseau et son e-réputation de façon proactive.

On peut citer pour cela par exemple Linkedin, le réseau social professionnel créé en 2003 et racheté par Microsoft en 2016 qui permet de rendre visible sur les moteurs de recherche son CV et son réseau.

![](/upload/linkedin.png "Exemple de page Linkedin")

Cf. fiche Technique Linkedin

Attention toutefois à bien maîtriser les paramètres de visibilité des informations remplies.

### 3.Montrer ce que l'on sait faire

Enfin, vous pouvez tout à fait créer du contenu en vue d'une embauche.

Cette stratégie exigeante, nécessitant un investissant important, peut s'avérer payante pour vous démarquer face à d'autres candidats et montrer l'étendue de vos compétences.

#### Exemple 1 : Lancer une chaîne Youtube/Twitch

But : Travailler son aisance à l'oral, les logiciels vidéo, adopter un point de vue.

![](/upload/youtubechannel.png "Exemple de chaine Youtube")

#### Exemple 2 : Réaliser 1 veille professionnelle sur Twitter

But : Trouver des sources d'informations sur un sujet donné, les lire, les trier, adopter un point de vue dessus.

![](/upload/twitterv.png "Exemple de veille professionnelle sur Twitter")

::: danger Attention : 

Bien sûr ce genre de stratégies ne va pas sans risque qu'il s'agisse :

\-de mauvais paramétrages laissant exposer des informations sensibles visibles à tous

\-de mélange entre ce qui relève de la vie privée et de la vie publique

\-de confrontation à des commentaires agressifs voir du cyberharcèlement

\-de discrédit face à des fautes d'orthographes que vous pourriez faire

:::

Exercice: Selon le projet professionnel des partipants vous pourrez avec eux voir comment:

* trouver des groupes d'emploi Facebook ou des annonces sur Twitter
* créer un compte Linkedin et le remplir avec eux (cf.fiche technique)
* créer un compte Twitch/Youtube/Twitter et établir une ligne éditoriale

::: tip Sources

![](RackMultipart20200715-4-f0kljg_html_19969f82a0bf69a6.png)

:::