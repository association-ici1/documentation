---
title: Rappels Markdown
---
# La syntaxe markdown

Pour formater votre texte vous avez la possibilité d'utiliser la barre d'outils située au-dessus de la zone de texte, ou vous pouvez utiliser la syntaxe markdown.

Le bouton à bascule permet de passer de l'éditeur de texte riche ("Rich Text") à l'éditeur de texte brut au format Markdown ("Markdown") :

![](/upload/screenshot-from-2020-06-28-16-06-32.png)

## Styles de texte

Vous pouvez utiliser `_` ou `*` autour d'un mot pour le mettre en italique. Mettez-en deux pour le mettre en gras.

* `_italique_` s'affiche ainsi : *italique*
* `**gras**` s'affiche ainsi : **gras**
* `**_gras-italique_**` s'affiche ainsi : ***gras-italique***
* `~~barré~~` s'affiche ainsi : ~~barré~~

## Liens

Créez un lien intégré en mettant le texte désiré entre crochets et le lien associé entre parenthèses.

`Aidez-vous avec [la documentation de Framasite](https://docs.framasoft.org/fr/grav/) !`

s'affichera :

Aidez-vous avec [la documentation de Framasite](https://docs.framasoft.org/fr/grav/) !

## Images

Utilisez une image en ligne en copiant son adresse (finissant par `.jpg`, `.png`, `.gif` etc…) avec un texte alternatif entre crochets (qui sera affiché si l'image n'apparaît pas) et le lien entre parenthèses. Vous pouvez aussi ajouter un texte qui apparaîtra au survol de la souris grâce aux `"`.

```
![le logo de Framasoft](https://framasoft.org/nav/img/logo.png)
```

donnera :

![le logo de Framasoft](https://framasoft.org/nav/img/logo.png)

## Citation

Les citations se font avec le signe `>` :

```
> Oh la belle prise !
```

> Oh la belle prise !

## Listes

Vous pouvez créer des listes avec les caractères `*` et `-` pour des listes non ordonnées ou avec des nombres pour des listes ordonnées.

Une liste non ordonnée :

```
* une élément
* un autre
 * un sous élément
 * un autre sous élément
* un dernier élément
```

* une élément
* un autre
* un sous élément
* un autre sous élément
* un dernier élément

Une liste ordonnée :

```
1. élément un
2. élément deux
```

1. élément un
2. élément deux

## Titres

Pour faire un titre, vous devez mettre un `#` devant la ligne. Pour faire un titre plus petit, ajoutez un `#` (jusque 6) :

```
# Un grand titre
## Un titre un peu moins grand
### Un titre encore moins grand
```

Vous pouvez également souligner le texte en utilisant `===` ou `---` pour créer des titres.

```
Un grand titre
=============
```

Comme dans un éditeur de texte comme Microsoft Word ou LibreOffice Write, l'ajout de titres avec différents niveaux hiérarchiques vous permet ensuite de générer automatiquement une table des matières (voir **Sommaire** ci-dessous).

## Tableaux

Pour créer un tableau vous devez placer une ligne de tirets (`-`) sous la ligne d'entête et séparer les colonnes avec des `|`. Vous pouvez aussi préciser l'alignement en utilisant des `:`. :

```
| Aligné à gauche  | Centré          | Aligné à droite |
| :--------------- |:---------------:| -----:|
| Aligné à gauche  |   ce texte        |  Aligné à droite |
| Aligné à gauche  | est             |   Aligné à droite |
| Aligné à gauche  | centré          |    Aligné à droite |
```

| Aligné à gauche | Centré   | Aligné à droite |
| --------------- | -------- | --------------- |
| Aligné à gauche | ce texte | Aligné à droite |
| Aligné à gauche | est      | Aligné à droite |
| Aligné à gauche | centré   | Aligné à droite |

## Sommaire

Pour ajouter un sommaire, au début d'une page par exemple, il vous suffit d'ajouter ce code au texte à l'aide de l'éditeur Markdown (pas dans l'éditeur "Rich Text" !) :

`[[toc]]`

La table des matières sera générée automatiquement à partir des titres (voir **Titres** ci-dessus) qui seront affichés en respectant leur ordre hiérarchique. 

Si elle ne s'affiche pas sur la page du site, vérifiez bien dans le texte brut au format Markdown qu'aucun caractère n'est présent devant ou après ce code.

::: danger Attention

Cette fonctionnalité étant capricieuse, merci de ne la rajouter qu'à la fin de votre transposition pour éviter les "/\[[toc]]"

:::

Note : Cette fonctionnalité est une extension au Markdown spécifique à Vuepress, elle ne s'affichera donc pas dans le panneau de prévisualisation à droite [dans l'interface admin](https://doc.associationici.fr/admin/#/) (Netlify CMS) qui sert juste à prévisualiser le Markdown classique.

## Blocs personnalisables

Pour ajouter un conteneur de texte en couleur ou déroulant, il vous suffit d'ajouter un bout de code au texte à l'aide de l'éditeur Markdown (pas dans l'éditeur "Rich Text" !) :

Ce bout de code :
```
::: tip Titre bloc vert
Pour afficher un bloc vert
:::
```

permet d'afficher :
::: tip Titre bloc vert
Pour afficher un bloc vert
:::

Ce bout de code :
```
::: warning Titre bloc orange
Pour afficher un bloc orange
:::
```

permet d'afficher :
::: warning Titre bloc orange
Pour afficher un bloc orange
:::

Ce bout de code :
```
::: danger Titre bloc rouge
Pour afficher un bloc rouge
:::
```

permet d'afficher :
::: danger Titre bloc rouge
Pour afficher un bloc rouge
:::

Ce bout de code :
```
::: work Titre bloc bleu
Pour afficher un bloc bleu
:::
```

permet d'afficher :
::: work Titre bloc bleu
Pour afficher un bloc bleu
:::


Ce bout de code :
```
::: details Cliquez ici
Pour afficher un bloc déroulant
:::
```

permet d'afficher :
::: details Cliquez ici
Pour afficher un bloc déroulant
:::

Note : Cette fonctionnalité est une extension au Markdown spécifique à Vuepress, elle ne s'affichera donc pas dans le panneau de prévisualisation à droite [dans l'interface admin](https://doc.associationici.fr/admin/#/) (Netlify CMS) qui sert juste à prévisualiser le Markdown classique.

## Vidéos

Pour intégrer une vidéo Youtube cliquez sur le bouton Partager en dessous de la vidéo puis sur Intégrer et enfin Copier. 

![](/upload/etapesinserervideoyoutube.png "Etapes Insertion Vidéo Youtube")

Coller le code ainsi copié dans la page que vous souhaitez illustrer de la sorte comme dans l'exemple ci-dessous:

<iframe width="560" height="315" src="https://www.youtube.com/embed/CbIhYhrOJAg" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

---

(Adapté via [Framasoft](https://docs.framasoft.org/fr/grav/markdown.html))