---
title: Recommandations générales
---
Pour une bonne utilisation du [CMS](https://doc.associationici.fr/admin/) :

* Travaillez avec une seule personne à la fois par page. Utilisez la page "workflow" pour vous répartir ce travail. Une fois qu'un enregistrement est fait par une personne, elle doit la mettre en "In review" pour signaler que la nouvelle version est prête à être relue, puis validée si nécessaire, avant la publication.
* Limitez autant que possible la longueur des pages. La page en question (https://doc.associationici.fr/collection4/word.html) est beaucoup trop longue et lourde. Elle devrait impérativement être divisée en plusieurs pages. Cela sera plus simple à suivre pour les lecteurs. Par ailleurs, les pages seront plus légères à charger et surtout vous éviterez les conflits dus au fait que plusieurs personnes essaient de modifier la même page en même temps.
* Nettoyez les pages avant de les enregistrer dans le CMS et gardez seulement les lettres utiles en enlevant tous les caractères inutiles avec la fonction chercher et remplacer : antislash, espaces redondants, etc. Tout cela peut gêner le fonctionnement et alourdit les pages inutilement.
* Utilisez la même mise en forme sur chaque page. Actuellement certaines pages utilisent des numéros, ou des lettres pour hierarchiser les titres et je vous recommande de les enlever car ils n'apportent rien au lecteur. Ce qui a du sens sur un document papier en a beaucoup moins sur une page web où l'on navigue avec des liens hypertexte, non pas avec des numéros de pages ou de titres.
* Ne mettez pas les titres en majuscule, le mélange avec les autres titres rend l'ensemble peu cohérent et peu lisible (par exemple ici : https://doc.associationici.fr/collection4/word.html).
* Inspirez-vous dans votre travail rédactionnel de sites de documentation comme celui-ci : https://docs.framasoft.org/, qui illustre parfaitement toutes ces recommendations.