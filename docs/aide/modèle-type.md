---
title: Modèle Type
---
# Titre de l'atelier

::: tip Modules APTIC

* *XX : Module APTIC correspondant en italique*
* *XX : Module APTIC correspondant en italique*
* *XX : Module APTIC correspondant en italique*

• **Résumé :**

Blabla.

• **Nombre d’heures :** X heures

• **Publics :** Débutant / intermédiaire / confirmé / GEN

• **Prérequis:** Maîtrise minimum du Français : lu , écrit, parlé. 

Avoir suivi les modules XX.

• **Objectifs de l’atelier**

* Objectif 1
* Objectif 2
* Objectif 3

:::

**Sommaire :**

[[toc]]

## **1. Sous-titre 1**

Objectifs intermédiaires : XXX.

Définition:

> Ceci est une définition.

### **1.1 Sous-sous-tire 1**

::: warning Intermédiaires et confirmés

## **2. Sous-titre 2**

Blabla

:::

## **3. Sous-titre 3**

Blabla.

![Image d'un serveur](/upload/cloud1.jpg "Un serveur")

::: danger Attention
On n'oublie pas !
:::

## **4. Sous-titre 4**

Blabla.

## **5. Sous-titre 5**

Blabla.

::: work Exercice:

 <https://learningapps.org/watch?v=p4yb1kafj19>

:::

::: tip Sources

<https://www.google.fr>

:::