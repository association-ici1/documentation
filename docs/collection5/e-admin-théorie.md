---
title: "🗄️ E-admin théorie "
---
# E-ADMIN THÉORIE

::: tip Modules APTIC 

* *Aucune.*
* **Résumé** 

A rédiger

* **Nombre d’heures du module** : 3h00
* **Public** : Module  Pro
* **Prérequis** : Maîtrise du Français lu et parlé / Possession d’un ordinateur ou d’un smartphone / Posséder une adresse mail / Avoir un accès internet ainsi que tous les identifiants des applications concernées.
* **Objectifs de l’atelier** :

  * Comprendre ce qu'est l'E-administration
  * Connaître le plan d'Action publique 2022
  * Connaître les avancées de la dématérialisation
  * Connaître les démarches réalisables en ligne
  * Initiation au site du Val-de-Marne

:::

**Sommaire:**

[[toc]]

## 1. Introduction à l'e-administration

### **1.1 Définitions et notions à comprendre :**

Depuis l’avènement des ordinateurs, les administrations publiques sont de grandes consommatrices de technologies de l’information et de la communication (TIC). A l’instar des entreprises privées, les premières applications informatiques ont été l’automatisation des tâches de traitement de masse utilisant ainsi la puissance de calcul des gros ordinateurs centraux. Puis l’évolution technologique a permis de passer de l’informatique de gestion aux systèmes d’information, véritable support aux activités des métiers. Désormais, depuis une décennie, les administrations ont recours aux technologies basées sur l’Internet.

**L' e-gouvernement** (ou gouvernement électronique) est un concept qui est apparu dans les administrations publiques vers la fin des années 80. L'e-gouvernement est définit par la communauté européenne comme « l'utilisation des technologies de l'information et des communications (TIC) dans les administrations publiques, associée à des changements au niveau de l'organisation et de nouvelles aptitudes du personnel. L'objectif est d'améliorer les services publics, renforcer les processus démocratiques et de soutien aux politiques publiques.»

Clairement, le e-gouvernement ne consiste pas seulement à mettre les prestations de service existantes sur l’Internet. Il ne s'agit pas du gouvernement « traditionnel » auquel on aurait rajouté l'Internet mais d'un processus radical de changement de la manière dont l'État travaille et communique.

**Le e-gouvernement se décompose en 3 grands domaines** :

1. *la e-administration* (synonymes : administration en ligne, administration électronique cyber administration) qui est en fait l'application du e-gouvernement dans sa relation avec les citoyens et les entreprises en tant qu'administrés. C'est le domaine de la prestation électronique de service qui consiste à offrir aux administrés citoyens et entreprises la possibilité de procéder en ligne à leurs transactions avec l'administration publique.
2. *la e-démocratie* (synonymes : démocratie en ligne, cyber démocratie) est l'axe du e-gouvernement qui développe la relation avec le citoyen en tant qu'acteur politique. C'est le domaine du e-voting (ou vote électronique), mais aussi des forums de discussion pour permettre aux citoyens d'échanger et d'enrichir les débats politiques.
3. *la e-société* est l'axe de développement des technologies de l'information et de la communication dans la société.

![](/upload/grand-domaine-du-e-gouvernement.png "Les 3 domaines du gouvernement en ligne")

Notre module s’intéressera lui à la notion ***d’e-administration***.

### **1.2 La notion d’E-administration**

La plupart des pays francophones utilisent le terme d’administration électronique notamment la France avec son programme ADELE pour Administration Electronique , d’autres pays parlent d’administration en ligne tandis que par exemple la Confédération Helvétique utilise plutôt le terme de cyberadministration.

***Selon l’OCDE, la e-administration*** est "l’usage des technologies de l’information et de la communication et en particulier de l’internet en tant qu’outils visant à mettre en place une administration de meilleure qualité".

**L’administration électronique** peut être définie comme « l’utilisation des techniques de l’information et de la communication (TIC), et en particulier d’Internet, dans le but d’améliorer la gestion des affaires publiques » (L’administration électronique : un impératif. Rapport OCDE 2003

Les premiers services télématiques voient le jour à la fin des années 1980 en France grâce au Minitel et les administrations participent au développement d’Internet dès la seconde moitié des années 1990. Mais ce n’est qu’à partir de 1997 que l’administration électronique émerge progressivement en tant que politique publique à part entière et que s’élabore une stratégie globale.

Profitons d’un temps pour contextualiser la notion avec son processus de mis en place.

### **1.3 Petite histoire de l’e-admin en France**

Le programme Action publique 2022 lancé par le gouvernement à l’automne 2017 entend donner un nouvel élan à la transformation numérique de l’administration mais est-ce réellement une révolution ?

***L’e-administration est en réalité un plan français au déploiement constant depuis plus de vingt ans.***

* En effet, depuis 1998, les pouvoirs publics ont élaboré plusieurs programmes ou plans en vue de développer l’administration électronique. Ce mouvement a débuté avec le **programme d’action gouvernemental pour la société de l’information (PAGSI)**. Il a notamment débouché sur l’adoption par les ministères de programmes pluriannuels de modernisation (PPM) et sur la création en 2000 du portail de l’administration, *<https://www.service-public.fr/>*

La politique poursuivie vise à faire de l’État un acteur exemplaire et un accélérateur, plus transparent et plus efficace, en facilitant la diffusion en ligne des informations publiques essentielles et en généralisant les téléprocédures. Il s’agit de mettre en place une « administration à accès pluriel » pour les usagers français (guichets physiques, courriers, services en ligne ou téléphonie).

* Ce mouvement de modernisation s’est poursuivi avec le **plan Administration Électronique (ADELE)** sur la période 2004-2007. La finalité de ce plan, doté d’un budget de 1,8 milliard d’euros, conséquent pour la période, est de faire de l’administration électronique un levier de la modernisation de l’État.
* En 2008, le **plan "France numérique 2012"** prend le relais d’ADELE en matière d’administration électronique. Il a notamment pour but d'accroître l’accessibilité des sites Internet publics, de développer le paiement en ligne, d'améliorer l’interopérabilité entre administrations et d'ouvrir les données publiques (open data). Selon un bilan présenté en 2011, le plan "France numérique 2012" a permis la dématérialisation de 76% des procédures les plus attendues par les usagers. Un référentiel général d’interopérabilité (RGI) est publié en 2009 et valorise les standards ouverts. Quant à la politique d'ouverture des données, elle se concrétise par la création fin 2011 de la plateforme de données publiques, *data.gouv.fr*, développée par la mission Etalab.
* En 2012, le **Secrétariat général à la modernisation de l’action publique (SGMAP)** est institué. Il est chargé de mettre en œuvre la politique de modernisation de l’État, notamment en matière numérique.
* Fin 2015, les usagers se voient proposer un nouveau service numérique : celui de saisir par voie électronique (SVE), dans les mêmes conditions qu’une saisine postale, les administrations d’État pour près de 9 démarches administratives sur 10. Cette saisine peut être effectuée par le biais d’une téléprocédure, d’un formulaire de contact ou par courriel.
* En 2016, **France Connect** est déployé. Cet outil permet d’utiliser un compte, un identifiant et un mot de passe uniques pour tous les services publics en ligne (impôts, caisse d’allocations familiales, mairie, etc.). La refonte du site **[Service-Public.fr](https://www.service-public.fr/)** a également lieu.
* En 2017, le **plan "Préfectures nouvelle génération" (PPNG)** est mis en œuvre. Les procédures de délivrance des titres (demande de permis de conduire ou de carte grise, pré-demande de passeport ou carte d’identité) sont dématérialisées. La réforme repose sur l’Agence nationale des titres sécurisés (ANTS) et les nouveaux centres d’expertise et de ressources des titres (CERT) répartis sur tout le territoire. La mise en place de la téléprocédure pour obtenir sa carte grise a cependant rencontré de nombreuses difficultés qui ont provoqué d’importants retards dans la délivrance des titres.
* Aujourd’hui le programme **Action publique 2022**, programme de réforme de l’État lancé par Édouard Philippe, Premier ministre, le 13 octobre 2017, reprend pour priorité la transformation numérique des administrations avec pour objectif le tout numérique administratif.

### 1.4 **La E-administration : Pourquoi cette mise en place ?**

  La e-administration doit permettre aux usagers de ne plus se déplacer de guichet en guichet. Elle doit centraliser et coordonner les procédures électroniques des différents ministères afin que celles-ci puissent être traitées de la même façon quelle que soit leur origine.

  Du point de vue de l’usager, l’administration devrait apparaître comme une seule organisation unifiée et cohérente.

  En outre, elle doit permettre aux différentes administrations de se transmettre des informations concernant un citoyen, avec l’accord et sous le contrôle de celui-ci et dans le respect du cadre légal de la protection de la sphère privée.

#### 1.4.1 **Où la mettre en place ?**

  L’administration électronique ne se limite pas à Internet comme nous pouvons le penser. Afin de la mettre en place, il faut certes développer des services électroniques qui soient accessibles par Internet , mais aussi par d’autres canaux, qu’il s’agisse d’une borne interactive (mairie ou les impôts par exemple), d’un papier, d’un téléphone ou par l’intermédiaire d’un agent public.

![](/upload/point-numérique-en-sous-préfecture.jpg "Exemple de lieu de médiation numérique")

### 1.5 **La e-administration permet de travailler plus efficacement pour l’administration**

  Comme en témoignent de nombreux pays de l’OCDE, la performance de la gestion publique est au cœur des préoccupations des réformes de la gestion publique.

  Les prestations électroniques de services mises à disposition sur Internet permettent aux usagers de saisir leurs données, de les contrôler et de les transmettre à l’administration. Ce canal est source de gain de productivité et la qualité des données s’en trouve améliorée. Au sein de l’administration, l’efficience sera accrue en réduisant les multiples collectes de la même information auprès du même usager.

  **Pourtant malgré l’attestation officielle il semblerait que la e-administration n’ait pas uniquement des côtés positifs. Creusons cette position.**

#### **1.5.1  Les limites de cette notion**

  En effet, malgré ses côtés séduisants, la e-administration, c’est également plusieurs aspects défavorables à :

* **Déshumanisation du contact avec les administrations** : aujourd’hui, toutes les administrations se sont dotées de ces moyens de communication modernes, en phase avec l’évolution de la société. S’il ne faut pas nier certains avantages, comme la possibilité d’effectuer des démarches sans se déplacer ou d’obtenir une réponse plus rapide, cette modernisation parfois excessive a aussi conduit, dans certains cas, à une déshumanisation du service public.
* **Exclusion par le numérique des publics les plus précaires**, âgés, handicapés ou illettrés accentuée par des démarches accessibles uniquement en ligne (ex: demande de logement social). Problème du non-recours au droit
* **Manque de transparence des algorithmes** (type APB) menant à des discriminations
* **Privatisation du secteur public**: Ex: Multiplication des start-up d’Etat et des entrepreneurs d’intérêt général pour développer dans des temps courts des ‘services publics innovants. De plus une étude menée pour le gouvernement montre que 70 % des effectifs de fonctionnaires pourraient voir leurs métiers changer profondément avec la montée en puissance du numérique. Tous les secteurs sont concernés, des enseignants aux policiers.

#### 1.5.2 **Et l’usager dans tout cela ?**

  Il est possible d’affirmer que la mise en place de la e-administration améliore la qualité des services cependant cette affirmation ne prend pas en compte la réalité du terrain souvent bien différente de la théorie bureaucratique énoncée ci-dessus.

  Le point essentiel des programmes de réforme et de modernisation de l’administration est une approche centrée sur le citoyen plutôt que sur l’administration vous dira-t-on, cependant le perdant à court et moyen terme se trouvera toujours le même : le citoyen dit lambda qui perdra ses repères et subira une nouvelle situation qu’il n’a pas souhaitée ni même anticipée. Quid de ceux qui ne maîtrisent pas les appareils nécessaires à l’application de ce grand changement ? Quid de ceux qui ne disposent pas du matériel chez eux ou près de chez eux ? Quid de ceux qui n’ont jamais voulu utiliser ces matériels ?

![](/upload/taux-équipement.jpg "Taux d'équipement en smartphone selon l'âge et le lieu de vie")

### 1.6 **Focus sur la notion d’illectronisme**

  C'est un chiffre alarmant. En décembre 2017, le gouvernement lançait une « stratégie nationale d’inclusion numérique », destinée à venir en aide aux laissés-pour-compte du digital.

  Face à un État qui se veut toujours plus connecté, les citoyens devaient pouvoir être sur la même ligne. Cependant comme évoqué précédemment ce n’est pas le cas. Avant l’été, le Syndicat de la Presse Sociale (SPS) publie un livre blanc sur « l'illectronisme » où nous y apprenons des faits importants. Aujourd’hui l’illectronisme concerne aujourd'hui 11 millions de Français, soit 23 % de la population.

  Cette dénomination recouvre évidemment plusieurs réalités :

* ***Les plus âgés*** : 27 % des 60 ans n'utilisent jamais Internet, ce chiffre passe à 42 % pour les plus de 80 ans.
* ***Les plus jeunes*** : situation protéiforme : Pourtant nés avec ces nouvelles technologies et très à l'aise sur certaines applications, réseaux sociaux ou consoles de jeu, ils se trouvent désemparés dès que l'utilisation devient moins ludique.
* ***Les adultes non diplômés*** : Un autre facteur d'exclusion numérique est le niveau d'études. Ainsi, les adultes peu ou pas diplômés vont également rencontrer des difficultés à se servir des outils, même si encore "près d'un adulte sur cinq n'utilise pas d'outil numérique ou abandonne en cas de difficulté", toujours selon le rapport.
* ***Les habitants de zones blanches*** : La fracture numérique est aussi une histoire de géographie et d'accès aux outils. En France, encore 541 communes sont dépourvues de toute connexion internet et mobile, selon le Défenseur des droits, Jacques Toubon, ce qui représente 500 000 personnes situées dans ces zones blanches.
* ***Les SDF*** : Selon une étude sur la précarité numérique, datée de 2018, 91% des sans-abri interrogés possèdent un téléphone et 71% ont un smartphone. Elle note toutefois une réelle "hétérogénéité en termes d'accès et d'utilisation des outils numériques". Les personnes SDF sont confrontées à diverses problématiques qui rendent plus difficile leur accès au numérique, notamment en ce qui concerne un accès Internet limité, des difficultés de rechargements, des vols d'appareils... Le manque de ressources et l'absence de domiciliation ne leur permettent pas toujours de bénéficier d'un abonnement mobile

  ***Les personnes illettrées***, qui représentent 7 % de la population française, sont aussi touchées.

![](/upload/illectronisme.jpg "Infographie sociologie de l'illectronisme")

  De plus, près d'un tiers des Français (32%), que l'étude nomme "abandonnistes", déclarent avoir déjà renoncé, dans les douze derniers mois, à faire quelque chose parce qu'il fallait utiliser internet.

  Ces personnes, qui se retrouvent dans toutes les catégories de la population (de façon équivalente quels que soient le sexe, la catégorie socio-professionnelle, en ville ou à la campagne...), disent renoncer principalement à une démarche liée aux loisirs (55%), mais aussi à des démarches administratives (39%).

  **Une lueur d’espoir ?**

  Quelles sont donc les solutions envisageables face à un problème qui possède des racines autant technologiques que sociales ? Plusieurs pistes sont à l'étude, mais c'est bien du côté du smartphone que les principaux espoirs semblent résider, notamment pour les services publics. De Plus, sur son site, l’association Emmaus Connect met aussi en avant l'exclusion numérique des SDF. Selon une étude sur la précarité numérique des personnes sans domicile fixe, datée de 2018, 91% des sans-abri interrogés possèdent un téléphone et 71% ont un smartphone. Elle note toutefois une réelle "hétérogénéité en termes d'accès et d'utilisation des outils numériques".

  "Les personnes SDF sont confrontées à diverses problématiques qui rendent plus difficile leur accès au numérique, notamment en ce qui concerne un accès Internet limité, des difficultés de rechargements, des vols d'appareils... Le manque de ressources et l'absence de domiciliation ne leur permettent pas toujours de bénéficier d'un abonnement mobile", souligne l'étude.

  D’après le rapport du Syndicat de la Presse Sociale, l’administration va mettre l’accent sur l'intelligence artificielle et les interactions vocales avec comme but d’ici les prochaines années de remplacer la saisie des données par la possibilité de la dictée vocale.

  « On peut imaginer que, d'ici trois ans, une personne qui ne sait pas lire ou qui ne peut plus taper sur son ordinateur pourra prendre un rendez-vous avec sa CAF ou déclarer ses revenus en ligne en parlant à son ordinateur ou son smartphone ».

  ***Pour conclure cette partie d’introduction*** sur l’e-administration française, trois aspects sont à prendre en compte et ont été largement sous-estimés et pourtant largement interconnectés. Il n’y aura pas :

* d’automatisation efficace sans numérisation ;
* de numérisation efficace sans révision, donc simplification des procédures ;
* de simplification si on ne prend pas en compte les contraintes et les changements inhérents à la numérisation et à l’automatisation.

### 1.7 **Focus sur les équipements des français**

#### **1.7.1 Le smartphone, grand gagnant des équipements**

  Dans la course effrénée des ménages aux équipements numériques qui voit chaque année fleurir de nouvelles possibilités (tablettes, montres connectées, et depuis peu enceintes intelligentes), le smartphone s’impose aujourd’hui comme l’équipement de référence. En 2018, 3 personnes sur 4 ont l’usage d’un smartphone (75%, + 2 points). L’accès au réseau 4G devient de plus en plus aisé : 61% des équipés mobiles se connectent à internet par ce biais. La courbe de progression de l’accès 4G est fulgurante : le taux a progressé de +19 points en deux ans, +47 points en quatre ans.

  Probablement en lien avec la plus grande rapidité de débit offerte par la 4G, le smartphone s’installe petit à petit comme le mode d’accès principal à internet (46%, +4 points par rapport à l’an dernier), grignotant encore cette année du terrain à l’ordinateur (35%, -3 points). Le smartphone est l’équipement le plus souvent utilisé pour accéder à internet - Champ : ensemble de la population de 12 ans et plus, en % **\-**

![](/upload/taux-équipement.png "Avec quel appareil les gens se connectent-ils à Internet?")

#### 1.7.2 **Un palier atteint par les autres équipements** ?

  S’agissant de l’ordinateur à domicile, le taux d’équipement baisse de 3 points cette année et s’établit à 78%. Le nombre de personnes disposant de plusieurs ordinateurs diminue (30%, - 3 points par rapport à 2017). Et, situation inédite, le nombre de personnes disposant d’une tablette diminue également (41%, contre 44% l’an dernier). Ces évolutions sont confortées par d’autres sources, notamment l’INSEE qui indique que « la 78% des voyageurs en 2018 69% des voyageurs en 2017 +9 points en un an . Au contraire, la consommation en appareils électroniques et informatiques se replie après une forte croissance en 2016 ».

![](/upload/taux-équipement-2.png "Taux d'équipement en ordinateur et tablettes de 1991 à nos jours")

  Dans la façon de se connecter à domicile, les connexions par ordinateur via une connexion fixe restent les plus pratiquées mais marquent le pas (75%, - 3 points), tandis que les connexions sur téléphone ou tablette via le wifi se maintiennent à 64%. Seules les connexions sur le réseau mobile continuent à progresser (55%, + 4 points en un an).

### **1.8 L’Union européenne et l’e-administration**

#### **1.8.1 Le cas de l’Estonie, championne d’Europe de la transformation numérique**

**Ne dites pas Estonie, mais plutôt e-Estonie**

SUPERFICIE : 45 227 km² (Eurostat - 2013)

POPULATION : 1,32 million (Eurostat - 2019)

![](/upload/carte-ue.jpg "Carte de l'Union Européenne")

Il y a un peu plus de deux ans, le 4 juillet 2017, devant l'Assemblée nationale, le Premier ministre Edouard Philippe déclarait : « *Fixons-nous un objectif simple : avoir des services publics numériques de même qualité que ceux du secteur marchand \[…] J'étais, avec un certain nombre de membres du gouvernement, en Estonie, la semaine dernière, eux l'ont fait. »*

La référence à l'Estonie, ancienne République soviétique de 1,3 million d'habitants, a, à première vue, de quoi surprendre. Pourtant, ce pays est effectivement parvenu à développer un modèle d'e-gouvernement aujourd'hui approuvé par tous. Explications :

#### **1.8.2 Quelle numérisation des services publics en Estonie ?**

**"En Estonie, on peut tout faire en ligne depuis sa maison, sauf se marier"** *Katrin Nyman-Metcalf, directrice de recherche à l’e-government academy (EGA) de l’Estonie, un think tank spécialisé dans la transformation numérique*

Très tôt après l'effondrement du bloc soviétique, les pouvoirs publics estoniens ont souhaité acquérir un avantage comparatif par l'utilisation des technologies de l'information et de la communication. Ce fut chose faite au moyen de politiques publiques volontaristes.

Cela a favorisé l'émergence d'un État plate-forme ouvert aux différents acteurs économiques et soucieux d'assurer tant les libertés des individus que l'efficacité de son administration.

Grâce aux cartes d'identité électroniques (adoptées par 98 % de la population), ***Estoniens comme résidents peuvent accéder à leurs ordonnances médicales, utiliser la signature électronique, enregistrer leur société en 18 minutes, ou encore voter en ligne***. Enfin, la mise en place d'un système d'échange d'informations décentralisé, le X-Road, permet aux individus de contrôler l'utilisation de leurs données sans pour autant obérer l'action publique.

Depuis la mise en place en 2014 de son programme « e-résident » qui confère l'accès à la plupart des e-services estoniens depuis l'étranger, l'Estonie se plaît à se présenter comme une « nation digitale » dans un « Etat sans frontière ».

La réussite du modèle d'e-gouvernement estonien est patente. Non seulement cet État balte peut se targuer d'être une Silicon Valley européenne avec un écosystème de start-up des plus attractifs, mais il se positionne aussi comme expert, initiateur de normes sur les scènes européenne et internationale comme l'ont démontré la présidence estonienne du Conseil de l'Union européenne ou la rédaction du « Manuel de Tallinn », qui propose une application du droit international aux cyber-conflits.

Aujourd'hui, l'exportation du modèle et du savoir-faire estoniens bat son plein et les visites de gouvernements en Estonie se multiplient. La France ne fait pas exception à la règle et la majorité actuelle entend pleinement s'inspirer de cette réussite tout en l’adaptant aux spécificités françaises. Un accord renforçant la coopération dans le numérique entre les deux États a d'ailleurs été signé le 19 mars 2018.

***La contextualisation du module réalisée, le but est ici de rentrer dans l’application de l’e-administration à la française, montrer comment elle s’est réalisée jusqu’à aujourd’hui.***

## 2. Initiation à l’E-administration

### 2.1 Le programme "Action Publique 2022"

Édouard Philippe a lancé le 13 octobre 2017, le programme "Action Publique 2022" aux côtés de Gérald Darmanin (Ministre des comptes publics) et de Mounir Mahjoubi ( ancien secrétaire d’État chargé du Numérique). Ce programme ambitieux vise à repenser le modèle de l'action publique en interrogeant en profondeur les métiers et les modes d’action publique au regard de la révolution numérique qui redéfinit les contours de notre société.

#### 2.1.1 Pourquoi ?

La transformation de l'action publique est présenté comme « ***un impératif pour répondre aux transformations profondes qui traversent notre société*** » et qui bouleversent les métiers et les modes d’action publique : l’irruption du numérique, le développement des nouveaux usages collaboratifs, l’évolution des attentes des usagers vers plus de proximité, le développement et l'usage de l’intelligence artificielle, l'optimisation de l'utilisation des données de masses.

Avec le lancement du programme "Action Publique 2022", en octobre 2017, le gouvernement français a pour objectif de bâtir un nouveau modèle de conduite des politiques publiques qui prend en compte la révolution digitale et ses nouveaux usages. Les agents publics, les usagers des services publics, les contribuables doivent tous bénéficier de cette transformation numérique de l'administration.

Le programme Action publique 2022 *<https://www.modernisation.gouv.fr/action-publique-2022/comprendre/action-publique-2022-un-programme-pour-accelerer-la-transformation-du-service-public>*

* **Étape** Quelles sont les avancées de la dématérialisation (situation à juin 2019 dernière mise-à-jour) / Quelles démarches en ligne ? *<https://observatoire.numerique.gouv.fr/observatoire/>*

Cette étape consiste à montrer de manière concrète aux apprenants le tableau de bord des démarches dématérialisées de l’État français.

Cet observatoire de la qualité des services numériques, qui a vu le jour en 2018, ne se contente pas de livrer une statistique générale. Chaque démarche est détaillée : est-elle réalisable entièrement en ligne ? L’identification France Connect est-elle sollicitée ? Peut-on la faire sur mobile ? Le support de l’internaute est-il au niveau (c’est-à-dire avec deux moyens de contact, dont un implique un correspondant humain) ? Quel sont le degré de disponibilité et le temps de réponse ?

Enfin, l’observatoire affiche aussi un indice de satisfaction des usagers. Celui-ci est généré en agrégeant les appréciations des internautes lorsqu’ils achèvent leur démarche. Ainsi, il sera possible de savoir si la procédure est appréciée des administrés, une information qui sera en réalité surtout utile à l’administration elle-même pour repérer les procédures qui ne vont pas.

Cette étape consiste autant en la lecture du tableau que de la participation des apprenants à leurs futurs démarches.

![](/upload/démarche.png "Visualisation de l'observatoire de la dématérialisation")

### 2.2 Comment se repérer dans les démarches administratives

**Site généraliste des démarches administratives : *<https://www.service-public.fr/>***

#### 2.2.1 Qu’est-ce que le site service-public.fr ?

Service-public.fr est le site officiel de l'administration française. Il constitue le pôle multicanal de renseignements administratifs. C’est également un guichet à distance d’information administrative et d’accès aux démarches en ligne.

***Service-public.fr propose 6 grandes rubriques*** :

* **Actualités :** articles quotidiens liés à l’actualité des droits et démarches indexés par thèmes et par public, focus sur une actualité particulière, agenda des démarches à effectuer au cours du mois
* **Vos droits et démarches :** organisé en 10 thèmes, ce guide pratique rassemble les ressources utiles pour connaître ses droits et ses obligations et effectuer ses démarches administratives (fiches d’information sur un droit et la démarche à entreprendre, démarche en ligne ou formulaire, questions-réponses, textes légaux de référence, définitions des termes administratifs, compléments d’information sur d’autres sites internet, coordonnées et heures d’ouverture du service administratif compétent dans la localité de l’usager pour le renseigner ou pour traiter sa démarche administrative affichée lorsque l’internaute saisit le code postal ou le nom de sa commune
* **Comment faire si... :** accès à 9 dossiers pratiques récapitulant les démarches à effectuer en cas de changement de situation
* **Annuaire de l’administration :** coordonnées de 11 000 services nationaux, 70 000 services publics locaux et 14 000 responsables, un accès direct aux mairies, aux ambassades de France à l’étranger et aux institutions européennes
* **Poser une question :** pour entrer en contact avec l’administration par courrier électronique, par téléphone avec «Allo Service-Public 3939» et possibilité de consulter les 300 questions fréquemment posées par les usagers ainsi que les réponses apportées par service-public.fr
* **Services en ligne et formulaires :** accès aux téléprocédures, formulaires en ligne et modules de calcul permettant de simuler un montant à percevoir ou à payer.

Le rôle de l’animateur est ici de présenter le site et de mettre en avant les services importants du site tels que le service en ligne et formulaires comme détaillés ci-dessus.

![](/upload/service-public.png "Le site Service-Public.fr")

#### 2.2.2 Qu’est-ce que mes aides.gouv.fr ?

**Site pour connaître les différentes aides : <https://mes-aides.gouv.fr/>**

*"Avec ce simulateur, notre ambition est d’accompagner plus efficacement chacun dans l’activation de ses droits. Il s’agit à la fois d’informer le grand public qu’il existe un outil facile d’utilisation, rapide, efficace et anonyme, qui permet, sans aucune démarche administrative, d’avoir une idée précise des aides existantes, mais aussi de donner aux professionnels du travail social les outils pour leur permettre d’engager un dialogue constructif avec les personnes suivies afin de les orienter au mieux vers les aides auxquelles elles ont droit" a résumé **la secrétaire d’État chargée des Personnes handicapées et de la Lutte contre l’exclusion**.*

Ce site vous propose de simuler vos droits à plus d’une vingtaine de prestations sociales en quelques minutes. Le site compare votre situation personnelle aux critères des principales aides nationales, comme le RSA, la CMU-C ou les allocations familiales, ainsi que de certaines aides locales.

Grâce aux informations transmises, le site estime à quelles aides vous avez droit ainsi que leurs montants et conditions d'attribution. Vous pouvez ensuite engager les démarches auprès des organismes ciblés. Une liste non exhaustive est présente ci-dessous :

* **Minima sociaux** : le Revenu de solidarité active (RSA), l’Allocation spécifique de solidarité (ASS), l’Allocation de solidarité aux personnes âgées (ASPA), l’Allocation supplémentaire d’invalidité (ASI), l’Allocation aux adultes handicapés (AAH).
* **Prime d’activité**
* **Prestations maladie** : la Couverture maladie universelle complémentaire (CMU-C), l’Aide au paiement d’une complémentaire santé (ACS).
* **Prestations familiales** : les Allocations familiales, l’Allocation soutien familial, le Complément familial, l’Allocation de base de la prestation d’accueil du jeune enfant.
* **Aides au logement** : l’Allocation de logement sociale, l’Allocation de logement familiale, l’Aide personnalisée au logement.

***Le simulateur demande des renseignements sur 7 paramètres : Son identité, ses enfants, son conjoint, le logement, les ressources et pensions alimentaires et enfin le résultat ( comme notifié sur la capture d’écran ci -dessous:*** 

![](/upload/simulateur.png "Paramètres du simulateur Mes-aides.gouv.fr")

![](/upload/evaluer-vos-droits.jpg "Le site Mes-aides.gouv.fr")

*Note* : *Le but du formateur sera de présenter le site et de montrer comment le simulateur fonctionne tout en prenant en compte le fait qu’il ne peut pas faire à la place de la personne.*

#### 2.2.3 Qu’est-ce que le site [Oups.gouv] (https://www.oups.gouv.fr/)?

*Le but de la démarche ici est d’introduire la notion de droit à l’erreur dans les démarches administratives.*

Qu’est-ce que la notion de droit à l’erreur ?

**Définition**

Le principe du droit à l’erreur repose sur un a priori de bonne foi et atteste de la possibilité pour chaque Français de se tromper dans ses déclarations à l’administration, sans risquer une sanction dès le premier manquement.

Chaque usager, particulier ou entreprise, peut alors rectifier, spontanément ou à la demande de l’administration, son erreur lorsque celle-ci est commise de bonne foi et pour la première fois.

Le droit à l’erreur s’inscrit ainsi dans une démarche globale visant à impulser une véritable relation de confiance entre le service public et les usagers autour des principes de bienveillance, de proactivité, de transparence et d’accessibilité.

Ce droit concerne toutes les catégories d’administrés, qu’il s’agisse de personnes physiques ou morales (particuliers comme entreprises).

***Ce que nous pouvons trouver sur le site*** :

* La liste des erreurs les plus fréquentes que nous rencontrons dans l’établissement de nos démarches administratives.
* Des conseils pour les éviter !

Si nous nous trompons quand même, la loi a introduit le concept du droit à l’erreur comme décrit ci-dessus.

![](/upload/oups.png "Le site Oups-gouv.fr")

\
\
Le rôle du formateur repose sur l’explication de l’utilité du site ainsi que de présenter les erreurs les plus fréquentes dans l’e-administration.

#### 2.2.4 Qu’est-ce que le site France Connect ?

**Étape introduction au concept de France Connect *<https://franceconnect.gouv.fr/>***

![](/upload/franceconnect.png "Infographie   fonctionnement France Connect")

**Pourquoi utiliser France Connect ?** ***Quelle utilité pour l’utilisateur ?***

Une myriade d’identifiants et de mots de passe à retenir pour accéder à des sites web individuels, des données à saisir à chaque création de compte : c’est le cauchemar des internautes qui utilisent activement la toile. C’est pourquoi l’État a imaginé un système un peu similaire à Facebook Connect qui permet de s’authentifier sur d’autres sites que Facebook grâce à son login du réseau social. Mais France Connect fonctionne un peu différemment.

Voici comment :

![](/upload/pourquoi-franceconnect.png "Raisons d'utiliser France Connect")

**Comment cela fonctionne t-il ?**

France Connect permet de s’authentifier avec un compte existant sur d’autres sites du réseau participant au système sans devoir recréer un compte, ou en en créant un de manière accélérée grâce à la récupération des données du compte France Connect. Mais une fois le compte créé, vous vous connectez avec vos identifiants France Connect, ce qui est bien plus pratique pour l’utilisateur.

![](/upload/franceconnect-comment-ça-marche.png "Etapes France Connect")

***Le rôle du formateur repose autant sur l’utilité de la méthode que sur la création de l’identifiant unique.***

**Comment créer un compte France Connect ?**

![](/upload/s-p-france-connect.png "Options France Connect")

1. Rendez-vous. sur le site de votre démarche : ANTS, service-public, Assurance retraite ou autre.
2. Cliquez. sur le bouton **France Connect**.
3. Sélectionnez. un **compte** existant pour vous identifier : Impots.gouv.fr, Ameli.fr, IDN La Poste, Mobile **Connect** et Moi ou MSA.
4. Saisissez.

#### 2.2.5 Qu’est-ce que l’identité numérique La Poste ?

**Étape L’identité numérique**

Avec L’Identité Numérique de La Poste, un seul identifiant sécurisé suffit pour vous connecter sur des centaines de sites et prouver votre identité. Ce service simple, fiable et gratuit vous prémunit des risques d’usurpation d’identité et vous permet de maîtriser le partage de vos données personnelles.

**Comment faire ?**

Toute personne majeure possédant une pièce d’identité française en cours de validité et un smartphone peut se créer et utiliser son Identité Numérique La Poste. Pour en bénéficier, c’est simple :

* 1. Demandez votre Identité Numérique sur *<https://lidentitenumerique.laposte.fr/>*.
* 2. Faites valider votre pièce d’identité par un postier assermenté à votre domicile ou en bureau de poste.
* 3. Pour activer votre Identité Numérique, téléchargez gratuitement l’application et choisissez votre code secret.

![](/upload/améli.png "Ecran d'accueil identité numérique La Poste")

\
Quand vous vous rendez sur un site proposant de vous connecter avec Identité Numérique La Poste, identifiez-vous avec votre Identité Numérique.

![](/upload/améli-2.png "Procédure identification Identité Numérique La Poste")

Confirmez que c’est bien vous en utilisant votre code secret ou votre empreinte digitale.

::: tip Sources

<https://www.modernisation.gouv.fr/action-publique-2022/comprendre/action-publique-2022-un-programme-pour-accelerer-la-transformation-du-service-public>

<https://www.afnic.fr/fr/produits-et-services/services/whois/>

<https://observatoire.numerique.gouv.fr/observatoire/>

<https://howsecureismypassword.net/>

<https://franceconnect.gouv.fr/>

<https://ants.gouv.fr/monespace/s-inscrire>

<https://mes-aides.gouv.fr/>

<https://www.service-public.fr/>

<https://www.demarches.interieur.gouv.fr/>

<https://www.ameli.fr/>

:::

::: tip Sources complémentaires

<https://www.service-public.fr/particuliers/actualites/liste-e-administration>

<https://www.vie-publique.fr/eclairage/18925-e-administration-du-pagsi-au-programme-action-publique-2022>

<https://www.ena.fr/content/download/1958/32828/version/4/file/bib_administration%20electronique_2016_sf.pdf>

<https://fr.wikipedia.org/wiki/Administration_%C3%A9lectronique>

<https://usbeketrica.com/article/en-france-l-administration-sert-l-etat-en-estonie-elle-sert-le-citoyen>

<http://www.slate.fr/story/78990/estonie-administration-numerique-linnar-viik>

<https://www.cairn.info/revue-francaise-d-administration-publique-2004-2-page-225.html>

:::