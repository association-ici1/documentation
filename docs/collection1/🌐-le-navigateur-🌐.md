---
title: "🌐 Le Navigateur "
---
# **Fiche Navigation sur Internet**

:::tip Modules APTIC

* *11 : Internet : fonctionnement et outils de navigation web*
* *104- Les alternatives libres pour la navigation Internet*
* **Format :** 2H (Débutant) à 3H (Grand Débutant)
* **Publics** : Grand Débutant/Débutant
* **Résumé :**

  * Définir & présenter les principaux navigateurs web.
  * Définir & présenter les principaux moteurs de recherche.
  * Fonctionnement des recherches sur Internet.
* **Thématique principales:** 

  * Avoir une vision globale des principaux navigateurs & des principaux moteurs de recherche et savoir les différencier.
  * Être capable de faire une recherche simple.
* **Objectifs de la formation :**

  * Expliquer au tout venant ce qu'est un navigateur web & un moteur de recherche
  * Dénombrer les principaux navigateurs & moteurs de recherche et leurs principales particularités
  * Comprendre les bases de la recherche sur internet
  * Comprendre le Fonctionnement du Moteur de Recherche
* **Prérequis :**

  * Clavier & souris
  * Bases de l'informatique

:::

**Sommaire :**

[[toc]]

## 1. Qu'est qu'un Navigateur Internet?

### 1. Définition

> Un navigateur internet est un logiciel qui est une interface graphique entre un être humain et le web. Basiquement le navigateur web se connecte à des sites web, télécharge les éléments de la page web (textes, images, sons, vidéos etc…) et les affiche à l'écran. Ils sont aujourd'hui tous gratuits.

**Un peu d'histoire**

Le premier d'entre eux, ce fut **Netscape Navigator** , qui a inspiré l'item « navigateur » (*browser en anglais*). Ce protocole a été inventé en 1989 par Tim Berners-Lee, il fut d'ailleurs anobli par la reine Elisabeth.
 On lui doit aussi les adresses internet et le HTML « *Hypertext Markup Language »*, format de données qui permet l'affichage des pages web encore utilisé de nos jours.
 L'année suivante il développe World Wide Web vite renommé Nexus premier navigateur-éditeur (qui permet d'écrire du contenu HTLM) qui permet donc d 'également modifier le contenu. En 1992 il participe à la conception et au développement du premier navigateur dit moderne : Mosaic.
 D'abord payant il deviendra gratuit en 1998 et publiera son code source, principe ensuite repris par Firefox à la fin de celui ci au début des années 2000.

### **2. Principaux Navigateurs**

![](/upload/principauxnavigateurs.png "Les 5 principaux navigateurs")

**\-Microsoft Edge**
 Le Navigateur de Microsoft.
 L'un des premiers pionniers dans le marché depuis 1995. Il gagnera rapidement des points en l'installant de base sur le système d'exploitation de l'entreprise : Windows, ce qui remettra en cause pour beaucoup le principe de concurrence.

Microsoft Edge succède à Internet Explorer en 2015 qui souffrait d'une mauvaise réputation suite notamment à son non respect des standards du web pendant longtemps (*Internet Explorer 6* et _Internet Explorer 7(en résumé le langage informatique utilisé sur IE n'était pas au même norme qu'ailleurs))_notamment furent le cauchemar des développeurs web) et à ses failles de sécurité.
 Il est installé de base sur tous les Windows et est le seul navigateur sur Xbox- One.

**\-Google Chrome**

Le Navigateur d'Alphabet(Google) crée en 2008.
 C'est le Navigateur le plus utilisé dans le monde suite à une politique novatrice (mise à jour permanente facilitant notamment la sécurité du navigateur), ses performances et son utilisation sur quasiment toutes les plateformes (*Windows, Mac, Linux, Android et iOS*).

 Sur le sujet du smartphone, il est reproché à Google d'exiger des fabricants que Chrome soit préinstallé si le Play Store ou Google Search est présent sur un smartphone. Difficile pour les fabricants de faire autrement… Samsung a dû s'y plier ainsi que les constructeurs chinois.
 Cependant il faut tenir compte des conditions d'utilisation qui prévoit le prélèvement d'informations et leurs partage à des tiers.

**\-Mozilla Firefox**

Le Navigateur de la Fondation Mozilla fondé en 2002.
 s'inspirant du défunt Netscape il est libre & gratuit. C'est à dire qu'outre la gratuité l'utilisation, l'étude, la modification et la duplication par autrui en vue de sa diffusion sont permises, techniquement et légalement.
 Anciennement second derrière Internet Explorer, il a perdu des parts sur le marché car trop lent à prendre le virage du smartphone.

**\-Safari**

Le Navigateur d'Apple pour Mac & IOS depuis 2003.
Installé par défaut sur tout les MacOS X tout comme sur IOS de la licence.
 A la particularité notable de posséder un mode lecteur pour simplifier l'affichage des pages web.

**\-Opéra**

Le Navigateur développé par la société norvégienne *Opera Software en 1995.*
Il est cependant assez peu utilisé (moins de 10% de parts de marché des navigateurs) par rapport aux ténors que sont Google Chrome, Mozilla Firefox, Microsoft Edge ou Apple Safari mais il est présent sur la plupart des plateformes (Windows, Linux, MacOS, smartphones et tablettes Android et iOS mais aussi sur les consoles de jeu Nintendo DS, Nintendo Wii ou Nintendo WiiU).

**\-Brave**

Le navigateur de Brendan Eich crée en 2008.
 Brendan Eich est également co-fondateur de Mozilla Firefox et créateur du Javascript( language informatique largement employé sur le web) Brave est un logiciel open source avec l'ambition de protéger la vie privé de son utilisateur via diverses moyens comme la non-prise en charge des cookies et autres traceurs(sortes de traces et de relevés d'information qui suit l'utilisateur lors de sa navigation) qui lui permettent notamment de charger les pages web plus rapidement.
 Brave bloque les publicités des sites internet et, si l'utilisateur le choisit, les remplace par d'autres jugées plus pertinentes. Les revenus de ces publicités sont alors reversés principalement entre le site web sur lequel elles apparaissent et l'éditeur du navigateur

**Quelques Chiffres**

![](/upload/moyenneusagenavigateurs.png "Moyenne de la part de pourcentage d' utilisateurs actifs par navigateur")

On remarque une écrasante majorité(62,2) de Google Chrome.
A eux deux Chrome & Safari représentent un peu plus de 75% du marché mondial, autrement dit 3 utilisateurs sur 4 utilisent soit le navigateur d'Apple soit celui de Google.

Ci-contre les Parts de marché des navigateurs Internet de bureau en France de Septembre 2012 à Septembre 2018.

![](/upload/partsdemarche.png "Parts de marché des navigateurs Internet de bureau en France de Septembre 2012 à Septembre 2018")

On remarque la rapide progression de Chrome au détriment de ces principaux rivaux Firefox & Internet Explorer (puis Microsoft Edge). Opéra & Safari ne sortant pas de leurs statut de navigateurs de « niche ».

* En résumé :

| Nom du Navigateur | Organisme             | Pourcentage d'Utilisateurs actifs -Monde | Pourcentage d' Utilisateurs actifs - France | Avantages                                                 | Inconvénients                                                             |
| ----------------- | --------------------- | ---------------------------------------- | ------------------------------------------- | --------------------------------------------------------- | ------------------------------------------------------------------------- |
| Microsoft Edge    | Microsoft Corporation | 6,2                                      | 5,4                                         | Capable de tourner sur terminaux mobiles, léger & rapide  | Interface vieillissante, peu de fonctionnalités                           |
| Google Chrome     | Alphabet(Google)      | 62,4                                     | 63,3                                        | Performances, léger et simple à prendre en main, sécurisé | Outil de récolte de données de Google                                     |
| Mozilla Firefox   | Fondation Mozilla     | 4,8                                      | 14,3                                        | L'un des plus innovants, riche en fonctionnalités         | Nécessite au moins une configuration moyenne                              |
| Safari            | Apple Corporation     | 16,7                                     | 9,4                                         | Rapide, conçu pour les système macs                       | Processus de démarrage un peu long, peu d'extensions & de fonctionnalités |
| Opéra             | Opera Software        | 2,3                                      | 2                                           | Stable & performant,rapide, beaucoup de fonctionnalités   | S'adresse aux utilisateur avancés, mode turbo uniquement sur http         |

### 3. Les Options Navigateurs

Les principales fonctionnalités du navigateur:

![](/upload/fonctionnalitésnavigateurs.png "Les Principales fonctionnalités du navigateur")

**1- Barre d'Adresse ou URL**
 C'est là où l'ont entre l'adresse du site que l'on souhaite visiter ou du moteur de recherche.

**2- Onglet Actif**

La page internet ou l'on est actuellement

**3- Fermer l'Onglet**

Ferme la page

**4- Ajouter un Onglet**

Ouvre une nouvelle page internet.

**5- Autre Onglet**

Autre page déjà ouverte.

**7- Menu**
 Menu des Options

**8-Actualiser la page**

Rafraichir ('refresh' en anglais). Relance la même page à nouveau. Utile en cas de modifications ou de mise à jour.

**9 & 10- Marque-Page/Favori**

La petite étoile à côté représente les favoris, en cliquant dessus vous pourrez garder en mémoire un site que vous avez déjà visité.
 Ces favoris sont aussi souvent appelés marque-pages.

**11-Barre de Recherche**

Permet de lancer une recherche sur le moteur de recherche par défaut. Il est possible de le changer via une languette ou dans les options du menu

**13-Effacer l'historique**

Permet d'effacer l'historique de recherche, soit la liste de nos recherches déjà effectués gardé en mémoire sur l'ordinateur, et uniquement celle conservé sur l'ordinateur. N'efface pas la totalité des traces sur internet.
 Par ailleurs on peut configurer celle ci dans les options du navigateurs, nous traiterons la gestion de celui-ci plus en détail dans le cours sur la Protection des données.

**14-Reculer/Avancer d'une page**

Permet de charger la page que l'on a chargé juste avant ou une page que l'on a chargé après dans la chronologie de l'historique.

La flèche vers le bas suivie d un trait ou juste la flèche représentent les téléchargements et donc une vision des fichiers que vous êtes en train de téléchargez ou avez téléchargé.

Et par la même occasion le mode Navigation Privée qui comme son nom l indique est une navigation sans historique accessible via CTRL+MAJ+P. Là encore nous creuserons ce mode dans un cours ultérieur.

Selon les différents navigateurs ou les différentes versions de ceux-ci il est possible que les différentes options mentionné soit situés ailleurs, l'ergonomie étant changeante, mais la plupart des options sont commune à l'ensemble des navigateurs.

## 2. Qu'est ce qu'un Moteur de Recherche ?

### 1.Définition

> Un moteur de recherche est une site web permettant de trouver des résultats à partir d'une requête sous forme de mots-clefs.
>  Les résultats peuvent être des pages web, des articles de forums, des images, des vidéos ou encore des fichiers.

![](/upload/recherchesurgoogle.png "Recherches sur Google")

Voici ce que cela donne sur un moteur de recherche comme Google lorsque l'on fait une recherche sur « crayon ». On peut ainsi trier le résultat de recherche par type de contenu recherché (images, shopping ou même lieu).

On trouve également des métamoteurs, c'est-à-dire des sites web où une même recherche est lancée simultanément sur plusieurs moteurs de recherche, les résultats étant ensuite fusionnés pour être présentés à l'internaute.

La confusion entre moteurs de recherches et navigateurs vient du fait que les navigateurs ont souvent un moteur de recherche pré-configuré par défaut - aujourd'hui Google mais dans les années 90/2000 c'était plutôt Yahoo ou MSN.

### 2. Principaux Moteurs

![](/upload/principauxmoteursderecherche.png "Principaux Moteurs de Recherche")

On peut les trier en plusieurs catégories :

**Les Incontournables**

**\-Google**

Google est le moteur de recherche sur internet de référence.
 En effet, en concentrant plus de 90% des parts de marchés, quel que soit le navigateur (Google Chrome Mozilla Firefox, Safari…) ou l'appareil. Lancé en 1998 et longtemps en concurrence avec Yahoo, le géant américain est aujourd'hui le plus utilisé.

**\-Bing**

Ce moteur de recherche a été développé, en 2009, pour remplacer Windows Live. Microsoft enrichit son algorithme afin d'apporter plus de fonctionnalités et une meilleure qualité de recherche pour les internautes.
 Très proche de Google, Bing est un moteur de recherche et un environnement complet : web, actualités, photos, vidéos, une carte (Bing Maps), un outil de traduction (Bing Traduction) ou encore un comparateur de prix.
 Il capte 5,2% des recherches en France

**\-Yahoo !**

Yahoo! existe depuis 2005, anciennement le plus utilisé avant l'apparition de Google. En créant beaucoup de fonctionnalités, Yahoo espère grignoter des parts de marchés et reprendre sa place de premier moteur de recherche
 Les très riches actualités peuvent se divisent en plusieurs catégories (Sport, Finance, Cinéma, Musique…) pour faciliter la lecture des dernières informations en très peu de temps.
 Les tendances du jour et une sélection de contenus pertinents sont disponibles. On trouve aussi la recherche multilingue, Mon Web (outil personnalisation), vidéos, photos (Flickr).

**Les Moteurs de Recherche qui protègent vos données personnelles**

**\-Qwant**

&quot;Le moteur de recherche qui respecte votre vie privée&quot;, c'est la promesse que fait le moteur de recherche français.
 Qwant garantit que les informations personnelles des utilisateurs ne sont pas stockées et utilisées à des fins commerciales. Aucun cookie ne trace l'internaute. Les résultats sont très orientés par l'actualité et les tendances.
 Enfin, Qwant propose une page de résultats avec un menu vertical avec les catégories suivantes : Web, Actualités (Medias), Réseaux sociaux, Images, Vidéos, Shopping et Musique (Beta).

Lancé en 2013 il peine encore à s exporter à l international.
 A noter Qwant Junior qui est un moteur dédié aux jeunes : 6-13 ans. Aucun accès à des sites violents, pornographiques ou encore e-commerce. Des actualités adaptées et des sites pédagogiques sont mis en avant tout comme des contenus éducatifs.

**\-DuckDuckgo**
 &quot;Le moteur de recherche qui ne vous espionne pas&quot;
 Le moteur de recherche américain, créé en 2007, ne traque pas l'internaute. L'adresse IP n'est pas utilisée : les annonces sponsorisées sont affichées grâce à la requête de l'internaute.

Dans la même veine que Qwant il a moins de mal à s'imposer à l'international.
 DuckDuckGo propose des recherches d'images et des recherches de vidéos. Il utilise près de 400 sites de références : Wikipedia, Bing, Yahoo!, Yelp…

**Les moteurs de recherche à dimension environnementale et sociale**

**\-Ecosia**

«Nous voulons créer un monde dans lequel l'environnement n'a plus besoin d'être protégé.».
 C'est un moteur de recherche écologique qui vise à réduire la consommation d'énergie imputable à une recherche. Une recherche Google consomme environ 7G de CO2, selon plusieurs études reconnus. Ce moteur caritatif allemand reverse les bénéfices à un programme de reforestation présent dans le monde entier. 5,5 millions d'utilisateurs actifs et réguliers ont permis de planter plus de 14 millions d'arbres.
Une petite équipe de moins de 20 personnes permet de faire fonctionner ce moteur de recherche. Les résultats des recherches et les encarts publicitaires sont fournis par Yahoo! et Bing.

**\-Lilo**

Dans la même veine qu'Ecosia, on retrouve le français Lilo qui utilise l'argent récolté pour le réinvestir dans des projets solidaires, sociaux ou environnementaux.
 Chaque recherche permet de cumuler une goutte d'eau. Les utilisateurs peuvent ensuite alloués leur crédit en gouttes d'eau transformées en argent réel au projet de son choix.

**Naviguer autrement : le concept Xaphir**

– **Xaphir**
Xaphir est un moteur de recherche un peu particulier:en effet, Google va chercher à afficher des résultats dont l'ordre (Page Rank) est défini par des algorithmes selon des critères précis.
 Xaphir fonctionne sur le même principe mais il va demander plusieurs mots clés à l'internaute pour affiner sa recherche. De plus, la popularité du site ne rentre pas en jeu. Un site très populaire ne sera donc pas affiché en priorité.

| Nom du Moteur | Organisme                             | Pourcentages d'Utilisateur actif- Monde | Pourcentages d'Utilisateur actif– France | Avantages                                                                                        | Inconvénients                                                                              |
| ------------- | ------------------------------------- | --------------------------------------- | ---------------------------------------- | ------------------------------------------------------------------------------------------------ | ------------------------------------------------------------------------------------------ |
| Google        | Google LLB(Limited Liability Company) | 92,3                                    | 94,2                                     | +de très nombreux résultats+facile à prendre en main et compatible avec les autres outils google | \-outil de récolte de donnée de Google-nombreuses publicités-présent sur tout les androids |
| Bing          | Microsoft Corporation                 | 2,3                                     | 3                                        | \-Beaucoup de personisations                                                                     | \-Microsoft                                                                                |
| Yahoo!        | Verizon Communications Inc            | 2,25                                    | 1,3                                      | \-Meilleure pendant de Google                                                                    | \-interface un peu chargé                                                                  |
| Qwant         |                                       | Moins de 0,5                            | 0,75                                     | \- n'enregistre et n'analyse pas vos recherches+serveurs en Europe développé en France           | \-manque de visibilité à l'international moins de résultats que sur d'autres navigateurs   |
| DuckDuckgo    | DuckDuckGo,Inc.                       | 0,36                                    | 0,4                                      | +n'enregistre et n'analyse pas vos recherches+moins de mal que Qwant à l'international           | \-moins de résultats que sur d'autres navigateurs                                          |

## 3. Faire sa Recherche

### 1. Fonctionnement du moteur de recherche

Notions D'algorithme & de référencement

#### a.Fonctionnement des crawlers & indexation

Les moteurs de recherche utilisent des robots, intitulés «*crawlers»* pour faire la recherche.
 Ces robots sont des processus informatiques qui se contentent de lire une page web, d'y extraire les liens et d'aller visiter ultérieurement les liens trouvés. En suivant les pages, de liens en liens, ces robots sont capables de visiter pratiquement tout le web.

Malgré la puissance de ces *crawlers*, il y a certaines limitations technique:

* Il n'est normalement pas possible pour ces robots de trouver une page orpheline . C'est à dire une page qui ne reçoit aucun lien d'autres sites ou même de son propre site(ce qui conduit la quasi-totalité des sites web à étaler leurs contenus sur plusieurs page)
* Deuxièmement le web est si grand que même ces robots ne peuvent tout indexer et mettre à jour rapidement.
  Selon le moteur de recherche et la méthodologie qui lui est associé, une page peut être revisitée quelques heures après sa publication ou plusieurs mois après.

Ce qui fait que :

* Un site qui ne reçoit aucun lien sera difficile à trouver par rapport aux autres
* une modification récente sur un site web ne sera pas pris en compte instantanément et prendra un certain temps à être référencer par les moteurs de recherche.

Une fois qu'un crawler a visité un site il va l'enregistrer dans une énorme base de données un «Data Center» comparable à un énorme annuaire. Il va notamment relever un certain nombre de mot-clés utilisés plus tard.

#### b. Comment un moteur de recherche classe les résultats?

Maintenant que l'on a notre répertoire comment savoir quels résultats vont tomber et dans quelle ordre lorsqu'un internaute effectue une recherche?
 Il y a souvent des millions de pages qui possèdent le mot recherché. Pour cette raison, les moteurs doivent classer les résultats par pertinence. Les utilisateurs d'un moteur de recherche doivent facilement trouver le résultat qui répondra à leurs attentes.

C'est la qu'interviennent les algorithmes pour classer tout ça.

::: details Rappel:

*Un algorithme est une suite finie et non ambiguë d'opérations ou d'instructions permettant de résoudre un problème ou d'obtenir un résultat.*

:::

En clair dans notre exemple dans sa gigantesque base de données le robot va dans un premier temps les reclasser par mots-clés mais comme cela ne suffira pas vu la masse de contenus il va pousser plus loin :

* **Il va évaluer la pertinence**
   Le mot-clé est-il présent dans le titre? dans l'URL? dans le contenu? Y'a t-il des synonymes du mot recherché dans le contenu? …
* **Il va évaluer la popularité:**
  Est-ce que la page reçoit beaucoup de liens? Ces liens proviennent-ils de pages elles-mêmes populaires? Les pages faisant des liens ont-elles la même thématique? Les sites qui font des liens vers cette page sont-il dans la même langue? Sont-ils des sites de confiance? ...

Malgré ces deux principaux facteurs influant sur les résultats, des critères alternatifs font leurs apparitions. Par exemple, le moteur de recherche Google base maintenant ses résultats selon la localité du visiteur et selon l'historique des précédentes recherches effectuées par l'internaute

L'ensemble de ces facteurs déterminent la place dans le listing du moteur de recherche. C'est ce que l'on appelle le référencement.
 Un mot toutefois sur le concept de référencement payant. En payant le moteur de recherche il est possible de « tricher » et d'être au sommet du résultat de recherche. Notamment utilisé par Google on le voit sous le titre de « résultat sponsorisés »

### 2. Affiner sa Recherche

#### a. Comment faire une Recherche

A rédiger

#### b. Choisir ses bons mots-clés

D'abord il faut prendre le temps de réfléchir au thème de sa recherche et de le formuler en essayant de distinguer et de fractionner la recherche en mot-clés facilement identifiable par le moteur.

Si l'on veut être sure qu'une certaine suite de mot se retrouve dans le résultat de notre recherche, il suffit mettre la suite de mots correspondante entre guillemets.
 Ainsi si je tape moteur de recherche sur mon moteur, il est possible qu'il me trouve des résultats en lien avec les moteurs ou la recherche en général. Pour éviter ca je n'est qu'à taper « Moteur de recherche ». Rien n'empêche de mettre plusieurs guillemets dans une même recherche

#### c. Les filtres de recherches

Pour affiner notre recherche on peut commencer par désigner le type de contenus recherchés donc sous forme de sites, d'images, de vidéos ect… mais aussi par thématiques (actualités, shopping ect…).
 On peut aussi choisir de n'afficher que les résultats sous une certaine langue ou en provenance d'une certaine région.
 On peut aussi délimiter la recherche au niveau de la date du plus récent au plus ancien mais aussi borner la recherche à une date précise.
 Enfin dans les paramètres de recherche on peut personnaliser sa recherche notamment indiquer le nombre de résultat par page internet (10 à 20 est conseiller pour ne pas ralentir le navigateur).
 Pour le cas de Google il propose ses recherches avec résultats privés qui incluent des données présentes dans d'autres produits google utiliser par l'utilisateur tel que gmail ou gdrive.
Google a aussi l'option activité de recherche c'est à dire en combinant un algorithme issues de vos propres recherches passé.

::: tip Sites Sources :

**<https://www.astuces-aide-informatique.info/268/navigateur-internet>**

**<https://www.1ere-position.fr/blog/10-meilleurs-moteurs-de-recherche-alternatifs-google/>**

**<https://fr.statista.com/statistiques/469330/navigateurs-web-parts-de-marche-france/>**

**<https://www.leptidigital.fr/webmarketing/seo/parts-marche-moteurs-recherche-france-monde-11049/>**

**Microsoft Edge:**

<https://www.cerfi.ch/fr/Actualites/12-Choses-a-Savoir-a-propos-de-Microsoft-Edge.html>

**Google Chrome :**

<http://www.revoltenumerique.herbesfolles.org/2014/04/23/pourquoi-vous-ne-devriez-pas-utiliser-google-chrome/>

:::

**Exercices d'Applications du contenu :**

<https://learningapps.org/2805982>