---
title: "📠 Se repérer dans les offres Internet "
---
# Se repérer dans les offres box et mobile

::: tip Modules APTIC

*34 : Internet : comprendre une offre Internet*

**Résumé**

Cet atelier a pour but de donner aux participants les bons repères pour choisir les offres box et mobiles en fonction de leurs besoins.

**Nombre d'heures:** 3h

**Publics:** Grand débutant/débutant

**Prérequis:**
Maîtrise minimum du Français: lu, écrit, parlé .

**Objectifs de l'atelier:**
Permettre aux participants de se repérer dans l’ensemble des offres internet et mobiles proposées sur le territoire national. 

**Sommaire:**

[[toc]]

:::

## 1. Historique et remise en contexte

### 1.1 Historique des offres internet en France

L’usage grand public des réseau numérique en France a démarré avec le minitel (cette technologie de communication a été développée par le ministère des Postes et Télécommunications et utilisée en France, essentiellement dans les années 1980 et 1990, avant d'être supplantée par l'accès à Internet). 

Accessible initialement à un nombre réduit d'utilisateurs dans un petit nombre d'entreprises et d'universités, l'utilisation de l'Internet par le grand public n'a commencé qu'à partir de 1994 et s'est
vraiment démocratisé au début du 21 ème siècle par l'apparition de l'ADSL. 

Mais c’est dans les année 2000 que l’internet grand public prend son essor avec le lancement de la première box internet : la freebox en 2002. 

lien pub free année 2000 : https://www.youtube.com/watch?v=6ZsPBgjITNs

Devant le succès rencontré par la freebox, l’ensemble des opérateurs ont suivi la tendances et se sont lancé dans l’offre de service triple play (internet/téléphonie/télévision). 

Depuis les offres se sont diversifiés et les opérateurs également. 
On retrouve dans le paysage actuel 3 types de connexion : 

* les offres bas débit
* les offre haut débit (ADSL, cable, satellite …)
* les offres très haut débit (fibre...)

Initialement les offres internet était sous forme très simple d’un accès internet accordé par un modem relié à la ligne téléphonique, ou l’on payait un forfait en nombre de Mo ou de Go de données consommées par mois. 

Aujourd’hui nous sommes habitués a nous abonner auprès d’opérateurs pour bénéficier d’offres de services sous 3 formes

* accès internet
* gestion de ligne téléphoniques
* décodeur télévisuel

En 2019, en France 92 % des foyers sont connectés à internet (sources médiamétrie 2019).

### 1.2 Historique des offres mobiles en France

La téléphonie mobile en France métropolitaine s'articule dans un marché concurrentiel structuré autour de :

* Quatre opérateurs mobile de réseaux : Bouygues Telecom, Free Mobile, Orange et SFR;
* Une quarantaine d'opérateurs de réseau mobile virtuels, dits MVNO, qui utilisent les réseaux de trois des quatre opérateurs disposant d'un réseau d'antennes.
* Trois accords de licence de marque.

Dans les années 2000 face au boom des téléphones portables, les opérateurs adaptent leur offres afin de permettre aux particuliers d’utiliser de façon plus libre leur appareil. 

#### 1.2.1  Rappel développement réseau mobile en France

En 1986, le premier réseau de téléphonie mobile est déployé en France, le Radiocom 2000, distribué par France Télécom. Il utilise des téléphones analogiques transportables principalement intégrés à des voitures. Ce premier réseau (1G) atteint le nombre de 60 000 abonnés. 
En 1987, le marché s'ouvre avec la création de SFR par la Compagnie générale des eaux, s'appuyant sur de nombreux ingénieurs et cadres issus de la Direction Générale des Télécommunications. SFR devient donc le premier opérateur mobile privé, apportant avec lui la concurrence. Il crée le réseau analogique SFR à la norme NMT, destiné à concurrencer celui de France Télécom. 
Le 1er juillet 1992, France Télécom lance le premier réseau de téléphonie mobile numérique français sous la marque Itineris, démocratisant l'utilisation de la téléphonie mobile et les grandes marques de téléphones mobiles avec Nokia, Alcatel, Mitsubishi, Philips, Ericsson et Motorola, dans toute la France. Il s'agit du premier opérateur de téléphonie mobile en France à utiliser une technologie numérique basée sur la norme GSM dont la standardisation avait débuté en 1982. Il utilise la bande de fréquence 900 MHz. SFR suit quelques mois plus tard, en décembre 1992.

Le succès du téléphone mobile croît et les deux opérateurs lancent des opérations pour séduire de nouveaux clients. En 1994, SFR offre une heure de communication aux nouveaux abonnés. Le nombre d'abonnés en France est alors de 280 000. 

Le premier forfait grand public arrive au début de l'année 1996, lancé par France Télécom ce forfait nommé « Déclic » propose un abonnement mensuel à 100 Francs, avec la minute à 4 F pendant les heures de bureau et 1 F pendant les heures dites creuses (soir et week-end). La structure du forfait favorise les communications domestiques, et contribue à la popularisation du mobile. Cette initiative est liée à l'arrivée d'un nouvel entrant quelques mois plus tard. 
En effet, le l'opérateur Bouygues Telecom entre sur le marché et invente « Le forfait de téléphonie mobile ». Il propose un abonnement incluant directement les minutes de communication intégrées au forfait, ce qui évite de nombreux calculs. Le prix du forfait mensuel est fixé à 240 F et il offre 3 heures de communication, le hors forfait est facturé à 2 F la minute supplémentaire en heures pleines et 1 F en heures creuses. La consultation du répondeur devient alors gratuite et illimitée. 
En 1997 apparaissent les cartes prépayées avec l'arrivée de Nomad de Bouygues Télécom.
En 1999 Bouygues Télécom lance le forfait « Millenium », premier forfait incluant les appels illimités le Week-end. La même année, on dénombre près de 20 millions d'abonnés mobiles en France, dont 10 millions sur le réseau Itineris de France Télécom, 7 millions sur celui de SFR, et 3,5 millions sur celui de Bouygues Télécom
En 2000, Bouygues Télécom lance les forfaits « Spot » qui offrent des minutes de communications supplémentaires en contrepartie de publicités et crée le forfait « Ultym'up » destiné aux adolescents avec le blocage du forfait et des services dédiés.
En 2001, les marques de France Télécom Mobile (Itinéris, OLA, Mobicarte) sont réunies sous la marque Orange, « Mobicarte » devient le nom de l'une des offres d'Orange, et les deux autres disparaissent en juin 2001. France Télécom avait racheté l'opérateur britannique Orange plc à Vodafone un an plus tôt. Orange et SFR obtiennent leur licence UMTS. SFR est le premier à lancer son réseau GPRS en juin, il s'agit d'une évolution du GSM permettant de naviguer sur internet et d'utiliser le WAP à une vitesse de 170 Kb/s contre 9 Kb/s pour le GSM, Orange et Bouygues Télécom suivront quelques mois plus tard.
En 2002, Bouygues lance la tarification à la seconde dès la première seconde et obtient sa licence pour l'UMTS son premier réseau 3G, il lance aussi l'i-mode concurrent du WAP. Orange lance le premier forfait WAP illimité à 6 € par mois. 
En 2004, SFR, puis Orange lancent leur réseau UMTS (3G) qui permet les débuts de l'Internet mobile. Bouygues Télécom lance le forfait « Liberté » qui est le premier forfait avec une tarification unique quel que soit l’opérateur. 
En 2008, Bouygues lance le premier forfait illimité Néo 2 avec des plages horaires au choix pour la voix
En 2009, Bouygues Télécom lance le premier forfait illimité 24h sur 24, Neo 24/24. Fin 2009, Free Mobile remporte les bandes de fréquence 3G attribuées lors de l'appel d'offres de l'ARCEP pour un quatrième réseau mobile. Il promet une forte baisse des prix. 

En 2011, face à l'imminence de l'arrivée d'un quatrième opérateur, Free Mobile, les opérateurs créent des marques low-cost : B&YOU, Sosh et la Série Red de SFR. Ces forfaits ont pour particularité de ne proposer aucun mobile subventionné et d'afficher des offres sans engagement de durée.

Afin de limiter l'excès de concurrence provoqué par l'arrivée du quatrième opérateur Free mobile, deux des trois premiers opérateurs — Bouygues Telecom et Orange — ont entamé des négociations à Noël 2015 en vue du rachat de Bouygues Télécom par Orange ; à l'issue de l'opération, Bouygues aurait pu être actionnaire du nouvel ensemble à hauteur de 15 %30. 
Fin 2018, Bouygues Telecom et Orange annoncent l'échec des négociations. Le rachat n'a donc pas eu lieu. Cependant, après une période de forte baisse des tarifs, Stéphane Richard, PDG d'Orange, annonce que les prix vont repartir à la hausse\[ souhaitée]. 
Selon une étude du cabinet finlandais indépendant Rewheel, en , les pays ayant une forte concurrence par la présence de MVNO ou d'un nouvel opérateur, étaient classés parmi les pays les moins chers. À contrario, les pays qui présentaient un monopole des acteurs nationaux dit "marché protégé" se retrouvaient avec les tarifs les plus élevés ; à titre d'exemple sur la 3G, le Gigaoctet était facturé de 6  jusqu'à 20 . Dans le début des pays les plus chers on trouve dans l'ordre : la République tchèque, la Grèce, la Hongrie et l'Allemagne. La France se classant en deuxième position des pays les moins chers, derrière la Grande-Bretagne

#### 1.2.2  Point sur les offres et tarifs

En France, SFR et Orange lancent leur offre commerciale fin 2004. L'opérateur Bouygues Telecom les suivra début 2007 puis on note l'arrivée d'un quatrième opérateur de téléphonie mobile, Free, début 2010

Les différentes offres du marché : 

Offres pour les sociétés ou les particuliers : 
Forfaits avec engagement, dits subventionnés et permettant le renouvellement du terminal téléphonique;
Recharges par carte (prépayé);
Forfaits dits low-cost, avec achat par correspondance et facturation électronique, généralement appelés "offres sans engagement", en opposition aux forfaits subventionnés.

Pour réduire les coûts, les opérateurs essayent de convaincre leurs clients que la facture électronique serait obligatoire, ce qui est contesté par Que-Choisir54.

Chez certains opérateurs, le coût de rupture du contrat pour certains de ces forfaits « sans engagement » s’élevait à 48 € diminué de 2 € par mois d'ancienneté.

En France, la tarification de la téléphonie mobile était en 2012 de l'ordre de deux à vingt centimes d'euro (€) la minute pour les appels vocaux, en fonction de l'opérateur et de l'abonnement. 

En 2019, le bilan effectué par l'A.R.C.E.P., autorité de régulation des télécommunications et des moyens informatiques, évalue à 95 % le taux de pénétration et le nombre de plus de 77 millions de cartes SIM représente la totalité des cartes détenues. 

2. ## Comment choisir son abonnement internet

Après le succès rencontré par le modem en France, ce petit boîtier permettant d'accéder à Internet, les FAI ou fournisseurs d'accès à Internet français se sont intéressés au concept étasunien de triple play (un ensemble de trois services : Internet, télévision et téléphone) pour lancer en 2002 les premières box. 
Ce sont Free avec sa Freebox et Neuf Telecom avec sa neufbox qui ont mis sur le marché les premiers modèles. 
(ajouter images)
Très vite, elles ont rencontré le succès et les autres FAI suivirent la cadence à l'instar d'Orange avec sa Livebox ou encore de Bouygues Telecom avec sa Bbox. Pour s'adapter aux demandes3 des consommateurs qui voulaient une connexion plus rapide que celle permise par l'ADSL, les différents opérateurs ont lancé successivement l'ADSL 2+, le VDSL, le VDSL 2 et la fibre optique. Les fournisseurs d'accès ont également cherché à rassembler en ajoutant des fonctionnalités tout en proposant des prix agressifs. 

Quels critères de choix : 

### 2.1 L’ éligibilité aux différents FAI (Fournisseur d’Accès Internet)

L’éligibilité technique consiste à évaluer la faculté d’un logement à supporter l’installation d’un service donné, comme par exemple l’ADSL, le VDSL ou encore la fibre optique.
Ainsi, un logement éligible aura la capacité d’être connecté à Internet via un opérateur.
La qualité de cette connexion et la nature de l’opérateur dépendront alors du type de connexion, du lieu d’habitation et de l’offre d’abonnement choisie.

Différents moyens de tester son éligibilité: 

* Tester son adresse postale => Cette méthode sera pratique pour les personnes ne disposant pas de téléphone fixe
* Réaliser son test d’éligibilité Internet via son numéro de téléphone fixe => Ce numéro doit être géographique. Autrement dit, il doit être identifié par ses deux premiers chiffres selon la région de France (01 pour l’Île-de-France ; 02 pour le Nord-Ouest ; 03 pour le Nord-Est ; 04 pour le Sud-Est ; 05 pour le Sud-Ouest ) 

(Remarque :Les numéros 08 et 09 sont des numéros virtuels, associés à des box Internet. Ils ne peuvent donc pas être utilisés pour un test d’éligibilité).

### 2.2 Définition du besoin

Il est important de bien définit son besoin afin de trouver un forfait ou un abonnement qui nous correspond parfaitement et ne pas payer pour des services qu’on utilise pas 

### 2.3 Les options d’appels et télévision

Appels illimités en France
Appel illimités en France + mobiles
Appel illimités en France + mobiles + destination étrangères …
Pour savoir quels type d’option téléphonique choisir, il faut étudier les numéros que l’on appelle le plus souvent avec le téléphone fixe (si on en a un). 
Ex : Appels illimités vers fixes en France Métropolitaine et plus de 100 destinations 

Pour ce qui est des options, elles sont liées à l’usage que l’on a de cette dernière. 
Ex :  Télévision : Accès à 100 chaînes ou accès à 200 chaînes 

### 2.4 Le prix

Le prix est également un élément à prendre en compte dans le choix de son opérateur. 
Les prix varient entre 19,90€ auprès d’opérateur comme Sosh ou Free à 37€ auprès d’opérateur comme Orange ou Bouygues Telecom dans le cadre d’offre fibre. 

Des promotions exceptionnelles ont lieu régulièrement durant l’année calendaire, permettant aux consommateurs de bénéficier d’offre alléchante (par exemple offre triple play à 9€ chez free pendant 12 mois sous condition d’abonnement avec engagement)

De très bons comparateurs existent sur internet et permettent de faire un choix en fonction des offres promotionnelles en cours et des spécificités de son lieu d’habitation. 

<https://www.quechoisir.org/comparateur-fai-n21205/>

<https://www.jechange.fr/telecom/internet/comparatif>

3. ## Comment choisir le son abonnement mobile ?

Avec la multitude de forfaits mobile à votre disposition, il est souvent difficile de cibler celui qui correspond le mieux à ses attentes. La concurrence est rude sur ce type de services; 

Les opérateurs historiques sont: 

* Bouygues Telecom,
* Orange,
* SFR

Il existe ce que l’on appel des opérateurs virtuels tels que : 

* Sosh,
* Virgin Mobile
* NRJ Mobile proposant chacun des offres très diverses.

Selon les opérateurs, on trouve des forfaits très différents, tant en termes de prix que de services.

Il est donc essentiel de connaître le type de consommation pour trouver le forfait qui correspond à ses attentes. 

Voici quelques conseils très simples pour économiser au maximum sur un abonnement mobile et payer uniquement pour les services dont on avez besoin.

### 3.1 Définition du besoin

La première chose à faire est de cibler quels sont vos besoins. Pour cela il vous suffit d'examiner votre consommation. Il est important de vérifier :
Le nombre d'heures d'appels effectués en France et le nombre de sms envoyé en moyenne
Le nombre d'appels internationaux 
Si vous faites du hors forfait 
La quantité d'internet utilisée 

Une fois ces éléments ciblés, il  sera plus simple de choisir votre forfait.

### 3.2 Avec ou sans engagement

Un autre paramètre est à prendre en compte lorsque l’on recherche un abonnement mobile est l’engagement. 
Ces offres sont toujours avec un engagement de 12 ou bien 24 mois et proposent un prix préférentiel sur le mobile. 
Il peut être intéressant de s'engager pour 24 mois lorsque le téléphone que l’on souhaite acquérir est un smartphone haut de gamme par exemple, car cela permet d'éviter de payer le montant de votre mobile d'un seul coup.

### 3.3 Les options d’appel

Tous les opérateurs proposent des abonnements avec appels illimités. Il est toutefois important de contrôler votre volume d'appels. 
En effet la grande majorité des consommateurs utilisent entre 2 et 3h d'appels par mois seulement. En souscrivant un forfait adapté à votre niveau de consommation pour pouvez déjà économiser quelques euros.

### 3.4 L’accès internet

La quantité de data proposée est souvent l'élément mis en avant sur une offre mobile. 
En effet de plus en plus d'utilisateurs naviguent sur le net avec leur téléphone. 
Il est donc important de vérifier la consommation internet (par exemple en utilisant un comparateur de forfaits il est possible de trouver l'offre avec la quantité de data idéale).
En effet, il existe des offres sans internet pour les utilisateurs de mobile simple, ainsi que des abonnements allant de 100 Mo à 100 Go.

::: details Rappel sur Octet
Connaître ces unités de mesures s'avère très utile lorsqu'il s'agit d'envoyer des fichiers, pour savoir la place disponible sur un appareil, etc. Que ce soit sur votre ordinateur portable ou bien votre smartphone, vous avez sûrement déjà vu ces termes passer lorsque vous téléchargiez un document, une photo ou une vidéo. Ce téléphone fait tant de Gigaoctets (Go), ce disque dur contient tant de O (To)... Avec toutes leurs différentes abréviations vous êtes un peu perdus et ne vous y retrouvez pas forcément. 
Tous ces préfixes indiquent une quantité, un poids, d'octets ou de bits en informatique. Ils correspondent à la capacité de stockage d'un appareil ou d'un fichier. Comme pour une autre unité de mesure (kilo, litre), les octets ont leur propre échelle de valeur :

En high-tech en termes de capacité de stockage, le Teraoctet est la plus grande valeur, du moins pour les produits grand public. Un appareil comme un disque dur peut aller jusqu'à 10 To de stockage, et un smartphone comme l'iPhone 7 peut aller jusqu'à 512 Go

Ex : Connaître la capacité de stockage peut s'avérer fort utile. Par exemple, un CD de musique occupe en moyenne une taille de 700 Mo, soit 0,7 Go

***Quoi faire avec 1go d’internet ?***

* Surfer sur Internet 60 heures (soit 2 heures par jour)
* Écouter 2 heures de musique en streaming
* Envoyer ou recevoir 100 mails (sans pièce jointe) 
* Télécharger 10 applications

***A quoi correspond 10go ?***
Si l’on cherche simplement à convertir d’10 Go d’internet en heures passées à surfer sur le web, on obtient le chiffre suivant : 10 Go d’internet mensuels permettent de naviguer **820 heures,** soit plus de 24 heures par jour. Autrement dit, **un forfait à 10 Go permet d’être connecté 24/24h**.

À titre indicatif, une heure de musique en streaming équivaut à 45 Mo. En écoutant de la musique une heure tous les jours, l’abonné aura consommé 1 Go à la fin du mois. Sur une enveloppe de 10 Go d’internet, il lui en restera donc 9 ! La personne qui écoute 2h de musique par jour sera, quant à elle, plus proche des 3 Go par mois
Ce sont les vidéos qui consomment le plus de data. Pour avoir un repère, une heure de vidéo par jour, en simple définition, équivaut à une consommation de 14 Go d’internet tous les mois. Combien de temps peut-on regarder de la vidéo avec 10 Go d’internet ? Réponse : 22h.
es messageries instantanées, comme Messenger, Snapchat ou WhatsApp, ne sont pas les applications qui consomment le plus. Tchater pendant une heure coûtera à l’abonné 2 Mo. Il faudrait les utiliser 541h ( soit 18h par jour) pour consommer 1 Go dans le mois. Impossible donc de dépasser les 10 Go d’internet en utilisant uniquement des messageries instantanées.
:::

::: details Quel accès à Internet pour quel usage?
On peut utiliser le smartphone pour surfer sur Internet, recevoir des mails ou visionner des vidéos. 
L’abonnement doit comprendre un accès à Internet. 
En général, vous devrez choisir la quantité de données à ne pas dépasser par mois . 
Il faut aussi vérifier ce que prévoit l'opérateur une fois ce plafond atteint. Le plus souvent, le débit est réduit jusqu'à la date anniversaire, mais il arrive que l'accès à Internet soit bloqué ou que les connexions soient facturées hors forfait.
:::

::: details Comment changer de forfait?
Avec l'évolution particulièrement dynamique des abonnements mobile, changer de forfait devient une pratique des plus courantes. S'il est aujourd'hui très facile de changer de forfait au sein d'un même opérateur via son compte client, migrer vers un autre fournisseur nécessite de prendre certaines précautions pour éviter tout incident : Perte du numéro, frais de résiliation ou encore double facturation.

***Pour changer votre forfait efficacement :***

* Si l’on souscrit un abonnement avec engagement de 12 ou 24 mois, il faut vérifier la date de fin d'engagement. Une résiliation ou une portabilité effectuée avant cette date, entraînera nécessairement des frais de résiliation. Cette information se trouve sur la facture ou dans l’espace client en ligne. 
* Il est possible de  conserver son numéro de téléphone, pour cela il ne faut pas résilier son abonnement mais communiquer le code RIO de son téléphone à son nouvel opérateur (ce code correspond à l'immatriculation du numéro de téléphone). Une case est prévue dans tous les formulaires de souscription. La portabilité s'effectue automatiquement, d'opérateur à opérateur et entraîne le transfert de votre dossier ainsi que la résiliation de votre abonnement.
  :::

4. ## Le matériel / lexique

### 4.1 Le matériel de base pour se connecter sur internet :

#### 4.1.1  Un ordinateur

![Ordinateur de bureau](/upload/ordinateur-de-bureau-3.jpeg "Ordinateur de bureau")

#### 4.1.2  un modem ou une boxe internet

Le modem est le périphérique utilisé pour transférer des informations entre plusieurs ordinateurs via un support de transmission filaire (lignes téléphoniques par exemple)Les ordinateurs fonctionnent de façon numérique, ils utilisent le codage binaire (une série de 0 et de 1), mais les lignes téléphoniques sont analogiques.
Le modem convertit en analogique l'information binaire provenant de l'ordinateur, afin de le moduler par la ligne téléphonique. On peut entendre des bruits étranges si l'on monte le son provenant du modem. 
Ainsi, le modem module les informations numériques en ondes analogiques. En sens inverse, il démodule les données analogiques pour les convertir en numérique. Le mot « modem » est ainsi un acronyme pour « MOdulateur/DEModulateur »

<iframe width="560" height="315" src="https://www.youtube.com/embed/Rlaq1UdYRNE" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

![Box internet des 4 leaders français](/upload/box-internet.jpg "Box internet des 4 leaders français")

#### 4.1.3 une ligne téléphonique

![Prise téléphonique murale](/upload/prise-téléphonique.png "Prise téléphonique murale")

### 4.2 Les différentes moyens de connexion

#### 4.2.1  ADSL

L’ADSL (Asymmetric Digital Subscriber Line ou, littéralement, « ligne d’abonné numérique asymétrique ») appartient à la famille des technologies xDSL, qui ont pour point commun de s’appuyer sur le réseau téléphonique traditionnel. Elle présente deux inconvénients majeurs. D’une part, elle est « asymétrique », c’est-à-dire que les débits ascendants (de l’abonné vers le réseau) sont inférieurs aux débits descendants (du réseau vers l’abonné). En clair, vous mettez beaucoup plus de temps à envoyer un fichier qu’à le recevoir. L’autre défaut de l’ADSL, c’est son débit. Même avec sa version plus élaborée, ADSL 2+, le débit plafonne à 25 mégabits par seconde (Mbits/s). Et encore, ce débit n’est que théorique : dans la pratique, l’utilisateur doit toujours se contenter d’un débit inférieur. Qui plus est, ce débit a tendance à se dégrader à mesure que la distance entre le central téléphonique et le modem de l’abonné augmente. Plus vous habitez loin de votre « répartiteur », moins vous avez de chances de bénéficier d’un débit correct. Dans certains cas, vous ne pourrez même pas recevoir la télévision. Depuis peu, l’ADSL a un petit frère : le VDSL2. Cette technologie, qui utilise aussi la ligne téléphonique, permet de faire grimper le débit maximal théorique à 100 Mbits/s. Mais pour en bénéficier, il faut d’une part être relié à un central téléphonique équipé en VDSL2 par son opérateur, et d’autre part habiter près de ce central, car le débit se dégrade encore plus rapidement qu’en ADSL.

![Raccordement ADSL](/upload/raccordement-adsl.png "Raccordement ADSL")

#### 4.2.2  Fibre Optique

Par ce fil de verre ou de plastique plus fin qu’un cheveu transitent de très grandes quantités d’informations en un temps record. Résultat : la fibre optique permet d’atteindre des débits nettement supérieurs à ceux de l’ADSL. Certains opérateurs promettent 200 Mbit/s, voire 1 gigabit par seconde (Gbit/s), quasiment sans déperdition (attention toutefois, car la promesse n’est pas toujours respectée). Avec de telles capacités, télécharger un morceau de musique ne demande qu’une poignée de secondes, un film ne prend pas plus de dix minutes. Envoyer ses photos sur Internet et recevoir des courriels avec de grosses pièces jointes est quasiment instantané. La fibre permet aussi de recevoir la télé en haute définition voire en ultra haute définition sur plusieurs postes en même temps. Autres avantages par rapport à l’ADSL : le débit est symétrique, c’est-à-dire que le débit montant est aussi élevé que le débit descendant (toutefois, dans la pratique, les FAI ont tendance à plafonner les débits) et le temps de latence (« ping ») est très court. Un détail qui peut s’avérer important, notamment pour les passionnés de jeux en ligne.
 Pour bénéficier de la fibre optique, il faut que le logement soit « éligible ». Pour savoir si le vôtre l’est, renseignez-vous auprès des différents fournisseurs d’accès à Internet ou de votre mairie. Peu importe quel opérateur a déployé la fibre dans votre immeuble ou dans votre quartier, vous pouvez, en théorie, souscrire une offre chez l’opérateur de votre choix, à condition toutefois que ce dernier couvre le secteur. Sachez enfin que quelques travaux peuvent être nécessaires au sein du logement pour bénéficier de cette technologie.

![Raccordement fibre](/upload/raccordement-fibre.png "Raccordement fibre")

![Intérieur câble fibre](/upload/fibre-optique.png "Intérieur câble fibre")

#### 4.2.3   La 4G

Des opérateurs proposent des box Internet fonctionnant grâce au signal 4G. Pour en bénéficier, il ne faut pas être trop exigeant, d’une part parce qu’elle ne permet pas de recevoir la télé et d’autre part parce que le débit réel varie en fonction du nombre d’utilisateurs simultanés de la 4G dans le quartier. Cette technologie est à réserver à ceux qui n’ont pas accès à l’ADSL et dont le logement est bien couvert en 4G par au moins un opérateur.

![Box 4G de bouygues telecom](/upload/bouygues-telecom-4g-box.jpg "Box 4G de bouygues telecom")

::: tip **Sources internet:** 


<https://selectra.info/telecom/guides/comparatifs/offre-internet-top10> <https://www.quechoisir.org/guide-d-achat-adsl-fibre-optique-cable-n10285/> <https://www.touslesforfaits.fr/>

<https://www.meilleurmobile.com/internet/guide-achat/>

<https://www.tuteurs.ens.fr/internet/histoire.html> <https://www.cablereview.fr/guides/internet/infographie-lhistoire-de-box-internet-de-2002-a-2017/>

<http://0franc.free.fr/acces/technologies/equipement.htm> <https://www.tomsguide.fr/lhistoire-des-box-adsl-en-france/> <https://www.commentcamarche.net/contents/753-le-modem> <https://fr.wikipedia.org/wiki/Internet_en_France> <https://www.numerama.com/tech/636216-vers-la-fin-des-forfaits-internet-fixes-illimites-en-france-le-cnnum-levoque-au-nom-de-lenvironnement.html> <https://www.abonnement-adsl.biz/historique-fai/> <https://www.mediametrie.fr/sites/default/files/2020-02/2020%2002%2020%20CP%20Ann%C3%A9e%20Internet%202019.pdf>

<https://www.blogdumoderateur.com/journee-type-internet-en-1999/> <https://www.generation-nt.com/bilan-internet-france-dix-ans-actualite-14719.html> <https://selectra.info/telecom/fournisseurs/orange>

<http://www.telecomspourtous.fr/histoire-reseau-mobile-france/>

<http://www.mobilophiles.com/article-32255774.html>

[https://fr.wikipedia.org/wiki/Téléphonie_mobile_en_France#Premiers_r.C3.A9seaux_de_t.C3.A9l.C3.A9phonie_mobile](https://fr.wikipedia.org/wiki/T%C3%A9l%C3%A9phonie_mobile_en_France#Premiers_r.C3.A9seaux_de_t.C3.A9l.C3.A9phonie_mobile)

<http://www.begeek.fr/free-mobile-loffre-de-forfaits-presentee-au-cours-du-lancement-49744>

[https://fr.wikipedia.org/wiki/Fréquences_de_téléphonie_mobile_en_France#Bande_des_700_MHz](https://fr.wikipedia.org/wiki/Fr%C3%A9quences_de_t%C3%A9l%C3%A9phonie_mobile_en_France#Bande_des_700_MHz)

<https://www.gsmarena.com/network-bands.php3?sCountry=FRANCE> <https://fr.wikipedia.org/wiki/T%C3%A9l%C3%A9phonie_mobile_en_France> <https://www.stelladoradus.fr/lhistorique-des-telephones-mobiles/> <https://www.futura-sciences.com/tech/dossiers/telecoms-histoire-telephone-portable-annees-80-nos-jours-1944/> <https://www.telecomspourtous.fr/architecture-generations-mobiles-2.html>

<https://www.officeeasy.fr/guides/amplificateur-gsm/ampgsm11.php> <https://www.choisir.com/guide/comment-choisir-son-abonnement-internet>

<https://blog.ariase.com/box/dossiers/bilan-adsl-degroupage>

<https://docplayer.fr/1958633-L-acces-a-internet-votre-fournisseur-d-acces-a-internet-1-2-box-internet-box-internet-decodeur-tv-decodeur-tv-freebox-livebox.html>

<https://www.zoneadsl.com/couverture/>

<https://www.monreseaumobile.fr/>

<http://map.datafrance.info/services?d.d1.id=initiatives-transition-energetique&d.d1.gr=marker&d.d1.y=2015&d.d1.gp=nom&d.d1.on=1&d.d1.slug=d1&d.d2.id=eligibilite-toutes-technologies&d.d2.gr=commune&d.d2.y=2015&d.d2.gp=eligibles-tout-debit-confondu&d.d2.on=1&d.d2.slug=d2&utm_medium=datagouv&utm_source=datagouv&utm_campaign=reuse_transition_energetique&coords.lat=48.39273786659243&coords.lng=3.9660644531249996&zoom=6>

:::