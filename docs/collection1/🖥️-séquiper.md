---
title: 🖥️   Comment s'équiper
---
# Comment s'équiper

::: tip Modules APTIC

* *NON?*
* **Résumé** 

Dans un contexte où les nouvelles technologies ont pris une importance particulière dans notre quotidien, toute personne qui se veut réactive aux opportunités d’emplois doit disposer d’un matériel informatique de qualité. Alors, comment s’équiper en matériel informatique ? Ce module vous explique comment réaliser un choix aussi pertinent que qualitatif basé sur vos besoins.

* **Nombre d’heures du module :** 2h00
* **Public :** Intermédiaire
* **Prérequis :** Maîtrise du français lu et parlé / Avoir un accès internet et un ordinateur.
* **Objectifs de l’atelier :**

  * Introduire la notion de besoin d’équipement
  * Comprendre l’intérieur d’un ordinateur pour analyser ses besoins futurs
  * Savoir analyser ses besoins
  * Sélectionner un premier achat d’ordinateur sur internet

​

 :::

* **Sommaire:**

[[toc]]

## 1.Introduction au module

**L'équipement informatique est un des facteurs importants à prendre en compte lorsque vous souhaitez travailler à partir de l’ordinateur. En optant pour des outils de qualité, adaptés à chacun d’entre nous, vous vous garantissez une meilleure efficacité, et un confort de travail appréciable.**

*« Je pense qu’il est juste de dire que les ordinateurs personnels sont devenus l’outil le plus utile que nous avons jamais créé. Ce sont des outils de communication, ce sont des outils de créativité, et ils peuvent être façonnés par leur utilisateur », Bill Gates, fondateur de Microsoft.*

Pour rappel, Bill Gates, qui a fondé la société Microsoft à l’âge de 20 ans et est aujourd’hui une multinationale informatique et micro-informatique. Son activité principale consiste à développer et à vendre des systèmes d’exploitation, des logiciels et des produits matériels dérivés.

Selon l’Observatoire des Dépenses Médias et Multimédias réalisé par Médiamétrie, les Français consacrent sur un an l’équivalent d’un mois de salaire moyen soit 2 348 euros dans leurs achats de biens culturels (livres, presse, musique, cinéma…), équipements informatiques, abonnements Internet et téléphonie. C’est le double pour un professionnel, qui doit souvent compter des dépenses supplémentaires, pour du dépannage informatique ou de la maintenance informatique.

Mais alors, quels sont les critères à retenir pour bien s’équiper sur le plan informatique ? Faut-il préférer un ordinateur portable ou un ordinateur de bureau ? Quelle marque est la plus performante ? Quel budget correspond à vos besoins ? Réponses dans ce module qui fait suite au module sur la découverte de l’ordinateur.

## 2. Les règles de base pour bien choisir son matériel informatique

Aujourd’hui, le monde numérique et l’informatique font partie intégrante de notre quotidien. Ordinateur portable et accessoires, produits d’imprimante ou encore disques durs externes et clé USB, ces matériels nous accompagnent souvent que ce soit au travail ou chez soi. Ainsi, il est très important de faire le bon choix lorsque vous faites vos achats. Pour vous aider, découvrez à travers cette partie introductive, trois conseils pour bien choisir votre matériel informatique.

* ### 2.1 Choisir son utilisation

Qu’il s’agisse de matériel tel qu’un ordinateur portable ou de logiciel, le choix d’un matériel informatique va dépendre grandement de l’utilisation dont vous allez en faire. En effet, tout va dépendre de vos besoins. Par exemple, pour le choix d’un ordinateur, un ordinateur portable est évident si vous avez de mobilité. Par contre, si vous travaillez souvent la maison, vous pouvez parfaitement choisir un ordinateur de bureau.

Il existe également d’autres éléments à considérer comme votre utilisation quotidienne. Si vous avez besoin d’un écran performant pour des traitements de vidéo, un écran plus grand, mais aussi une carte graphique de qualité doit être de mises. Aussi, les autres accessoires varient également selon vos besoins.

* ### 2.2 Un matériel de qualité

Quelle que soit l’utilisation de votre matériel informatique, il est toujours préférable d’opter pour des appareils de qualité. En effet, en choisissant des matériaux de qualité, vous pouvez être sûr de leur durabilité et de leur performance. Si vous prenez des appareils bas de gamme, vous pouvez avoir des problèmes très tôt et être obligé d’acheter ou de réparer. Ce qui pourrait vous entrainer des dépenses supplémentaires.

* ### 2.3 Un budget adapté à ses besoins

Le prix des matériels informatiques dépend de la qualité, mais également du matériel en question. En effet, un ordinateur portable est souvent plus cher qu’un ordinateur bureau. De même qu’une souris spécifique pour gamers sera également plus chère qu’une souris classique. Si vous souhaitez choisir un produit qui dure et de grande qualité, n’hésitez pas à y mettre le prix.

En optant pour des produits et matériels de qualité, même si le prix est assez élevé, vous avez l’assurance d’une durabilité plus grande et donc des réparations moins onéreuses. Aussi, comme la performance et la qualité riment avec le prix, il est évident de débourser une somme plus importante pour avoir des accessoires performants.

## 3. Comment choisir son matériel : l’ordinateur ou des types d’ordinateurs

Lors de l’achat d’un ordinateur, le plus important est de savoir ce que vous allez faire avec. La réponse à cette question va avoir une influence sur le prix, le type d’ordinateur (fixe ou portable), et la puissance dont vous aurez besoin.

![](/upload/usage.png)

### 3.1 Ordinateur fixe ou portable

Le choix entre un PC de bureau ou un PC portable dépend encore une fois de vos besoins. Afin de vous aider à choisir, voici les avantages principaux de l’un par rapport à l’autre.

![](/upload/capture-d’écran-du-2020-07-15-14-58-01.png)

### 3.2 Le guide pour choisir votre ordinateur : les clés à connaître pour faire votre choix

Lors du module de « découverte de l’ordinateur » dispensé par l’association ICI, vous avez appris ce que comprenait la boîte de l’ordinateur. De manière approfondie vous avez découvert les notions suivantes :

* La carte mère
* Le processeur
* La mémoire vive
* Le disque dur
* L’alimentation
* Le lecteur CD/DVD
* La carte son
* La carte graphique
* La carte réseau

Les performances d’un ordinateur dépendent des six composants suivants:

1. ***L’écran***
2. ***La carte mère***
3. ***Le processeur (CPU)***
4. ***La mémoire vive (RAM)***
5. ***Le disque dur (HDD / SSD)***
6. ***La carte graphique (GPU)***

Ce sont ces éléments que vous devez absolument prendre en compte lorsque vous achetez un ordinateur. 

Pour un ordinateur portable, vous devrez également tenir compte de son poids et de son autonomie.

Commençons par l’écran.

#### 3.2.1 L’écran

**Quels critères pour choisir son moniteur informatique ?**

C'est à ce titre que le choix d'un moniteur n'est pas à prendre à la légère : étant donné la place de l'informatique dans le quotidien, vous passerez sûrement des heures et des heures devant l'écran de votre ordinateur. Avec des désagréments, voire des migraines à la clé, si l'affichage et l'équipement ne sont finalement pas ceux que vous attendiez.

Les usages de l'ordinateur se sont en effet démultipliés : visionnage de vidéos, jeux vidéo, bureautique, création graphique, etc. Vous devez donc identifier vos besoins prioritaires, car aucun écran PC existant ne brille dans tous les domaines sans la moindre lacune.

Tout dépend de la définition, de la technologie de la dalle, de la connectique, de l'ergonomie... Le design prend lui aussi de l'importance. Les écrans PC LCD, plus fins et plus légers que les antiques tubes cathodiques, s'y prêtent plus volontiers. Mais c'est un argument dont le caractère subjectif est indéniable. Pour tout le reste (taille de dalle, fréquence, résolution, temps de réponse), ce dossier vous assistera dans votre décision.

**Quel écran d’ordinateur pour quel usage ?**

![](/upload/capture-d’écran-du-2020-07-15-15-00-05.png)

#### 3.2.2 La carte mère

Composant pas toujours des plus connus, la carte mère est pourtant la véritable colonne vertébrale d’un pc.

En effet, du bouton de mise sous tension au processeur en passant par le bloc d’alimentation et la carte graphique, tous les composants y sont reliés de manière directe.

La carte mère est un grand circuit imprimé au fond du boîtier ! Son nom n’est vraiment pas usurpé étant donné qu’elle définit à elle seule presque toutes les possibilités du système.

Toutes les cartes mères, même les moins chères intègrent une puce audio et une puce réseau.
Au niveau des connecteurs, elles proposent au moins 4 ports USB 2.0, les entrées et sorties audio et un port Ethernet.

Les modèles plus chers disposent d’une connectique plus étoffée avec par exemple : une sortie audio optique, un second port Ethernet, un connecteur SATA, des ports USB 3.0, une interface FireWire, etc.

Généralement, les cartes mères haut de gamme sont livrées avec des braquets qui contiennent des ports USB supplémentaires et supportent le montage de disques durs (ou de SSD) en RAID.\
Il existe également des modèles avec une puce Wifi ou Bluetooth intégrée…\
A noter que toutes ces fonctionnalités peuvent s’ajouter via des cartes filles ou grâce à des dongles USB.

Quoi qu’il en soit, ne perdez pas de vue qu’une connectique étoffée tire le prix de la carte mère vers le haut.

**Avec ou sans puce graphique ?**

Certaines cartes mères sont équipées d’un chipset avec une « carte graphique » intégrée.
Il s’agit d’un contrôleur graphique d’entrée de gamme aux performances très limitées surtout en 3D.
Les puces graphiques intégrées sont cependant largement suffisantes pour tous les usages courants excepté les jeux 3D…

**Les différents formats des cartes mères**

La majorité des cartes mères sont au format ATX ou µATX.\
Les cartes mères ATX sont plus grandes et offrent donc plus de ports d’extension (PCI-Express et PCI) voir plus de banques de mémoire.

Les cartes µATX sont plus petites et destinées à des boîtiers plus compacts au format … µATX ! Elles peuvent cependant prendre place dans un boîtier ATX classique.

Par le passé, les cartes µATX étaient souvent des modèles économiques.
Mais de nos jours, les constructeurs proposent des cartes mères µATX très haut de gamme (taillées pour l’overclocking avec une connectique très complète) qui n’ont rien à envier à leurs grandes sœurs.

#### 3.2.3 Le processeur

Le processeur est le cerveau d'un ordinateur. Plus il est rapide, plus vite sont traitées les données. Alors, AMD ou Intel ? Mono, dual, tri, quad ou octo-core ? Que choisir en fonction de son utilisation (jeux, bureautique, vidéo…) ?

AMD et Intel proposent une gamme assez large, mais force est de constater qu'Intel est un peu seul sur le segment "performance". Pour autant, AMD ne propose pas des processeurs au rabais et se bagarre un peu plus sur le rapport performance / prix sur l'entrée et le milieu de gamme. La course à la fréquence et au nombre de cœurs était très en vogue il y a quelques années, mais elle est désormais moins d'actualité. Pour gagner en performance, les deux protagonistes améliorent et optimisent surtout le fonctionnement interne des processeurs, c'est-à-dire leur architecture. Parallèlement aux performances, AMD comme Intel travaillent à réduire la consommation électrique des CPU qui, entre deux générations, sont moins énergivores tout en étant plus rapides.

**Les 3 critères de choix des processeurs**

Le processeur est un peu le cerveau de votre ordinateur. La *vitesse* et la *rapidité* de ce dernier dépendent en grande partie de lui. Il est composé de :

* **Cœur** : il s’agit du nombre de puces incluses dans le processeur. Le nombre de cœurs est très important, puisqu’il détermine le **nombre de tâches** que vous pourrez réaliser simultanément. Attention, le nombre de cœurs indiqué sur les fiches produits peut être soit **physique**, soit **logique** . Si vous êtes un gamer, nous vous conseillons de choisir un ordinateur comportant un processeur avec des cœurs uniquement physiques. Pour le multi-tâches classique en bureautique, l’hyper-threading est très adapté.

Le nombre de cœurs d’un processeur peut donc être de type 2/4, 4/4, 4/8, 8/8, 8/16 …

* **Fréquence** : il s’agit tout simplement de la vitesse de votre ordinateur. Plus elle est élevée, plus votre ordinateur ira vite. La fréquence est exprimée en Hz et correspond au **nombre d’opérations par seconde** que votre ordinateur va pouvoir faire. Un processeur avec une fréquence de 2GHz, pourra réaliser 2 milliards d’opérations à la seconde. Mais la fréquence donnée correspond à la somme des cœurs de votre processeur. Si vous avez un processeur 4 cœurs physiques cadencés à 2,5Ghz, vous aurez en réalité 2,5/4 = 625Mhz. Si vous avez 2 cœurs cadencés à 2,5Ghz, la fréquence de chaque cœur sera de 1,25Ghz. Un processeur 2 cœurs à 2.5Ghz ira donc plus vite pour une seule tâche qu’un 4 cœurs de même fréquence. En revanche, si vous avez plusieurs petites tâches à effectuer, le 4 cœurs ira plus vite.
* **Mémoire cache** : c’est une mémoire tampon qui permet de stocker temporairement les données qui deront être traitées par le processeur. Ce procédé permet de **réduire le temps d’attente** et augmente donc la rapidité de votre processeur. La mémoire cache augmente ainsi la fréquence d’horloge d’un processeur.

**Les processeurs INTEL**

INTEL est la marque leader en termes de fabrication de processeurs pour ordinateur (PC de bureau, PC portable, Serveur …). Elle produit plusieurs séries de processeurs, dont les 3 plus connus pour ordinateur sont issus de la 7ème génération de processeurs INTEL pour PC :

* Les processeurs INTEL i3 (*Fréquence de 2,4Ghz à double cœurs physiques et 4 logiques, et 3Mo de mémoire cache*)
* Les processeurs INTEL i5 (*Fréquence de 3,2Ghz à double cœurs physiques et 4 logiques, et 4Mo de mémoire cache*)
* Les processeurs INTEL i7 (*Fréquence de 3,6Ghz à double cœurs physiques et 4 logiques, et 4Mo de mémoire cache*)

Vous pourrez également trouver d’autres gammes comme *les processeurs Intel Core M, V Pro, ou encore M Vpro ou l’I7 extrême édition.*

**Les processeurs AMD**

AMD est la seconde marque du secteur à concevoir et fabriquer des processeurs. On distingue deux séries, la série E et la série A :

* La **série E** est prévue pour des ordinateurs d’entrée / milieu de gamme pour permettre les tâches basiques sur un ordinateur. Ce modèle est économique.
* La **série A** est prévue pour des ordinateurs nécessitant plus de puissance. (A4, A6, A8, A9, A10, A12)
* La **série FX** (*Dotée des technologies Vulkan et direct X12*)

AMD est toutefois peu présente sur le marché des processeurs par rapport à INTEL. Vous les retrouverez beaucoup plus dans les cartes graphiques où elle est beaucoup plus spécialisée.

#### 3.2.4 La mémoire vive

« C'est la mémoire temporaire de l'ordinateur, c'est là que sont stockés tous les fichiers sur lesquels l'utilisateur est en train de travailler. Cette mémoire est temporaire, car les informations sont supprimées lors de l'arrêt de l'ordinateur.​

Plus cette mémoire est importante, plus l'ordinateur travaille facilement et rapidement et plus il peut gérer des tâches différentes.​

La capacité de cette mémoire s'exprime en Gigaoctets (Go) ».

La mémoire vive est un composant indispensable de tout ordinateur, qu’il soit fixe ou portable, gamer ou bureautique. Elle se présente sous forme de barrette composée de micropuces indépendantes que l’on nomme aussi barrettes mémoires, mémoire séquentielle, barrettes de RAM ou RAM tout court.

**Combien de mémoire faut-il ? Quelle est la quantité de RAM qu'il faut choisir ?**

* **4Go de RAM :** C'est le MINIMUM pour un PC de bureautique ou Multimédia, mais cela ne suffit plus pour un PC orienté pour le jeux. Inutile donc de vous orienter sur un PC qui n'aura que 4Go de ram.
* **8Go de RAM : Le MINIMUM** pour une config de jeux actuellement. Dans le cas de jeux très gourmands cela peut ne pas s'avérer suffisant.
* **16Go de RAM Recommandé pour :**

  * * Jeux très gourmands en résolution 2K ou 4K.
    * Budget conséquent
    * Si vous utilisez des logiciels de retouches photos et/ou des logiciels de montage vidéo
    * également si vous faites de la virtualisation de machine ou du streaming.
* **\> 16Go de RAM :** Utilisation de logiciels professionnels très gourmand.

#### 3.2.5 Le disque dur

C'est le support sur lequel on peut stocker des informations. Les capacités de stockage ne cessent d'augmenter et permettent donc d'enregistrer un grand nombre de données : documents, photos, films... ​

**Il y a actuellement deux types de disques durs** : SSD et HDD. Les SSD ont l'avantage d'être extrêmement rapides, mais ils sont plus chers et de capacité limitée ».

Pour disposer d'une capacité de stockage satisfaisante, le disque dur de votre ordinateur doit être choisi avec soin.

Le disque dur permet de stocker nos données et, contrairement à la mémoire vive, conserve ces données même lorsque l'ordinateur est éteint. Les données sont stockées sous forme binaire (0 et 1) sur le disque dur et sont appelées bits.

Le disque dur héberge aussi bien le système d'exploitation que tous les logiciels et fichiers contenus dans l'ordinateur.

Le disque dur est un élément indispensable de votre ordinateur. C’est là que tout est stocké : le système d’exploitation, les logiciels, tous vos fichiers personnels. Un disque dur vieillit et s’use naturellement, ce qui peut entraîner des pertes de données et de performances.

Il existe deux grands types de disque durs internes : les HDD et les SSD. ... Le HDD, plutôt bon marché, est parfait pour stocker un grand nombre de fichiers. Le disque SSD est lui équipé d'une mémoire flash, similaire à celle qui se trouve dans les smartphones et les clés USB.

Les disques durs SSD utilisent une mémoire flash (et non mécanique comme les HDD) pour stocker l’information. Ils offrent une durabilité améliorée et une performance supérieure aux disques durs HDD. Néanmoins, ces derniers présentent une capacité de stockage généralement plus élevée pour un coup moindre.

**Que faut-il donc regarder en particulier ?**

#### **La capacité**

La capacité de stockage, exprimée en octets, est le premier critère de sélection. Elle est liée directement au budget mais surtout à l'usage que l'on aura d'un disque dur.

* Si les besoins de stockage de données se limitent à des fichiers bureautiques (quelques Ko), une petite capacité (1 Go) suffit pour en stocker **plusieurs dizaines de milliers**.
* Pour des photos ou des musiques au format MP3 (quelques Mo), une capacité supérieure (à partir de 160 Go) sera nécessaire pour en stocker autour **d'une centaine de milliers**.
* Les fichiers les plus gourmands en espace de sauvegarde sont les vidéos : de l'ordre **d'1 Go à 30 Go** pour un film en haute définition ! Un disque dur de grande capacité (supérieure à 1 To) permettra de stocker **plusieurs centaines d'heures de vidéo**.

#### **La vitesse de rotation**

C'est l'élément qui indique la rapidité d'un disque dur à plateaux magnétiques. La vitesse de rotation s'applique aux plateaux qui tournent autour de l'axe du disque dur. Elle s'exprime en tours par minute (RPM). Plus cette donnée est élevée, plus le disque dur est rapide... et plus il est bruyant, gourmand en électricité et aura tendance à chauffer. La norme est de **5400 tours/minute** pour un disque dur externe 2,5" (ordinateur portable) et de **7200 tours/minute** pour un disque dur externe 3,5" (ordinateur de bureau).

**Quel disque dur pour quelle utilisation?**

Le choix d'un disque varie selon les besoins de chacun. Tout d'abord, il faut savoir qu'il est préférable d'avoir deux disques durs différents sur un ordinateur, plutôt qu'un seul à capacité égale.

En effet, de cette façon un disque dur est réservé au système d'exploitation et l'autre au stockage de données, logiciels, jeux...

En cas d'erreur système et d'obligation de formater le disque dur système, les autres données ne seront pas perdues.

#### **Disque dur entrée de gamme : pour la bureautique et le surf sur Internet**

Si vous choisissez de réserver un disque dur pour le système d'exploitation, celui-ci devra avoir une bonne performance mais ne demandera pas des performances optimales :

* Prix : aux alentours de 50 €,
* Disque dur fonctionnant à 7200 tours par minute,
* Capacité minimale de 250 Go,
* La capacité de stockage ne sera pas l'élément déterminant.

Ces disques durs conviendront également pour de la bureautique, du surf sur Internet etc.

#### **Disques durs de haute capacité : stockage des données**

Si vous souhaitez consacrer un disque dur au stockage de vos données, il faudra alors choisir un disque dur haute capacité.

Ce disque dur n'aura pas forcément besoin d'être très performant.

Vous trouverez ainsi des disques durs d'une capacité de 1 ou 2 To en 7200 ou 5400 tours minutes entre 70 et 100 €

#### Disque dur haute performance : pour des programmes spécifiques

Le disque dur haute performance est nécessaire si :

* vous recherchez un ordinateur puissant,
* vous souhaitez faire fonctionner des programmes demandant un matériel puissant.

Le disque dur de haute performance doit avoir :

* un débit de lecture et écriture important,
* une mémoire cache importante.

Vous devrez donc porter votre choix vers un disque dur en 10 000 tours par minute et une mémoire cache de 32 ou 64 Mo.

#### 3.2.6 La carte graphique

La carte graphique soulage le processeur d’un ordinateur dans les tâches qui nécessitent un traitement graphique. Il est indispensable de choisir une carte graphique performante pour jouer à des jeux vidéo récents, traiter des images, monter des vidéos et créer des animations 3D dans des bonnes conditions.

Les deux choses que vous devez savoir avant de faire votre choix:

1. Il existe deux types de cartes graphiques : les cartes dédiées et les cartes intégrées à la carte mère de l’ordinateur. **Les cartes graphiques dédiées** sont les plus performantes, car elles possèdent leur propre processeur et leur propre mémoire. **Les cartes graphiques intégrées** exploitent le processeur et la mémoire de l’ordinateur, ce qui limite sérieusement leurs performances.
2. Il existe deux grandes marques qui fabriquent les puces (GPU) qui sont ensuite assemblées par des constructeurs comme Asus, MSI, Gygabyte ou encore Zotac: **AMD** et **NVidia**. Ces deux marques sont excellentes. Dans votre choix, privilégiez une carte graphique dédiée avec un type de mémoire rapide.

![](/upload/capture-d’écran-du-2020-07-15-15-03-41.png)

#### 3.2.7 Les imprimantes

Comment choisir son imprimante en listant les explications suivantes sous forme de tableau avantages et inconvénients :

![](/upload/capture-d’écran-du-2020-07-15-15-06-35.png)

#### 3.2.8 Le réseau

Comment choisir son réseau en listant les explications suivantes sous forme de tableau avantages et inconvénients :

![](/upload/capture-d’écran-du-2020-07-15-15-07-47.png)

**PC de marque ou PC monté : l’éternel débat**

**Un PC de marque** est un ordinateur assemblé par un constructeur. C’est-à-dire que vous ne pourrez en principe pas personnaliser sa configuration lors de son achat. Soit il vous plaît en l’état, soit il ne vous plaît pas. **Un PC monté** est un ordinateur assemblé par vous-même ou une entreprise spécialisée, ce qui donne l’avantage de pouvoir choisir librement ses composants dès le départ.

Si vous souhaitez acheter un Mac, cette question ne se pose pas et vous pourrez uniquement acheter un Mac sur le site officiel d’Apple, dans un Apple Store ou chez un revendeur agréé.

Si vous optez pour un PC équipé de Windows ou Linux, le choix est plus ouvert. Les grandes surfaces (Fnac, M Eltronics, InterDiscount, Fust, Manor, Media Markt, etc.) vendent en général uniquement des PC de marque.

### 3.3 Le critère de la marque

Lors du choix d’un matériel informatique, il est important de réfléchir à la question de la marque.

***ASUS***: marque très appréciée des consommateurs pour la bonne qualité des composants et offre un excellent rapport qualité prix. ​

***APPLE***: est considéré en 2019 comme la meilleure marque d’ordinateur portable par de nombreux utilisateurs ! Apple n’a cessé de développer ses produits pour devenir une excellente marque d’ordinateur portable.​

***HP***: Hewlett-Packard propose une gamme de PC importante qui répond aux besoins de l’ensemble des consommateurs.

**DELL** : Le constructeur DELL est actuellement le troisième fabricant d’ordinateur au monde derrière Hewlett-Packard et Lenovo. C’est une marque reconnue pour sa qualité et son SAV.

**LENOVO** : Le design des ordinateurs Lenovo est souvent assez austère. Il offre probablement le meilleur rapport qualité/prix en 2019.

**ACER** : Les ordinateurs de marque ACER sont aujourd’hui d’une qualité très correcte.

Cette liste est non exhaustive !

### 3.4 Des éléments importants à ne pas oublier

Lorsque vous achetez un ordinateur, pensez également aux éléments suivants :

![](/upload/capture-d’écran-du-2020-07-15-15-14-24.png)

## 4. **Comment choisir son matériel : l’ordinateur portable**

### 4.1 Les fonctionnalités à regarder impérativement

Le marché de l’ordinateur portable est très concurrentiel, les nouveautés sont fréquentes et les performances ne cessent d'augmenter tandis que prix, poids et encombrement baissent.

#### 4.1.2 L’encombrement

**Transportable**

Si vous n'avez pas vraiment besoin de mobilité, c'est-à-dire que vous ne vous déplacez pas sans arrêt avec votre machine, que vous ne travaillerez pas dans des endroits improbables, il est inutile d'acheter un tout petit portable. Choisir un modèle un peu plus imposant mais doté d'un grand écran (17" ou 18"), d'une clavier numérique séparé et d'une grosse batterie s'avère alors un bon choix en terme de confort d'utilisation

**Portable**

Votre ordinateur doit pouvoir vous suivre partout mais vous avez besoin d'un minimum de confort d'utilisation. Un écran de 15,4 " suffit et n'est pas tellement encombrant. Le poids est raisonnable et le clavier confortable.

**Mini portable**

Les minis portables sont de très petits ordinateurs. La miniaturisation, toujours meilleure, des composants permet de fabriquer des ordinateurs performants de moins d'1 kg offrant toutes les fonctionnalités que l'on attend d'un ordinateur. On peut ranger ces machines dans un petit sac et s'en servir n'importe où. Intéressant pour un grand besoin de mobilité, des déplacements fréquents ou pour travailler n'importe où.

#### 4.1.3 L’écran

**Taille**

On trouve des portables avec des écrans allant de 7 à 20 pouces. La taille de l'écran dépend de l'usage que vous souhaitez faire de votre machine. Un grand écran permettra de visionner agréablement des films, de travailler confortablement ou encore de jouer. En revanche, l'encombrement sera plus important ainsi que le poids. Au contraire un petit écran à partir 12'' et moins permet d'avoir une machine très pratique à transporter.

**Brillance**

Comme pour les photos sur papier il faut choisir le mat ou le brillant. Le brillant favorise le visionnage de vidéos avec un rendu proche d'une TV. Le mat lui est certes moins agréable à regarder mais est moins sensible aux reflets. Encore une fois, le choix dépend de votre utilisation.

#### 4.1.4 Le processeur

Pour les ordinateurs portables, les processeurs peuvent comporter de 2 à 4 cœurs. Plus le nombre de cœurs d'un processeur est élevé, plus l'ordinateur est en mesure de faire fonctionner plusieurs programmes simultanément de manière fluide.

Il existe deux grands fabricants de processeurs, le groupe Intel et le groupe AMD. Dans le tableau ci-dessous, vous trouverez une sélection des processeurs les plus courants, classés en fonction de leur puissance de calcul.

En comparant les processeurs vous verrez lesquels sont les plus efficaces pour la bureautique face au multimédia et au Gaming.

![](/upload/gaming.png)

La fréquence de fonctionnement, exprimée en Gigahertz ou GHz, entre aussi en compte pour déterminer la puissance finale d'un ordinateur portable.

#### 4.1.5 La carte graphique

Tout comme les processeurs, il existe une multitude de modèles de cartes graphiques, aux performances très différentes selon votre usage. Ici, ce sont les groupes Nvidia et AMD qui se partagent le marché.

Sachez cependant que pour un usage exclusivement bureautique, le choix de la carte graphique n'aura pas d'impact au quotidien.

![](file:///tmp/lu3569n0tik5.tmp/lu3569n0tiwz_tmp_50d8d23eb1c16c59.png)

#### 4.1.6 La mémoire vive

Aussi connue sous le nom de RAM (Random Access Memory), la capacité de la mémoire vive est exprimée en Gigaoctets ou Go. Elle a pour fonction de stocker temporairement les données à traiter par le processeur. Aussi, une quantité de mémoire vive élevée améliore la fluidité de l'ordinateur lors de l'exécution des tâches lourdes et complexes.

Pas de soucis de ce côté, pour un usage de bureautique une mémoire vive de 4 Go sera suffisante. Cependant si vous comptez ouvrir plusieurs documents ou de nombreuses pages Internet en même temps, portez votre choix sur un ordinateur portable équipé de 8 Go pour éviter les ralentissements et vous offrir un ordinateur propice à la productivité.

#### 4.1.7 Le disque dur

Le disque dur d'un ordinateur portable sert à stocker les différents fichiers et programmes présents sur l'ordinateur même éteint. En moyenne les ordinateurs portables sont équipés d'un disque dur d'une capacité allant de 500 Go à 2To ce qui vous permettra de stocker à loisir de nombreux fichiers tels que votre système d'exploitation, vos programmes ainsi que vos photos, vidéos, musiques et bien d'autres.

Certains ordinateurs disposent d'espaces de stockage particuliers, appelés "**SSD**". Un SSD est généralement 5 à 6 fois plus rapide qu'un disque dur, et permet de démarrer l'ordinateur et les programmes en quelques secondes.

![](/upload/capture-d’écran-du-2020-07-15-15-17-42.png)

#### 4.1.8 La connectique

La connectique est un autre point à ne pas négliger.

En effet, en bureautique, il est conseillé d'avoir plusieurs ports USB 2.0 ou USB 3.0 afin par exemple : de connecter votre smartphone, disque dur externe, clé USB, tracker d'activités, ou encore lecteur MP3. Sachez également qu'un port USB 3.0 permet d'atteindre une vitesse de transfert bien plus rapide avec un périphérique compatible.

![](file:///tmp/lu3569n0tik5.tmp/lu3569n0tix1_tmp_5dbf291bee9cecd1.jpg)

Lors de réunions, vous pouvez avoir besoin d'afficher des présentations PowerPoint ou diaporama sur un écran externe ou un vidéoprojecteur, il faudra donc s'assurer que votre ordinateur soit bien équipé d'une prise VGA.

Un port Ethernet (ou RJ45) peut être très utile en supplément du Wifi intégré pour ainsi vous connecter à Internet en haut débit via un câble réseau.

\
Néanmoins ce port a tendance à disparaître sur certains modèles nomades et peut être remplacé par des adaptateurs compatibles USB.

#### 4.1.9 L’autonomie

Elle est de 2 à 8 heures en utilisation bureautique. C'est un point clé dans le choix d'un portable. Il est bon de savoir que plus une batterie est performante plus elle est lourde. Il faut prendre cela en considération si vous devez transporter votre PC partout. On peut donc jouer sur la qualité de la batterie mais aussi sur la consommation. Il existe désormais des processeurs basse consommation qui permettent aux machines de fonctionner bien plus longtemps en autonomie totale (CPU suffixes U et Y). Regardez bien les données constructeurs (en sachant qu'elles sont souvent un peu optimistes).

#### 4.1.10 Le système d’exploitation

C'est certainement le plus important. Si tout le monde est rompu aux OS Microsoft, de plus en plus d'ordinateurs portables low cost sortent sous distribution Linux afin de faire des économies sur les licences Windows. Le système Linux est assez simple d'utilisation mais il faut tout de même prévoir un certain temps d'adaptation si c'est votre première excursion hors Microsoft. De même, toutes les applications ne sont pas compatibles avec Linux, même si on trouve des logiciels équivalents.

( ***Se référer au module « découverte de l’ordinateur »***)

#### 4.1.11 Le bruit et la chaleur

On oublie souvent ces paramètres lors du choix d'un PC portable. Pourtant à l'usage, ils se révèlent bien souvent prépondérant en terme de confort d'utilisation. Les ordinateurs chauffent et pour cela ils sont ventilés. Parfois la ventilation peut être très bruyante. De même, il arrive que certaines machines chauffent plus que d'autres. Vérifier ces paramètres avant l'achat est souvent difficile, surtout si l'on commande en ligne. On peut néanmoins dire que plus on monte en gamme moins ces problèmes sont importants.

Il peut s’avérer utile d’aller voir l’ordinateur tourner en magasin pour se faire une idée plus précise.

#### 4.1.13 La difficile solution de l’upgrade

Bien sûr, on voudrait pouvoir faire avec son portable la même chose qu'avec son ordinateur de bureau : remplacer les composants un peu anciens par des plus performants. S'il est toujours facile d'ajouter de la mémoire, et souvent facile de changer le disque dur, seuls certains portables permettront de changer le processeur, et encore moins auront une carte graphique interchangeable :\
seuls les (rares) portables équipés de carte graphique reliés par connecteur spécial à la carte mère (connecteur MXM) pourront voir leur carte graphique remplacée.\
Quant aux cartes graphiques externes (e-GPU), il faut un port spécial pour ne pas les brider, les connections via port PCMCIA ou ExpressCard étant équivalentes à du PCI-Express x1...

Il faut donc bien choisir dès l'achat !

Sachez qu'il existe des magasins spécialisés qui vous changeront le processeur ou la carte graphique si c'est possible.

#### 4.1.14 Le poids et l’autonomie

Choisissez le poids idéal en fonction de votre usage.\
Si le poids moyen d'un ordinateur portable se situe autour de 3 kilos, sachez qu'il existe néanmoins des ordinateurs qui pèsent entre 1,5 kg et jusqu'à moins de 1 kg grâce à leurs composants réduits ou leur batterie moins lourde. Toutefois attention à l’autonomie et aux affichages de durée irréaliste en magasin.

## **5. Comment choisir son matériel : la tablette**

Selon IDC, 37,6 millions de tablettes ont été écoulées au troisième trimestre 2019 dans le monde. C'est 1,95% de plus qu'au troisième trimestre 2018. Néanmoins les ventes plafonnes depuis plusieurs années.

![](/upload/capture-d’écran-du-2020-07-15-15-22-14.png)

Initialement perçue par beaucoup de personnes comme un appareil de pure consommation multimédia, la tablette est devenue multitâche. A la maison, au travail ou en déplacement, les critères à prendre en compte avant d’acheter votre tablette sont les suivants :

### 5.1 Les différentes familles

Depuis l’apparition de la première tablette, la famille s’est agrandie. De nombreuses marques de high-tech proposent aujourd’hui leurs modèles :

* la ***tablette tactile***, classique, parfait mix entre l’ordinateur et le smartphone;
* la [](https://www.cdiscount.com/informatique/tablettes-tactiles-ebooks/livre-electronique-ebook/l-1079802.html)***liseuse*** qui permet de stocker et lire toutes sortes de contenus;
* la [](https://www.cdiscount.com/informatique/tablettes-tactiles-ebooks/tablette-tactile-clavier/l-1079835.html)***tablette avec clavier amovible***, pour ceux qui sont moins à l’aise avec l’écran tactile ou qui s’en servent pour écrire;
* et enfin, ***l’ordinateur-tablette***, véritable PC portable qui se convertit en tablette tactile grâce à un écran amovible.

Les prix des tablettes sont très variables. Comptez en moyenne entre 150 et 250 € chez Samsung ou Huawei. Les iPad de la marque Apple, sont accessibles à partir de 300€ et peuvent grimper jusqu’à plus de 1000€ pour un iPad Pro.

![](/upload/capture-d’écran-du-2020-07-15-15-23-24.png)

### 5.2 Le système d’exploitation

Actuellement, deux systèmes d’exploitation dominent le marché des tablettes : iOS d’Apple qui détient désormais moins d’un tiers du marché et Android de Google qui dispose quasiment des deux tiers restants. Microsoft détient autour de 5%.

Contrairement au monde des smartphones, largement dominé par les deux systèmes d’exploitation **Android** (proche de Google) et **iOS** (disponible uniquement sur les mobiles d’Apple), vous aurez un troisième choix pour les tablettes avec **Windows** (de Microsoft). A considérer si vous ne voulez pas changer de vos habitudes de travail avec votre PC et désirez partager plus facilement vos fichiers. Par contre, le nombre d’applis disponibles sur ce dernier est limité.

![](/upload/capture-d’écran-du-2020-07-15-15-31-39.png)

( ***Se référer au module « découverte de l’ordinateur »***)

### 5.3 L’écran

La **qualité d’affichage** est très variable sur les tablettes. Accordez lui une grande importance si vous aimez visionner dessus des films et surfer des heures sur le Web. A moins de lire très peu, faites aussi attention à la définition : plus le nombre de pixels par pouce (donnée constructeur) est élevé et plus votre lecture sera agréable.

La diagonale de l’écran est, en général, comprise entre 5 et 12 pouces, les modèles les plus courants étant en 7 et 10 pouces. Les modèles 7 pouces sont plus adaptés à un usage extérieur en raison de leur petit poids (300g) qui permet de les transporter facilement contrairement aux tablettes de 10 pouces qui sont globalement plus adaptées pour la maison (600 g) et plus agréables à utiliser.

La résolution minimum acceptable sur les produits actuels est le 1280×800 pour les modèles 7/8 pouces et le 1920x1200 en 10/11 pouces. Les meilleurs produits disposent désormais de plus en plus souvent de la résolution 2048 x 1536 (QXGA) ou plus.

![](/upload/capture-d’écran-du-2020-07-15-15-33-04.png)

### 5.4 Le processeur

C’est sur ce point que les tablettes se distinguent des PC, en effet la très grande majorité des tablettes (et des smartphones) fonctionnent avec des processeurs fabriqués sous licence ARM alors que [les PC](https://www.configspc.com/configs-pc-types/) fonctionnent avec [des processeurs X86 fabriqués par Intel et AMD](https://www.configspc.com/processeur/). Le point fort des processeurs ARM est leur faible consommation d’énergie qui permet d’assurer une bonne autonomie (tout est relatif !) aux téléphones et tablettes. On parle en général de SOC (system on a chip) car le système complet est embarqué sur une seule puce, pouvant comprendre de la mémoire, un ou plusieurs microprocesseurs, des périphériques d’interface etc.

### 5.5 L’autonomie

Contrairement aux smartphones, nombre de tablettes assurent une relative bonne autonomie. Facilement, au moins dix heures d’utilisation, soit, au minimum, une journée de travail. Et même plusieurs jours en usage modéré.

### 5.6 Les connexions

Nous vous conseillons de choisir une tablette avec lecteur de carte SIM si vous comptez l’emporter régulièrement en déplacement. Tout d’abord parce que cela vous évitera de dépendre du Wifi des lieux publics, ensuite, parce que les prix de la connexion de données ont fortement baissé et incluent même l’usage dans plusieurs pays d’Europe. Autre chose : une connexion permanente augmente vos chances de localiser et retrouver votre appareil en cas de perte.

![](/upload/capture-d’écran-du-2020-07-15-15-34-52.png)

### 5.7 La capacité de stockage

La capacité de stockage correspond au volume d’informations (photos, applications, vidéos, documents, etc.) qu’un dispositif sera capable de stocker. Pour une tablette tactile, elle varie généralement entre 16 Go et 1 To (1 024 Go). Notez également que plus de 95 % des tablettes stockent les informations sous forme de mémoire flash ou SSD. Les autres fonctionnent avec un disque dur.

La capacité de stockage pour une tablette tactile se choisit en fonction de l’usage qu’on en fait :

* vous allez surfer sur Internet, lire vos mails ou stocker provisoirement vos fichiers : une tablette tactile de 32 Go peut suffire ;
* vous prévoyez de stocker des fichiers lourds (films, documents de dessin) ou de télécharger de nombreuses applications : optez alors pour une capacité de stockage plus importante, entre 64 Go et 128 Go.

![](/upload/capture-d’écran-du-2020-07-15-15-35-56.png)

### 5.8 Comment protéger sa tablette

Vous disposez d’un large choix en termes d’accessoires pour tablette. Pour protéger votre tablette tactile des aléas du quotidien (liquide renversé, chute, rayures), vous pouvez recourir à plusieurs types d’accessoires :

* ***le film protection écran***: ultrafin et à peine visible, il protège l’écran de votre tablette d’éventuelles rayures ;
* ***l’étui de protection*** : c’est un accessoire indispensable si vous souhaitez prolonger la vie de votre tablette, notamment si vous êtes particulièrement maladroit ! Vous pouvez opter pour une housse de transport, une coque ou un étui pour une protection optimale.

## 6. **Comment choisir son matériel : le téléphone portable**

Le but de cette partie est de s’interroger sur la sélection de son futur téléphone. Comment fait-on pour le choisir ?

### 6.1 Quel usage de son futur téléphone ?

Si pour vous, un téléphone ne sert qu'à téléphoner, orientez-vous davantage vers un téléphone portable simple (dit non-smartphone)​

En revanche, si vous souhaitez naviguer sur Internet, envoyer des SMS, lire des vidéos, ou encore prendre des photos, il faudra s'orienter vers un **smartphone**.​

Le choix devient plus compliqué !

### 6.2 La question épineuse de la marque

Le but de cette sous-partie est de donner une idée de la manière dont les marques sont perçues en donnant les explications suivantes :

**Huawei**: La marque est un spécialiste du rapport qualité/prix et a permis à Huawei de s’imposer comme le troisième acteur du marché.​

**Lenovo**: La marque a fait le choix de la modularité​

**Samsung**: Pendant longtemps, on moquait Samsung pour la complexité de sa gamme. Large gamme d'utilisation avec Galaxy J, le Galaxy A et le Galaxy S pour le haut de gamme.​

**Sony**: Le fabricant assure de plus un bon suivi des mises à jour Android et propose un design réellement différent du reste du marché.​

**Wiko**: s’est rapidement imposé en France avec ses smartphones très abordables, va se concentrer sur le marché à moins de 400 euros.

### 6.3 La fiche technique

Le premier des réflexes dans un magasin sera de regarder la fiche technique du produit désiré. Un premier regard sur le design de l’appareil vous aura convaincu. Place à la technique. Le but de cette étape est de comprendre ce qu’il se passe derrière tous les termes techniques de la fiche. A vous de le faire comprendre aux apprenants en les expliquant comme dans le tableau ci-dessous :

![](/upload/capture-d’écran-du-2020-07-15-15-38-29.png)

### 6.4 L’écran

Le but de cette partie est d’analyser les données techniques d’un téléphone concernant l’écran en expliquant les notions de qualité, de taille, de définition ainsi que de résolutions :

L'écran est un élément important lors de l'achat puisqu'il apporte le confort nécessaire à nos yeux. La qualité de l'écran est donc importante. Il faut ainsi regarder la définition de l'écran, sa résolution et le type d'écran.

![](/upload/capture-d’écran-du-2020-07-15-15-39-35.png)

![](/upload/capture-d’écran-du-2020-07-15-15-41-48.png)

**Quelle est la taille d’écran idéale pour un smartphone ?**

Les fabricants réfléchissent de plus en plus à ce détail important d’un smartphone : la taille de l’écran. Alors quelle est la taille idéale ?

C’est un fait indéniable : les personnes sont nombreuses à passer de plus en plus de temps devant leur smartphone. Il faut dire qu’aujourd’hui on peut tout faire avec : téléphoner, envoyer des messages, des mails, des photos, des documents, effectuer ses démarches administratives, jouer ou bien encore regarder un film. Mais pour effectuer tout cela, quelle est la taille d’écran idéale pour un smartphone ?

**Taille idéale pour un écran de smartphone ?**

La taille du smartphone est devenu l’un des critères les plus importants lorsque l’on se décide d’investir dans un nouveau produit. Si l’on remarque que les téléphones portables ont tendance à être de plus en plus imposant par la taille, il ne faut pas oublier que les tailles plus modestes existent toujours et qu’avant de choisir, il convient de bien en définir son usage.

Voici une présentation des différents modèles et de leurs avantages.

**Les smartphones avec écran de petite taille ?**

Ils s’appellent Sony XZ1, iPhone SE ou bien encore Samsung J1 et ont en commun d’être des smartphones de petites tailles. Ils sont dotés d’un écran de 4 à 4,6 pouces. Même si beaucoup de gens ont tendance à toujours vouloir plus gros en cas d’investissement, il ne faut pas oublier que les smartphones aux petits écrans présentent tout de même un certain nombre d’avantages.

Tout d’abord, il est plus facile de les tenir dans la main et une fois dans la poche, ils se font plus discrets. Il est aussi plus facile de naviguer entre les applications qui sont plus vites accessibles, les mouvements du doigt étant moins importants que sur un grand écran.

En résumé, nous pourrions dire que ce sont des téléphones intéressants pour les personnes qui débutent sur les téléphones intelligents, pour une meilleure appréhension du système tactile. De plus, avec la sortie de plus en plus rapprochée de modèles derniers cris, forcément plus grand, leurs prix baissent considérablement, il faut donc y songer en cas de budget restreint. Soulignons malgré tout un inconvénient de taille, il faut être doté d’une bonne vue car les écritures sont souvent plus petites.

**Les smartphones avec écran de taille moyenne ?**

Ils ont un écran qui fait plus de 5 pouces, en général entre 5,1 et 5,8. On pense aux iPhones 6, 7, 8 ou bien encore au Galaxy s7 ou s8. La taille de leur écran a été une véritable révolution au moment où ils sont apparus. Alors certes, ils nécessitent qu’on les manie avec les deux mains et prennent un peu plus de place dans la poche, mais la lecture de textes est beaucoup plus agréable, on a en général moins besoin de rapprocher ses yeux de l’écran et ils sont également confortables pour jouer à des jeux ou bien encore regarder des vidéos.

Parlons aussi de la photo. Pour beaucoup de personnes, les smartphones ont aujourd’hui remplacé les appareils photos, et sur un smartphone de taille moyenne, prendre des photos et les regarder en détails est forcément plus agréable.

**Les smartphones avec écran de grande taille ?**

Ils sont presque incontournables aujourd’hui, dès qu’un nouveau modèle de smartphone voit le jour chaque année, les constructeurs y associent un modèle XL. On pense là à des modèles comme l'Iphone XR, Samsung S10 ou Huawei P30. Leur écran a une taille moyenne de 6 pouces mais peut aller au-delà et le moins que l’on puisse dire c’est qu’en magasin, on les repère de loin.

Leur avantage, c’est incontestablement qu’ils sont confortables pour les yeux et que l’on peut facilement y regarder des vidéos ou bien encore des films ou séries.

En dehors de cela, il faut bien noter que la prise en main est parfois longue car il faut être parfaitement à l’aise avec ses deux mains et de plus, une fois dans la poche, il est difficile de ne pas les remarquer.

En résumé, on peut dire que ce sont les meilleurs modèles pour regarder des vidéos longues ou des films ou bien encore pour travailler. En effet, de plus en plus de personnes se servent de leur smartphone pour envoyer des mails ou bien encore ouvrir des documents de différents formats, la taille de l’écran est forcément un plus pour travailler avec aisance.

::: danger ATTENTION : 
LES PLUS GROS SMARTPHONES N’ONT PAS FORCÉMENT LE PLUS GRAND ÉCRAN
:::

Vous souhaitez un grand écran pour lire plus facilement, faire de la photo ou bien encore regarder des vidéos. Vous vous dîtes que sans doute, il faut s’orienter instinctivement vers le téléphone le plus massif pour bénéficier de l’écran le plus large, méfiez-vous ! La diagonale de l’écran n’exprime que la taille de l’écran. Il faut également prendre en considération la taille des marges autour de ce dernier.

Il est donc recommandé de bien lire les étiquettes si vous souhaitez vous orienter vers un écran XL pour plus de confort.

Alors au final, quelle taille d’écran choisir ?

On ne peut pas vraiment dire qu’il existe une taille idéale pour l’écran d’un smartphone. Il ne faut pas résumer le tout en disant que la taille la plus importante est forcément la meilleure. Il faut au contraire savoir choisir son smartphone en fonction de l’utilisation que l’on souhaite en faire et de la façon dont on se sent à l’aise avec la taille de celui-ci.

Pour vous aidez à prendre votre décision, voici un tableau de correspondance pour quelques tailles standards, sur la base d’écrans en 16/9.

![](/upload/capture-d’écran-du-2020-07-15-15-43-15.png)

::: details A noter 
La superficie d’écran progresse de plus en plus vite avec l’augmentation du nombre de pouces : 24.8 cm² séparent des écrans de 4 et 5 pouces, tandis que la superficie gagnée entre des écrans de 5 et 6 pouces s’élève à 30,3 cm². Pour jouer, surfer sur Internet ou utiliser leurs applications habituelles avec un meilleur confort visuel, de plus en plus d’internautes s’orientent vers des écrans de 5 ou 5,5 pouces. Mais la taille ne fait pas tout !
:::

En résumé, les smartphones avec petit écran sont parfaits pour les débutants, les smartphones avec écran de taille moyenne correspondent bien aux amateurs de vidéos et de photos et enfin les tablettes sont un outil très intéressant pour un usage professionnel et pour ceux qui passent du temps à regarder des films et des séries, dans les transports et même depuis leur lit !

### 6.5 Les processeurs et la RAM

Le but de cette sous-partie est d’analyser les données techniques du processeur et de la RAM d’un téléphone qui consiste en un point essentiel. En effet :

* **La partie processeur est l'une des plus importantes, et pourtant c'est l'une des plus compliquée à comprendre pour un novice.**​

​

Aujourd'hui près de 99 % des smartphones sont en 64 bits. Le nombre de cœurs sur un processeur permet de **multiplier les tâches par le nombre**, ainsi, un processeur à un cœur est théoriquement 6 fois moins rapide qu'un processeur à 6 cœurs .

* La **quantité de RAM** (Random Access Memory) est également importante. Il s'agit de la **mémoire disponible pour les applications**. Afin de pouvoir utiliser son smartphone tranquillement, il vaut mieux acheter un smartphone avec **au moins 2Go de RAM** ».

### 6.6 L’appareil photo

Entre 2006 et 2018, le marché des appareils photos numériques s’est effondré (–65,7 %, source : GfK, novembre 2018). Et pour cause : dans le même temps, la photo sur smartphone a fait des progrès spectaculaires.

Aujourd'hui devenu très important dans l'utilisation quotidienne, la qualité de l’appareil photo repose sur sa résolution donnée en mégapixels, explications :

![](/upload/capture-d’écran-du-2020-07-15-15-44-49.png)

Chaque année, les constructeurs augmentent le **nombre de Mégapixels** offerts par les **capteurs photo de nos smartphones** de la même façon que la taille des écrans.

Afin de creuser un peu la question et dans le but de déterminer **quelle résolution est la plus pertinente** pour un smartphone, il ne suffit pas de faire une course effrénée au mégapixels. En effet, d’après certains spécialistes, un appareil photo de 4 Mégapixels est déjà en mesure d’offrir une définition optimale pour une impression au format A3 et parfaite lorsqu’il s’agit de tirer des clichés au format 13 x 18 cm. S’il s’agit de photos destinées à être mises en ligne ou à être diffusées sur grand écran, alors dans ce cas, un capteur photo 8 Mégapixels ferait largement l’affaire, même pour une diffusion sur un téléviseur UHD.

### 6.7 La mémoire interne

Qu’est-ce que la mémoire interne d’un téléphone ? Aujourd'hui devenu très importante dans l'utilisation quotidienne, la mémoire interne d’un téléphone repose sur sa capacité à stocker les informations, explications :

La **mémoire interne** sert à **stocker différents types de contenus** tels que les photos, les vidéos, les musiques ou encore les applications.​

Si vous souhaitez installer plus d'une vingtaine d'applications sur votre appareil, orientez-vous vers une mémoire interne de 16Go minimum.​

Notez également que sur les 16Go de mémoire interne du téléphone, près de **6Go seront utilisés par le système d'exploitation** et l'ensemble des applications préinstallées

### 6.8 La batterie

Le but de cette sous-partie est de parler d’un thème connu de tous mais succinctement.

La batterie de notre smartphone est mise à rude épreuve chaque jour du fait de l'utilisation de l'internet mobile et des applications. Ainsi pour une **utilisation intensive**, il vaut mieux s'orienter vers des batteries d'**au moins 3000 mAh** ».

### 6.9 La carte Sim

Le but de cette sous-partie est de parler des différents formats de carte Sim existants. Explications :

* Carte **mini SIM** : Il s'agit de la carte SIM standard que l'on retrouve dans les anciens téléphones mobiles basiques.​
* Carte **micro SIM** : Plus petite que la carte SIM standard, la carte micro SIM est utilisée dans la plupart des smartphones actuels.​
* Carte **nano SIM** : De plus en plus petite, la carte nano SIM n'est disponible que chez certains fabricants comme Apple, Microsoft et HTC.​
* Carte **e-SIM** : Il s'agit peut-être de la future carte SIM déjà intégrée au smartphone. Cette carte serait source de privation de liberté pour les utilisateurs et les opérateurs. Les fabricants contrôleraient donc les abonnements de tous leurs clients.

### 6.10 Le réseau compatible

Le but de cette sous-partie est de parler des différences 3G/ 4G/ 5G.

*Explications* :

La 3G et la 4G sont des technologies utilisées par les réseaux mobiles de **3ème génération** pour la 3G et de **4ème génération** pour la 4G​.

Pour **profiter de la compatibilité 4G du smartphone**, il est primordial d'être **couvert par le réseau 4G** de l'opérateur chez lequel le forfait a été souscrit ce qui est fréquent aujourd’hui sur le territoire.

* Vous pouvez vérifier, à tout moment, la carte de visualiser de la couverture 4G sur le territoire français à l’adresse suivante : *<https://www.data.gouv.fr/fr/reuses/carte-de-couverture-du-reseau-4g-en-france/>*

Le tableau ci-dessous donne les différences entre les différentes générations :

![](/upload/capture-d’écran-du-2020-07-15-15-46-13.png)

Aujourd'hui, les forfaits les plus communs sont ceux proposant un réseau 3G ou 4G. Si vous souhaitez souscrire un abonnement en 4G, alors vous pourrez également profiter de la couverture 3G, et de toutes les générations de réseaux précédentes, afin de rester connecté à tout moment. Il en va de même pour un abonnement 3G : vous serez en mesure de vous connecter sur les réseaux moins puissants, lorsque vous quittez une zone géographique couverte par la 3G.

### 6.11 Le budget

En listant tous les critères à prendre en compte, il n’est pas question de ne pas parler du plus important, l’argent. Même si celui-ci est déterminant il n’est pourtant plus un élément fondamental, explications :

Le prix du smartphone est le **critère le plus important** à prendre en compte. Depuis l'arrivée des smartphones sur le marché, les nouvelles technologies et fonctionnalités sont de plus en plus nombreuses et elles se répercutent sur les prix. En effet, les smartphones sont **de plus en plus chers car de plus en plus puissants**.

La tendance va continuer, en effet les téléphones, comme tous les appareils électroniques, sont composés de pièces provenant de divers fournisseurs, et si le coût de ces pièces augmente, le prix des téléphones augmentera aussi. Ces dernières années, la demande de stockage a entraîné une hausse des prix, ce qui a fait grimper le coût de la mémoire et incité les fournisseurs à investir dans la construction d'un plus grand nombre d'usines pour répondre à la demande.

L'ajout de capteurs plus sophistiqués, comme le capteur 3D des iPhone X et XS, ou la multiplication des objectifs, comme les trois capteurs du Huawei Mate 20 Pro, coûtent également plus cher. Il en va de même pour les matériaux comme le verre, la céramique ou l'aluminium. Il est également coûteux pour des entreprises comme Samsung de mettre au point un tout nouveau procédé de fabrication pour des éléments comme le verre bombé et les écrans OLED flexibles.

L'arrivée de la 5G va également faire grimper le prix des smartphones car peu de modem seront capables de prendre en charge la norme dans un premier temps, ce qui va mécaniquement tirer les prix vers le haut.

Cependant, rassurez-vous, les marques ont bien conscience de ce phénomène et s’adaptent en produisant des smartphones pour toutes les utilisations. Le prix de vente moyen d'un smartphone atteint "*345 euros en 2019, en hausse de 10,3 % par rapport aux 313 euros de 2018* " commente Anthony Scarsella, analyste chez IDC, dans le Worldwide Quarterly Mobile Phone Tracker d'IDC. Il existe, par exemple, une multitude de smartphones à moins de 100 euros. Une rapide recherche sur internet vous permet de vous en rendre compte.

**Il existe donc des smartphones à tous les prix, votre choix devra dépendre de votre utilisation.**

## 7. **Conclusion**

**De nos jours les matériels informatiques sont indispensables dans chaque domicile, car désormais ces derniers représentent un indispensable.**

Grâce aux développements de la technologie, il est possible de voyager autour du monde sans devoir bouger de votre fauteuil, et cela grâce aux différents matériels informatiques : ordinateurs, téléphones et tablettes.

**Alors, comment trouver le matériel idéal ?**

La réponse à cette question est plutôt simple. Lors de l’achat d’un matériel informatique, le plus important est de savoir ce que vous allez faire avec. La réponse à cette question va avoir une influence sur le prix, le type d’ordinateur (fixe ou portable), et la puissance dont vous aurez besoin. Tout dépend donc de ce que vous voulez en faire !

En ayant suivi ce module, vous connaissez tous les paramètres à connaitre pour faire un choix utile et efficace.

Cependant il reste probablement encore des conseils à partager avec vous, mais le but de ce module était de se focaliser sur l’essentiel sans tomber dans le piège de dire qu’une famille de matériel est meilleure qu’une autre.

**Le meilleur matériel est celui qui correspond le mieux à vos besoins, c’est simple !**

A vous de jouer !

::: tip SOURCES

<https://le-monde-informatique.com/sequiper-materiel-informatique/>

<https://desidees.net/5-astuces-pour-sequiper-en-materiel-informatique-a-moindre-prix/>

<http://www.lancer-sa-boite.com/choisir-materiel-informatique/>

<https://cours-informatique-gratuit.fr/cours/le-materiel-informatique/>

<https://www.materiel.net/guide-achat/>

<https://www.lesnumeriques.com/informatique/quel-materiel-informatique-acheter-pour-rentree-a3285.html>

:::

::: details Lexique

L'informatique (information automatique) désigne l'automatisation du traitement de l'information par un système concret - la machine - ou abstrait- on peut parler d'automate. L'informatique désigne l'ensemble des sciences et techniques en rapport avec le traitement de l'information.

Le jargon informatique est un pseudo langage propre aux représentants des professions gravitant autour de l’informatique, discutant par messages courts, ces personnes utilisent des abréviations souvent d’origine anglophone.

Vous trouverez ci-dessous une liste complémentaire succincte des abréviations et sigles pouvant vous aidez à mieux comprendre l'informatique.

**Afficher** : Faire apparaître des informations sur un écran.

**Aperçu** : Ouverture d'une petite fenêtre qui permet de voir le fichier sélectionné, avant de l'imprimer par exemple.

**Application** : Autre nom d’un programme.

**Backup** : Programme de sauvegarde des fichiers du disque dur

**Bureau** : Espace de travail délimité par la surface de l'écran et et sur lequel on peut déposer toutes sortes de documents.

**Bureautique** : Ensemble des techniques et des moyens tendant à automatiser les activités de bureau et principalement le traitement et la communication de la parole, de l'écrit et de l'image.

**Cache** : Zone mémoire rapide utilisée entre un périphérique et un programme.

**Carte graphique** : Périphérique interne assurant l’interface entre le système d’exploitation et l’écran du moniteur.

**Carte mère** : Carte électronique servant de support aux différents composants de l'unité centrale (microprocesseur et RAM).

**Cliquer** : Enfoncer et relâcher le bouton-poussoir d'une souris ou d'un autre dispositif destiné à déplacer un repère sur un écran.

**Configurer** : Définir les sous-ensembles constituant un matériel ou un logiciel ou agir sur leurs paramètres pour en assurer la mise en oeuvre.

**Connexion** : Procédure permettant à un utilisateur de se mettre en relation avec un système informatique et, si nécessaire, de se faire reconnaître de celui-ci.

**Corbeille** : Espace de stockage temporaire dans lequel sont placés les fichiers supprimés. Il faut penser à la vider si vous ne voulez pas qu'on puisse accéder aux documents que vous avez effacés.

**Courrier électronique** : Service de communication sous forme écrite par Internet ou autres réseaux appelé également e-mail.

**Défilement** : Déplacement vertical ou horizontal du contenu d'un écran de visualisation à l'intérieur d'une fenêtre de telle façon que de nouvelles données apparaissent à un bord alors que d'autres disparaissent au bord opposé.

**Didacticiel** : Logiciel interactif destiné à l'enseignement ou à l'apprentissage, et pouvant inclure un contrôle de connaissance

**Disque dur** : Composant de l'ordinateur sur lequel sont enregistrés les fichiers du système d'exploitation, les logiciels et toutes les données. Les disques durs sont en général très fiables, mais la moindre panne peut avoir de grandes conséquences.

**Dossier** : Espace de stockage sur le disque dur, destiné à classer les fichiers. Un dossier peut contenir des sous-dossiers.

**Download** (Ang.) : Téléchargement.

**Écran tactile** : Écran muni d'un dispositif qui permet de sélectionner certaines de ses zones par contact.

**Editeur** : Programme de création et de modification de textes En ligne : Se dit d'un matériel informatique lorsqu'il fonctionne en relation directe avec un autre, ou d'un système informatique accessible par un réseau Excel : Programme de calcul ou tableur

**Fenêtre** : Partie d’un écran utilisée par un programme Windows

**Fichier** : Entité de stockage d’informations Font : Police de caractères Fusionner : Réunir en un exemplaire les éléments de plusieurs ensembles.

**Freeware** : Logiciel diffusé gratuitement.

**Hyperlien** : Zone de texte contenant une référence à un autre emplacement du même texte ou d’un autre texte.

**Icône** : Sur un écran, symbole graphique qui représente une fonction ou une application logicielle particulière que l'on peut sélectionner et activer à partir d'un dispositif tel qu'une souris

**Internet** : Réseau mondial reliant des millions d’ordinateurs.

**JPEG** : Format graphique utilisé sur Internet.

**Ko** : Abréviation de kilo-octet = 1024 octets.

**Logiciel** : Ensemble des programmes, procédés et règles, et éventuellement de la documentation, relatifs au fonctionnement d'un ensemble de traitement de données.

**Matériel** : Désigne la partie physique de l’ordinateur.

**Mémoire** : Organe qui permet l'enregistrement, la conservation et la restitution de données

**Mémoire morte** : Mémoire dont le contenu ne peut être modifié en usage normal.

**Mémoire tampon** : Mémoire ou partie de mémoire permettant le stockage temporaire de données entre deux organes ayant des caractéristiques différentes.

**Mémoire vive** : Mémoire dont le contenu peut être modifié en usage normal.

**Messagerie électronique** : Service géré par ordinateur fournissant aux utilisateurs habilités les fonctions de saisie, de distribution et de consultation différée de messages, notamment écrits, graphiques ou sonores.

**Microprocesseur** : Processeur miniaturisé dont tous les éléments sont, en principe, rassemblés en un seul circuit intégré.

**Multimédia** : Désigne plusieurs formes de fichiers, audio, vidéo, animations, etc.

**Navigateur** : Programme de navigation et de visualisation de pages web.

**Numérique** : Se dit, par opposition à analogique, de la représentation de données ou de grandeurs physiques au moyen de caractères - des chiffres généralement - et aussi des systèmes, dispositifs ou procédés employant ce mode de représentation discrète.

**Numériser** : Représenter un signal (caractère, image, impulsion, etc.) sous forme numérique Octet : Ensemble de 8 bits.

**Processeur** : Organe destiné, dans un ordinateur ou une autre machine, à interpréter et à exécuter des instructions.

**Raccourci clavier** : Touche ou combinaison de touches du clavier qui peut se substituer à une séquence de commandes plus complexes

**Souris** : Dispositif de commande tenu à la main, connecté à un ordinateur et dont le déplacement sur une surface entraîne le déplacement d'un repère sur l'écran.

**Surbrillance** : Marquage par une luminosité plus grande.

**Shareware** : Programme utilisé gratuitement mais dont l’utilisation courante impose le versement d’une redevance.

**Système d'exploitation** : Logiciel gérant un ordinateur , indépendant des programmes d'application mais indispensable à leur mise en oeuvre.

**Système de gestion de base de données** : Pour une base de données, logiciel permettant d'introduire les données de les mettre à jour et d'y accéder.

**Tableur** : Logiciel de création et de manipulation interactives de tableaux numériques. **Télémaintenance** : Maintenance d'une unité fonctionnelle, assurée par télécommunication directe entre cette unité et un centre spécialisé.

**Télécharger** : Action de transférer un fichier d’un serveur vers un ordinateur.

**Traitement de texte** : Ensemble des opérations de création, manipulation et impression de texte effectuées à l'aide de moyens électroniques.

**Tutoriel** : Initiation guidée à l'utilisation d'un ensemble de notions ou d'une technique.

**USB** : Universal Sérial Bus : technologie de connexion de périphériques à des ordinateurs.

**Version** : Logiciel contenant de nouvelles fonctions modifiant un logiciel plus ancien.

**Virus** : Programmes exécutant généralement des opérations dangereuses pour les données des ordinateurs et se reproduisant de manière autonome.

**Window** (Ang.) Fenêtre. W.W.W.

**Windows** : Famille de système d’exploitation de Microsoft.

**WWW** : Word Wide web, autre nom de la Toile.

:::