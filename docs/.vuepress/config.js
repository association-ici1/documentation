const fs = require("fs")
const path = require("path")
const glob = require("glob")

// Script from https://blog.howar31.com/vuepress-blog-tutorial/#enable-navbar-and-sidebar
/**
 * ['./docs/collection1/a.md', './docs/collection1/b.md', './docs/collection1/README.md']
 * =>
 * ['collection1/a.md', 'collection1/b.md']
 **/
function generateSidebar(markdownPaths) {
  // console.log(markdownPaths)
  const res = markdownPaths
    .map(
      filePath =>
        filePath.substring(basePath.length, filePath.lastIndexOf("/")) +
        "/" +
        path.basename(filePath)
    )
    .filter(f => {
      return !f.match(/README.md/i) // removing README entries that makes sidebar crash
    })
  console.log(res)
  return res
}

const basePath = "./docs/"
// List all md files
let mdFilesPaths = glob.sync(basePath + "/*/*.md")
let sideBar = generateSidebar(mdFilesPaths)

module.exports = {
  title: "ICI Doc",
  temp: "tmp/",
  description: "ICI Doc",
  themeConfig: {
    docsDir: "docs",
    nav: [
      { text: "Association ICI", link: "https://associationici.fr/" },
      { text: "Hub Francil'IN", link: "https://francilin.fr/" },
      { text: "Admin", link: "https://doc.associationici.fr/admin" },
      { text: "Source", link: "https://gitlab.com/association-ici1/documentation" },
    ],
    sidebarDepth: 2,
    sidebar: [
      {
        title: "Découverte de l'ordinateur",
        path: "/collection1/",
        collapsable: true, // optional, defaults to true
        sidebarDepth: 4, // optional, defaults to 1
        children: generateSidebar(
          glob.sync(basePath + "/collection1" + "/*.md")
        ),
      },
      {
        title: "Découverte du smartphone",
        path: "/collection2/",
        collapsable: true, // optional, defaults to true
        sidebarDepth: 4, // optional, defaults to 1
        children: generateSidebar(
          glob.sync(basePath + "/collection2" + "/*.md")
        ),
      },
      {
        title: "Emploi",
        path: "/collection3/",
        collapsable: true, // optional, defaults to true
        sidebarDepth: 4, // optional, defaults to 1
        children: generateSidebar(
          glob.sync(basePath + "/collection3" + "/*.md")
        ),
      },
      {
        title: "Bureautique",
        path: "/collection4/",
        collapsable: true, // optional, defaults to true
        sidebarDepth: 4, // optional, defaults to 1
        children: generateSidebar(
        glob.sync(basePath + "/collection4" + "/*.md")
      ),
      },
      {
        title: "e-Administration",
        path: "/collection5/",
        collapsable: true, // optional, defaults to true
        sidebarDepth: 4, // optional, defaults to 1
        children: generateSidebar(
        glob.sync(basePath + "/collection5" + "/*.md")
      ),
      },
      {
        title: "Usages professionnels",
        path: "/collection6/",
        collapsable: true, // optional, defaults to true
        sidebarDepth: 4, // optional, defaults to 1
        children: generateSidebar(
        glob.sync(basePath + "/collection6" + "/*.md")
      ),
      },
      {
        title: "Cultures numériques",
        path: "/collection7/",
        collapsable: true, // optional, defaults to true
        sidebarDepth: 4, // optional, defaults to 1
        children: generateSidebar(
        glob.sync(basePath + "/collection7" + "/*.md")
      ),
      },
      {
        title: "GEN",
        path: "/collection8/",
        collapsable: true, // optional, defaults to true
        sidebarDepth: 4, // optional, defaults to 1
        children: generateSidebar(
        glob.sync(basePath + "/collection8" + "/*.md")
      ),
      },
      {
        title: "ℹ️ Aide",
        path: "/aide/",
        collapsable: true, // optional, defaults to true
        sidebarDepth: 4, // optional, defaults to 1
        children: generateSidebar(
          glob.sync(basePath + "/aide" + "/*.md")
        ),
      },
    ],
    // lastUpdated: "Dernière modification",
  },
  plugins: [
    [
      "container",
      {
        // custom container (https://gitlab.com/association-ici1/documentation-/issues/5)
        type: "work",
        before: (info) => `<div class="work"><p class="title">${info}</p>`,
        after: "</div>",
      },
    ],
  ],
}
