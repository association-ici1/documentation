---
title: 📟 Tableur
---
# Tableur

::: tip Modules APTIC

* *70: Tableur : découverte et utilisation de base.*
* **Résumé** 

Un tableur est destiné à manipuler des données organisées dans des grilles formées de lignes et de colonnes. Plus largement il offre une multitude d’opportunités. Pour un particulier, la maîtrise d’un logiciel lui permet aussi de maximiser ses chances lors d’un recrutement. Les entreprises sont souvent en recherche de professionnels qualifiés maîtrisant cet outil basique. Cette formation a pour ambition de vous faire connaître ce logiciel et de vous en donner les bases.​

* **Nombre d’heures du module** : 3h00
* **Public** : Module « Débutants »
* **Prérequis** : Maîtrise du français lu et parlé / Avoir un accès internet et un ordinateur.
* **Objectifs de l’atelier** :

  * D'introduire la notion de tableur​
  * De se familiariser avec l'outil
  * Maîtriser les fonctionnalités de base

:::

**Sommaire:**

[[toc]]

## 1.INTRODUCTION

### 1.Définition

> Le tableur est un logiciel utilisé en bureautique et dans les applications scientifiques. 
> Il est spécialisé dans le traitement des données, essentiellement numériques. 
> Le tableur offre une série de feuilles de calculs. 
> Ces feuilles présentent un grand nombre de cellules dans lesquelles il est possible de déposer des nombres, des textes, des formules etc. puis de réaliser des graphiques de toutes sortes. 
> C'est également l'outil tout désigné pour une première interprétation des données issues de l'expérience scientifique. 
> Tous les tableurs disponibles actuellement proposent les fonctions qui seront décrites dans ce module.

### 2.Qui sont-ils ?

Lors de cette séance, nous vous présenterons plusieurs logiciels dits tableurs comme le logiciel OpenOffice.org Calc qui présente l'avantage d'être libre et gratuit. Il est disponible au téléchargement à partir de l'adresse <http://fr.openoffice.org/about-downloads.html>. A noter qu’il existe des versions pour Windows, Mac et Linux.
Un autre logiciel tableur bien connu est Microsoft Excel. Il n'est quant à lui pas gratuit. Il est un logiciel de la suite Microsoft Office. Il est un programme de tableur simple. Il dispose de nombreuses formules susceptibles d’apporter des solutions aussi bien aux particuliers qu’aux entreprises.
C’est un logiciel tableur dans sa définition simple. Il propose des fonctions de calcul numérique, d’analyse de données, de représentation graphique et de programmation. Le logiciel permet donc d’effectuer des opérations comme des calculs simples tels l’addition ou la soustraction à des calculs plus complexes comme la trigonométrie. Ces différentes fonctions sont nécessaires à la mise en place de solutions diverses tant chez les particuliers que chez les entreprises.

### 3.Pour quelle utilité ?

Pour les professionnels, une entreprise disposant d’employés maîtrisant parfaitement un tableur gagne tout d’abord en productivité. Le second avantage qu’a une entreprise à ce que ses employés fassent une utilisation parfaite d’un tableur réside dans l’efficacité et la précision des résultats obtenus. Avec un tableur, une société peut donc dresser facilement de nombreux graphiques. Ceux-ci lui permettent d’avoir une visualisation de données chiffrées. Vous pouvez donc aisément analyser les données et faire des projections à partir de celles-ci.
Pour les particuliers, le tableur dispose d’une interface assez simple et facile d’utilisation. Vous pouvez donc modeler les différents tableaux et colonnes selon vos besoins. Les particuliers n’auront pas vraiment une grande difficulté à l’utiliser. Il est en effet simple à installer et facile à utiliser. Pour un particulier, la maîtrise d’un logiciel lui permet aussi de maximiser ses chances lors d’un recrutement. Les entreprises sont souvent en recherche de professionnels qualifiés, mais ne sont toujours pas disposées à payer pour leur assurer certaines formations qu’elles considèrent comme basiques.

## 2.LES FONCTIONNALITÉS DE BASE

### 1.Comment ouvrir un tableur

**Pour Excel**

Cliquez successivement sur Fichier >Ouvrir > Ordinateur > Rechercher.

1. Pour afficher uniquement les fichiers enregistrés au format OpenDocument, dans la liste des types de fichiers (en regard de la zone Nom de fichier), cliquez sur Feuille de calcul OpenDocument (*.ods).
2. Cliquez sur le fichier de votre choix, puis sur Ouvrir.

Vous pouvez également utiliser directement le menu démarrer/ programmes/ Microsoft Office/Excel

***Pour Libre Office Calc***

Vous pouvez utiliser directement le menu démarrer/ programmes/ LibreOffice/ Calc
Ou Choisissez Fichier - Ouvrir.

1. Sélectionnez un format dans la liste Type.
2. Sélectionnez un nom de fichier et cliquez sur Ouvrir.

![Ouverture du tableur](/upload/ouverture-page.png "Ouvrir LibreOffice Calc")

### 2.Comment fermer un tableur

Après avoir enregistré son travail, vous avez trois choix possibles :

![Fermeture de Calc](/upload/fermer-libreoffice.jpg "Fermer LibreOffice  Calc")

### 3.Structure de l’interface d’un tableur

Composition d’un classeur de tableur : un tableur produit des "classeurs"
​
    • Un classeur contient des "feuilles"​
    • Les feuilles sont composées de "cases" ( lignes + colonnes)​
    • Les cases sont appelées "cellules" »

### 4.La fenêtre commune des tableurs

![](/upload/fenetre-commune-des-tableurs.png)

Lorsque deux outils informatiques ont plus ou moins la même base, on a tendance à croire qu’ils sont pareils. C’est précisément ce qui est arrivé avec Open Office Calc et Microsoft Excel, on en arrive à se demander quelle est la différence entre les deux outils. Cette observation n’échappe pas à la première vision que nous avons des deux tableurs. En effet :

**Pour Excel** :

![](/upload/fenetre-commune.png)

Le logiciel se compose donc de deux fenêtres imbriquées l’une dans l’autre. Ainsi, la fenêtre contenant un classeur peut être fermée ou redimensionnée sans que la fenêtre générale ne change d’aspect.

***Sous la barre de menus***, on trouve la barre d’outils, qui affiche les commandes les plus utilisées d’Excel.
Juste en-dessous, la barre d’accès rapide contient par défaut trois boutons de commande permettant d’enregistrer le classeur, d’annuler une commande ou de rétablir l’effet d’une commande annulée.

***Encore en-dessous***, la barre de formule permet de saisir et de modifier les formules.
La zone quadrillée équivaut à l’espace de travail, en l’occurrence une feuille de calcul découpée en lignes et en colonnes.

Deux barres de défilement placées sur la droite et en bas de l’écran permettent de se déplacer sur la diapositive si celle-ci n’apparaît pas en entier sur l’écran de l’ordinateur. Il y a aussi un zoom situé tout en bas à droite.
En bas, on trouve des onglets qui permettent de se déplacer dans les différentes feuilles de calcul du classeur. La feuille affichée à l’écran porte, par défaut, le nom : feuil1.

***Enfin, tout en bas***, la barre d’état donne des informations liées à ce que l’utilisateur est en train de faire.

![](/upload/fenetre-excel.png)

![]()

**Pour LibreOffice Calc**

L’affichage se compose de trois parties, la barre de menus, la barre d’outils et les onglets.

**Barre des menus** En dessous de la Barre de titre se trouve la Barre de menus. Quand vous choisissez l'un des menus, un sous-menu apparaît avec d'autres options.

**Fichier** contient des commandes qui s'appliquent au document entier, comme Ouvrir, Enregistrer, Modèles, Exporter au format PDF et Signatures numériques.

**Édition** contient des commandes pour modifier le document, comme Annuler, Modifications, Comparer le document et Rechercher & remplacer.

• Affichage contient des commandes pour modifier l'apparence de l'interface utilisateur de Calc, comme Barres d'outils, Plein écran et Zoom.

• Insertion contient des commandes pour insérer des éléments, comme des cellules, des lignes, des colonnes, des feuilles et des images dans un classeur.

• Format contient des commandes pour modifier la présentation d'un classeur, comme Styles et formatage, Cellules et Fusionner les cellules.

• Outils contient des fonctions, comme Orthographe, Partager le document, Contenu des cellules

• Données contient des commandes pour manipuler les données dans votre classeur, comme Définir la plage, Trier, Filtre et Table de pilote.

• Fenêtre contient des commandes pour la fenêtre d'affichage, comme Nouvelle fenêtre, Scinder et Fixer.

**Barre d’outils**
Calc comporte plusieurs types de barres d'outils: ancrée (à place fixe), flottante et détachable. Les barres d'outils ancrées peuvent être déplacées à différents endroits ou rendues flottantes, et les barres d'outils flottantes peuvent être ancrées. Quatre barres d'outils sont situées sous la Barre de menus par défaut : la barre d'outils Standard, la barre d'outils Rechercher, la barre d'outils Formatage et la Barre de formules.

**Onglets** 
En dessous de la grille des cellules se trouvent les onglets. Ces onglets permettent l'accès à chaque feuille individuelle, la (les) feuille(s) visible(s) (actives) ayant un onglet blanc. Cliquer sur un autre onglet permet d'afficher cette feuille et son onglet devient blanc. Vous pouvez également sélectionner de multiples feuilles en une fois en gardant la touche Ctrl appuyée pendant que vous cliquez sur les noms.

### 5. Comment enregistrer son travail

**Pour Excel** :

Quel que soit l’emplacement où vous enregistrez votre classeur (sur votre ordinateur ou sur le web, par exemple), toutes les opérations d’enregistrement s’effectuent à partir de l’onglet Fichier.
Tandis que la commande Enregistrer (ou le raccourci Ctrl+S) vous permet d’enregistrer un classeur existant à son emplacement actuel, vous devrez utiliser Enregistrer sous pour enregistrer votre classeur pour la première fois, à un emplacement différent, ou pour en créer une copie à un emplacement identique ou différent. 

Voici comment procéder :

• Cliquez sur Fichier > Enregistrer sous.

![](/upload/enregistrer.png)

**Pour Calc** :

 La procédure est la même que pour le logiciel Microsoft Excel. Pour rappel, vous devez procéder de la manière suivante :
    • Cliquez sur Fichier > Enregistrer sous.
    • Sous Enregistrer sous, sélectionnez l’emplacement où enregistrer votre classeur. 

![](/upload/enregistrer-calc.png)

Par exemple, pour enregistrer sur votre Bureau ou dans un dossier sur votre ordinateur, cliquez sur Ordinateur.

• Cliquez sur Parcourir pour trouver l’emplacement que vous voulez dans votre dossier Documents.

• Pour choisir un autre emplacement sur votre ordinateur, cliquez sur Bureau, puis choisissez l’endroit exact où vous voulez enregistrer votre classeur.

• Dans la zone Nom de fichier, tapez un nom pour votre classeur. Entrez un nom différent si vous créez une copie d’un classeur existant.

• Cliquez sur Enregistrer.

### 6.Insérer une feuille de calcul

Cette action est commune sur les deux tableurs et se déroule de la manière suivante :

Sélectionnez l’icône (Plus) au bas de l’écran.

 • Vous pouvez également sélectionner Accueil > Insertion > Insérer une feuille.

![](/upload/insérer-une-feuille-de-calcul.png)

### 7.Renommer une feuille de calcul

Cette action est commune sur les deux tableurs et se déroule de la manière suivante :

### 8.Supprimer une feuille de calcul

Cette action est commune sur les deux tableurs et se déroule de la manière suivante :

### 9.Sélectionner des cellules

Cette action est commune sur les deux tableurs et se déroule de la manière suivante :

La sélection d’une ou de plusieurs cellules est un préalable à l’exécution
d’une action sur cette ou ces cellules.

![](/upload/sélectionner-des-cellules.png)

![](/upload/sélectionner-des-cellules-2.png)

### 10.Sélectionner une plage de cellules

Cette action est commune sur les deux tableurs et se déroule de la manière suivante :

Il suffit tout simplement de sélectionner plusieurs cellules en restant appuyé sur le clic gauche de la souris. Vous pouvez sélectionner autant de cellules que vous le souhaitez :

![](/upload/sélectionner-une-plage-de-cellules.png)

![](/upload/sélectionner-une-plage-de-cellule-2.png)

### 11.Utiliser une référence de cellule

Cette action est commune sur les deux tableurs et se déroule de la manière suivante :

Cliquez sur le petit triangle noir renversé situé juste à droite de la Zone de nom. La référence de cellule actuelle sera en surbrillance. Tapez la référence de la cellule où vous voulez aller et appuyez sur Entrée. Les références de cellules sont insensibles à la case : a3 ou A3, par exemple, sont identiques. Ou cliquez simplement dans la Zone de nom, faites un retour arrière sur la référence de cellule existante, tapez la référence de cellule que vous voulez et appuyez sur Entrée.

Exercice : Saisir des informations simples : Saisir du texte : Cliquez sur une cellule, saisir du texte puis valider ! 

### 12.Saisir des informations simples

Exercice : Continuons, modifier le contenu d'une cellule : Cliquez sur une cellule, modifier le texte puis valider ! 

### 13.Manipulation des cellules

Cette action est commune sur les deux tableurs. Nous procédons de la manière suivante :

**Sélectionner plusieurs cellules disjointes**: sélectionner chaque cellule en maintenant la touche ctrl enfoncée ;

![](/upload/sélectionner-une-ou-plusieurs-cellules-disjointes.png)

**Supprimer une ou plusieurs cellules**: sélectionner la ou les cellules, appuyer sur la touche Suppr.
**Sélectionner toute la feuille** : Cliquez sur le rectangle situé à gauche de la colonne « A »

![](/upload/sélectionner-une-ou-plusieurs-disjointes-2.png)

**Insérer une ligne/colonne:** sélectionner l’entête de la ligne/colonne, appuyer sur le bouton de droite (menu contextuel), sélectionner Insérer.

**Sélectionner une ligne** : sélectionnez un numéro de ligne et cliquez dessus.

![](/upload/sélectionner-une-ligne.png)

**Sélectionner une colonne** : sélectionnez une lettre de colonne et cliquez dessus.

![](/upload/sélection-colonne.png)

### 14.Modifier la largeur d'une colonne

Vous disposez de plusieurs méthodes pour effectuer cette opération. Vous pouvez :

– positionnez le curseur sur la ligne verticale à droite de la lettre numérotant la colonne dont vous souhaitez modifier la taille ;
– cliquez sur le bouton gauche de la souris sans le relâcher, et déplacez le
curseur vers la droite ou la gauche jusqu’à ce que vous obteniez la largeur désirée ;
– relâchez le bouton de la souris.
– utiliser l’option Format du menu Accueil ainsi :
– sélectionnez d’abord la colonne à modifier ;
– sélectionnez le menu Accueil, choisissez l’option Format puis l’option Largeur de colonne et indiquez dans la boite de dialogue qui s’ouvre une largeur désirée.

Ou

Vous pouvez aussi faire en sorte que la largeur d’une colonne soit calculée au plus juste et s’adapte exactement à la largeur de l’intitulé le plus long contenu dans une cellule de cette colonne. Pour ce faire, cliquez deux fois rapidement sur la ligne verticale à droite de la lettre numérotant la colonne dont vous souhaitez modifier la taille.

### 15.Modifier la hauteur d’une ligne

Vous disposez, là aussi, de plusieurs méthodes pour effectuer cette opération. Vous pouvez, soit :
    • utiliser la souris ainsi :
– positionnez le curseur sur la ligne horizontale en dessous du nombre
représentant la ligne dont vous souhaitez modifier la taille ;
– cliquez sur le bouton gauche de la souris sans le relâcher, et déplacez le
curseur vers le bas ou le haut jusqu’à ce que vous obteniez la hauteur désirée ;
– relâchez le bouton de la souris.
Ou
    • utiliser l’option Format du menu Accueil ainsi :
– sélectionnez d’abord la ligne à modifier ;
– sélectionnez le menu Accueil, choisissez l’option Format puis l’option Hauteur de ligne et indiquez dans la boite de dialogue qui s’ouvre une hauteur désirée exprimée en « points ».
Formater des cellules
Le tableur vous donne la possibilité de modifier les attributs d’affichage du contenu des cellules de votre modèle. Certains attributs s’appliquent à tout type de contenu, qu’il s’agisse de textes ou de valeurs numériques. C’est le cas lorsque l’on souhaite changer l’alignement, la police de caractères utilisée, la forme des caractères, par exemple en gras, italique etc. D’autres attributs ne s’appliquent qu’aux valeurs numériques et permettent, par exemple, de faire en sorte que toute valeur numérique s’affiche systématiquement avec deux chiffres derrière la virgule, soit suivie du symbole €
Pour formater des cellules, procédez ainsi :
– sélectionnez les cellules pour lesquelles vous souhaitez modifier
l’apparence;
– sélectionnez le menu Accueil et cliquez sur l’une des flèches située en bas à droite des panneaux Police, Alignement ou Nombre. Une boite de dialogue, comme celle indiquée ci-après, apparaît à l’écran.
Faire des bordures
	Appliquer des bordures de cellules prédéfinie

Sur une feuille, sélectionnez la cellule ou la plage de cellules dans laquelle vous souhaitez ajouter ou modifier les bordures.
    1. Dans l'onglet Accueil , dans le groupe police , cliquez sur la flèche en regard de bordures  , puis cliquez sur la bordure de la cellule que vous voulez appliquer

Conseil : Pour ajouter ou supprimer des parties d'une bordure, cliquez sur autres bordures en bas du menu. Sous bordure, cliquez sur les bordures que vous voulez ajouter ou supprimer.
	Supprimer toutes les bordures

Sélectionnez la cellule ou la plage de cellules dans laquelle vous voulez supprimer les bordures.

### 16.Figer une cellule

Cela consiste à indiquer que le calcul doit se faire en prenant toujours la même cellule pour référence.
Par exemple, lorsque vous devez appliquer un même coefficient à une colonne, plutôt que de multiplier vos cellules à chaque fois avec la même valeur, vous allez pouvoir appliquer ce coefficient à toutes les cellules de vos colonnes.
Il faut donc figer la cellule C16 pour indiquer au tableur qu’il faut utiliser cette cellule pour toute la colonne. Pour ce faire, il faut :

* se placer dans la cellule à figer
* la cellule est alors mise en surbrillance et nous devons y appliquer soit le signe $ soit la touche F4.
  Une astuce, vous pouvez également figer une ligne, en plus d’une colonne. Pour ce faire il faut comprendre la logique suivante :
* une fois : $B$X
* deux fois : B$X
* trois fois : $BX

### 17.Les raccourcis claviers

**Cette action est commune sur les deux tableurs et se déroule de la manière suivante** :

*Les raccourcis ne sont utilisables uniquement s’ils sont simples. En effet, un raccourci doit être*:

A l’inverse, si le raccourci est trop complexe à exécuter (beaucoup de touches et/ou des touches trop éloignées les unes des autres), ou bien s’il permet d’exécuter une commande dont vous vous servez très rarement, inutile de s’encombrer la mémoire avec.

**Ce tableau répertorie les raccourcis claviers les plus fréquemment utilisés et leurs utilisations premières** :

![](/upload/capture-d’écran-du-2020-07-13-15-29-38.png)

**Les raccourcis clavier suivants sont strictement identiques dans Excel et Calc** :

![](/upload/rac.png)

Vous avez pu constater qu’Excel et Calc fournissent des raccourcis clavier qui peuvent réellement améliorer votre productivité.
Il est probable que vous connaissiez déjà certains d’entre eux. Nous vous encourageons vraiment à essayer ceux que vous ne connaissiez pas.

### 18.Savoir comment imprimer

Pour imprimer rapidement un document, cliquez sur « Fichier » puis « Imprimer ».​
​Si votre document est au format paysage, modifiez l'orientation.​
​Si nécessaire, modifiez les marges et/ou utilisez les options d'ajustement.​
​Imprimez ensuite votre document.

​

![](/upload/imprimer-votre-document.jpg)

​
Que ce soit pour Cal ou Excel, une impression rapide est possible en utilisant le bouton suivant situé en haut du document:  

Pour imprimer uniquement une sélection, sélectionnez la zone et cliquez sur "Mise en page" « ZoneImpr » puis « Définir ».

![](/upload/imprim2.png)

### 19.Mettre en page un modèle de calcul

Si vous souhaitez imprimer une feuille, cliquez sur le menu Fichier, puis sélectionnez l’option Imprimer.

![](/upload/mettre-en-page.png)

**Pour définir les paramètres de mise en page**, cliquez sur l’option Mise en page, indiquée ci-dessus Cette option permet de définir les attributs d’impression d’une feuille de calcul en choisissant la taille des marges, l’orientation du papier ou la position d’un en-tête ou d’un pied de page par rapport à ces marges.

**L’onglet Page** vous donne la possibilité de sélectionner l’orientation d’une page de façon à pouvoir imprimer un tableau plus haut que large, Portrait, ou plus large que haut Paysage. Cette rubrique vous permet aussi de définir un coefficient d’agrandissement ou de réduction selon que le tableau à imprimer es très petit ou très grand.

**L’onglet Marges** vous permet de modifier la taille des marges, Gauche, Droite, Haut et Bas et la distance qui sépare soit un en- tête, soit un pied de page, respectivement du haut ou du bas d’une feuille de papier. Les valeurs par défaut sont réglées à 1,30 cm, ce qui signifie que la distance qui sépare le haut de la feuille de la première ligne de votre en-tête est de 1,30 cm et celle qui sépare la dernière ligne de votre bas de page du bas de la feuille est de 1,30 cm.

**L’onglet En-tête/Pied de page** vous permet de définir un ensemble d’intitulés qui doivent être imprimés sur toutes les pages. Pour cela, vous pouvez utiliser les options des listes déroulantes En-tête et Pied de page pour choisir un intitulé prédéfini, ou utiliser les boutons En-tête personnalisé ou Pied de page personnalisé pour définir vous-même les intitulés à imprimer.

**Enfin, l’onglet Feuille vous permet de définir** :

La Zone d’impression, c’est-à-dire la partie du tableau à imprimer si vous ne souhaitez pas imprimer sa totalité : pour indiquer une zone, cliquez dans la zone de texte à droite de cette rubrique et sélectionnez avec la souris, dans la feuille de calcul, la plage de cellules à imprimer

* les Titres à imprimer, c’est-à-dire les intitulés du tableau qui devront apparaître systématiquement soit en haut soit à gauche de chaque page ;
  – les paramètres de l’Impression vous permettant notamment de quadriller le tableau ou d’imprimer les numéros de lignes et de colonnes de votre feuille de calcul.

## 3.LES FONCTIONNALITÉS GRAPHIQUES

### 1.Création d’un graphique

Vous pouvez représenter, sur un même graphique, une ou plusieurs séries de données. Selon que vous choisirez de représenter sur un même graphique une ou plusieurs séries, vous n’aurez pas accès aux mêmes types de graphiques. Ainsi est-il impossible de réaliser un graphique en secteurs à partir de deux séries.
Pour réaliser un graphique, vous devrez procéder ainsi :

– si vous souhaitez que le graphique comporte des intitulés en abscisse, sélectionnez au préalable les cellules qui les contiennent. 

Note : Si ces cellules ne sont pas situées immédiatement à côté des cellules contenant les valeurs numériques, appuyez sur la touche Ctrl du clavier et, sans la relâcher, sélectionnez la série contenant les valeurs numériques ;

– cliquez ensuite sur le menu Insertion, et dans le panneau Graphique, choisissez le type de graphique désiré : Colonne, Ligne, Secteurs... et choisissez le type désiré. Si vous optez pour l’option Tous types de graphiques, la boite de dialogue suivante apparaît alors à l’écran.

![](/upload/graphique.png)

### 2.Modifier la taille du graphique

Un graphique réalisé par le tableur est toujours subdivisé en au moins deux parties. La première, qui est délimitée par un cadre au contour noir, est nommée la Zone de graphique. Cette zone permet de sélectionner l’intégralité du graphique pour le déplacer dans la feuille de calcul ou pour modifier sa taille. La deuxième, intitulée Zone de traçage, permet de déplacer ou de modifier la taille du graphique sans affecter la position et la taille de la Zone de graphique. Les autres zones sont utilisées pour afficher le titre du graphique, les titres des axes, la légende...
Pour déplacer une zone, il suffit de cliquer sur le pourtour de la zone voulue et, sans relâcher le bouton de la souris, de déplacer la zone à l’endroit désiré.
Pour agrandir ou rétrécir une zone, il convient de sélectionner la zone puis de cliquer sur l’un des petits carrés noirs qui sont situés sur le pourtour de la zone et, toujours sans relâcher le bouton de la souris, d’étendre ou de rétrécir la sélection.
Les différentes zones d’un graphique sont présentées sur l’écran ci- dessous :
    • Zone de graphique
    • Zone de traçage

### 3.Modifier le contenu du graphique

Si vous souhaitez modifier une partie du graphique, cliquez deux fois rapidement sur cette partie. Dans tous les cas de figure, une boite de dialogue apparaîtra et vous proposera des options de modification adaptées à la partie désignée.
Modifier la présentation des axes
Si votre graphique comporte des axes et si vous souhaitez modifier l’apparence des intitulés appartenant à l’un d’entre eux, sélectionnez les intitulés des abscisses ou des ordonnées, puis cliquez le sur menu Accueil, et choisissez les options de mise en forme pour modifier, par exemple, la police. Vous pouvez aussi modifier certains autres paramètres, comme l’unité des axes (milliers, millions...). Pour cela, cliquez sur les valeurs en abscisse ou en ordonnées, effectuez un clic droit sur ces valeurs, et choisissez l’option Mise en forme de l’axe.

### 4.Modifier l’apparence du fond

Si votre graphique comporte un fond et si vous souhaitez modifier son apparence, cliquez sur la partie du fond qui vous intéresse, puis effectuez un clic droit à cet emplacement, et choisissez l’option Mise en forme des murs.

### 5.Modifier l’apparence du graphique

Si vous souhaitez un histogramme, modifier l’apparence d’une courbe, clic-droit sur l’élément d’une série considérée.
Plusieurs options vous seront alors proposées afin de modifier, entre autres, le type de graphique, d’effectuer une rotation 3D, d’ajouter des étiquettes etc.

### 6.Modifier la présentation des axes

Si votre graphique comporte des axes et si vous souhaitez modifier l’apparence des intitulés appartenant à l’un d’entre eux, sélectionnez les intitulés des abscisses ou des ordonnées, puis cliquez le sur menu Accueil, et choisissez les options de mise en forme pour modifier, par exemple, la police. Vous pouvez aussi modifier certains autres paramètres, comme l’unité des axes (milliers, millions...). Pour cela, cliquez sur les valeurs en abscisse ou en ordonnées, effectuez un clic- droit sur ces valeurs, et choisissez l’option Mise en forme de l’axe. La boite de dialogue suivante apparaît alors à l’écran.

## 4.LES FONCTIONNALITÉS DE CALCUL

**Modalité de création d’une formule de calcul**

Les formules de calcul commencent toujours par le symbole « = » afin que le logiciel « tableur » sache qu’il s’agit d’une formule et non d’un texte quelconque. Ces formules peuvent contenir des nombres, des coordonnées d’autres cellules, des fonctions prédéfinies dans le tableur, des opérateurs mathématiques. Dans le cas où le tableur ne comprendrait pas ce que vous avez saisi, vous verrez alors apparaître un message d’erreur dans la cellule en question.
Lorsque vous souhaitez élaborer des formules complexes, vous devez faire attention à l’ordre de priorité des opérateurs.
Les opérateurs de formules les plus utilisés

**Voici la liste des opérateurs les plus utilisés dans les formules**.

![](/upload/calculs.jpg)

### 1.Construction d’une formule

![](/upload/focus.png)

**Quelle signification ?**

Afin de simplifier la construction des formules, le tableur dispose d’un certain nombre de formules prédéfinies appelées fonctions. La fonction SOMME(), par exemple, a pour rôle d’additionner toutes les cellules dont les coordonnées sont indiquées entre parenthèses. Cette fonction peut être utilisée aussi bien des valeurs numériques qu’avec des coordonnées de cellules.
Exemples :
=SOMME(A1:A10) =SOMME(A1;A10) =SOMME(A1;A3;A5:A9)
=SOMME(A1:A3;A5:A9)
En revanche, si l’on souhaite additionner le contenu de cellules non contiguës, on utilisera comme séparateur le « ; » correspondant au point-virgule.
Pour éviter de saisir au clavier des coordonnées de cellules dans une formule, le tableur vous donne la possibilité de les insérer automatiquement. Pour cela, lorsque vous êtes en train de définir une formule, cliquez sur la cellule dont les coordonnées doivent être introduites dans la formule. De la même manière, vous pouvez insérer automatiquement un intervalle de cellules, en sélectionnant, au cours de la définition de la formule, les cellules en question.

### 2.Les fonctions usuelles

![](/upload/fonctions-usuelles.png)

![](/upload/si.png)

![](/upload/moyenne.png)

### 3.Les fonctions statistiques

![](/upload/fonctions-stats.png)

### 4.Les fonctions de recherche et référence

![](/upload/fonctions-recherche.png)

### 5.Les fonctions texte

![](/upload/fonctions-texte.png)

**Comment recopier une formule**

Pour effectuer une recopie de formule, le processus général est le suivant :

– sélectionnez la cellule à copier ;

– exécutez la commande Copier ;

– sélectionnez la plage de cellules où vous souhaitez effectuer la copie ;

– exécutez la commande Coller.

Maintenant, vous disposer de plusieurs façons pour exécuter les commandes Copier et Coller. Vous pouvez utiliser la barre de menus, en sélectionnant les commandes en question dans le menu Accueil.

Vous pouvez aussi utiliser le menu contextuel associé à la cellule à copier, en cliquant sur cette dernière avec le bouton droit de la souris, ou encore vous pouvez utiliser les touches du clavier, en tapant Ctrl- C pour copier puis Ctrl- V pour coller, ou faire glisser le curseur à partir du coin inférieur droit de la cellule à recopier.

**Focus cas pratique**

• ***Lancez le tableur et créez un nouveau document*** 

 • ***Nommez votre document : « relevé de notes »*** ​

•  ***Dans la première cellule : tapez le titre Relevé de notes ​***

•  ***Insérez une ligne vierge​***

 • ***Insérez un tableau*** de 7 colonnes et 8 lignes, dans la 1ère colonne, tapez Nom des élèves, dans la 2ème, Maths, dans la troisième Français, dans la 4ème Histoire, dans la 5ème Géographie et la 6ème Anglais, tapez ensuite Moyenne dans la 7ème colonne. ​

 • ***Tapez les noms des élèves sous la colonne appropriée ​***

• ***Tapez les notes de chaque élève dans les cellules correspondantes ​***

 •  ***Sélectionnez ensuite la cellule*** qui doit comporter la moyenne (G4) ​

 • ***Tapez la formule dans la zone de formule***, vérifiez la formule dans la barre de formule, dans l’exemple cela doit correspondre à =MOYENNE(B4-G4) ​

• ***Sélectionnez la cellule*** dans laquelle se trouve le résultat (G4) puis cliquez sur la poignée de recopie et glissez-la jusqu’à la dernière cellule.​

• ***Choisissez une forme de tableau*** et enregistrez votre fichier. 

## 5.FONCTIONNALITÉ DE TABLEAUX CROISES DYNAMIQUES

Les tableaux croisés dynamiques servent à manipuler des listes de données. Une liste représente un ensemble de lignes contenant des données similaires – données relatives à un ensemble de produits, aux caractéristiques des clients, au détail des ventes d’une entreprise... Une liste peut être utilisée comme une table d’une base de données, dans laquelle les lignes sont des enregistrements et les colonnes, des champs. On ne peut créer de tableau croisé qu’à partir du moment où la liste contient des séries finies d’intitulés (noms des vendeurs, des clients, mois de l’année etc.) et des valeurs numériques qui peuvent être regroupées en fonction de ces intitulés – prix, bénéfice, chiffre d’affaires…

### 1.Créer un tableau croisé dynamique

**Pour créer un tableau croisé, procédez ainsi** :

– sélectionnez au préalable toutes les cellules appartenant à votre liste, y compris les cellules situées sur la première ligne ;
– sélectionnez le menu Insertion, puis l’option Tableau croisé dynamique, et dans la liste qui s’affiche, cliquez sur Tableau croisé dynamique. La boite de dialogue ci-après apparaît.
Si vous avez correctement sélectionné les cellules contenant la liste sur laquelle vous travaillez, le tableur vous demande de confirmer que ces données se situent bien dans la plage de cellules qu’il vous indique. Si ce n’est pas le cas, sélectionnez-les avec la souris.
– indiquez au tableur la destination du tableau, donc dans quelle feuille celui-ci sera inséré.
Choisissez entre Nouvelle feuille de calcul et Feuille de calcul existante. Si vous avez choisi Feuille de calcul existant, sélectionnez avec la souris la première cellule à partir de laquelle le tableau sera inséré.
– terminez en cliquant sur Ok.

**LIAISON ENTRE LES FEUILLES DE CALCUL**

Les liaisons entre feuilles de calcul permettent de faire apparaître dans une feuille des éléments présents dans une autre sans passer par l’intermédiaire d’un copier-coller. Cette fonction est très utile lorsque l’on souhaite qu’une modification opérée dans une feuille se répercute automatiquement dans une autre.

Les liaisons entre feuilles se font à partir des cellules : une cellule ne peut être liée qu’à une seule autre cellule. Il y a donc toujours deux cellules en jeu dans une liaison : la cellule qui contient la valeur d’origine, et la cellule qui contient un lien vers cette valeur.

**Pour créer ce lien, il faut insérer, dans une cellule, une formule qui aura la
forme suivante**:

\=Nom de la feuille! Coordonnées de la cellule Par exemple : =Budget!A1

## 6.CONCLUSION

Pourquoi utiliser un tableur et continuer son apprentissage jusqu'au perfectionnement ?​
​Le tableur est un outil modifiable et évolutif qui vous permet d’afficher clairement vos données sous forme de tableurs et de graphiques.​
​Les modèles de calcul sont rapides à développer et à modifier.​
​Son utilisation permet à tout particulier de gérer des données relatives à son activité professionnelle par exemple. Il peut servir à gérer les stocks, la comptabilité, à établir une facture​.
​Enfin, il offre une multitude d’opportunités. Il suffit juste d’être bien formé et de maîtriser le logiciel « parfaitement ».​
​Pour un particulier, la maîtrise d’un logiciel lui permet aussi de maximiser ses chances lors d’un recrutement. Les entreprises sont souvent en recherche de professionnels qualifiés, mais ne sont toujours pas disposées à payer pour leur assurer certaines formations qu’elles considèrent comme basiques.

ÉVALUATION

Réussite ou non du/des cas pratique(s)

::: tip SOURCES

<https://support.office.com/fr-fr/article/formation-excel-pour-windows-9bc05390-e94c-46af-a5b3-d7c22f6990bb>

<https://fr.tuto.com/excel/tuto-excel-gratuit.htm>

<https://www.excel-pratique.com/fr/cours.php>

<https://www.labri.fr/perso/hocquard/Files/coursc2i/D1JP/EXCELTUTO_perso.pdf>

<https://openoffice-libreoffice.developpez.com/cours/>

<https://www.openoffice.org/fr/Documentation/Calc/calc140114.pdf>

:::

::: details LEXIQUE DE BASE A CONNAÎTRE

**Activer** : Pour rendre une feuille de graphique ou une feuille de calcul active, ou sélectionné, dans la feuille. La feuille que vous activez détermine les onglets sont affichés. Pour activer une feuille, cliquez sur l’onglet correspondant à la feuille dans le classeur.

**Bandeau** : chaque onglet du ruban est composé d’un bandeau depuis lequel les commandes sont disponibles en cliquant sur un icône ou en utilisant la touche Alt + la lettre correspondante qui s’affiche.

**Cellule** : un tableau est composé de cellules dans lesquelles figurent les données. Il est possible de travailler sur une cellule ou une plage de cellules comprenant alors plusieurs cellules suivant la sélection effectuée. La cellule active est la cellule dans laquelle le curseur est positionné et qui recevra les données saisies. Il convient de toujours vérifier que la cellule active est bien celle sur laquelle vous souhaitez intervenir.

**Cellule active** : La cellule sélectionnée dans lequel les données sont saisies lorsque vous commencez à taper. Une seule cellule est active à la fois. La cellule active est entourée d’une bordure épaisse.

**Classeur** : fichier contenant une ou plusieurs feuilles.

**Clic droit** : la souris possède deux boutons. Le bouton gauche est celui que l’on utilise le plus couramment et le bouton droit sert à ouvrir un menu appelé menu contextuel qui varie en fonction des logiciels et de la fenêtre dans laquelle il est utilisé. Privilégiez l’utilisation des raccourcis clavier à celle des menus contextuels.

**Double clic** : action de cliquer deux fois rapidement avec le bouton gauche de la souris.

**Dossier** : un dossier est un espace de classement dans lequel vous pourrez enregistrer des fichiers.

**Extension** : le nom d’un fichier est composé de son nom défini librement par l’utilisateur suivi d’une extension séparé par un point. L’extension est déterminée par le programme qui sert à créer le fichier. Ainsi un document Excel par exemple aura pour extension .xlsx pour un classeur, .xltx pour un modèle, xlsm pour un classeur contenant des macros.

**Feuille** : une feuille est l’espace de travail qui contient les données sur lesquelles vous pourrez effectuer différents traitements et principalement des calculs. Une feuille peut aussi contenir un graphique. La feuille dont les données sont visibles est la feuille active.

**Fichier** : un fichier est créé à partir d’un programme. Cela peut être un document Calc ou Excel par exemple.

**Flèche de direction** : sur le clavier vous disposez d’un groupe de quatre flèches qui vous permettent de vous déplacer sans la souris ou d’effectuer une sélection dans une liste déroulante.Ces flèches se trouvent à côté du pavé numérique.
Elles permettent aussi en combinaison avec les touches Alt, Maj ou Ctrl d’effectuer des sélections rapides sur des zones étendues. La touche F8 permet elle aussi d’effectuer une sélection à l’aide des flèches de direction.

**Groupe** : à l’intérieur des bandeaux qui composent chaque onglet les commandes sont centralisées par groupe. Ainsi, dans l’onglet Accueil on trouve les groupes Presse-papier, Police, Alignement, Nombre, Style, Cellules et Édition.

**Info-bulle** : zone d'information qui apparaît au bout de quelques secondes lorsque la souris est passée sans cliquer sur les zones pour lesquelles cette fonctionnalité est disponible.

**Liste déroulante** : dans les boîtes de dialogue les options à choix multiples sont disponibles sous forme de listes dites déroulantes car elles se déroulent lorsque vous cliquez sur la flèche prévue à cet effet.
Si vous utilisez la touche tabulation pour vous déplacer d’une zone à une autre vous n’aurez pas besoin d’utiliser la souris. Lorsque le curseur se positionnera sur une liste déroulante vous pourrez l’ouvrir en tapant une lettre. Si vous tapez une lettre existant dans la liste l’option correspondante sera sélectionnée. Si vous tapez une lettre qui n’existe pas dans la liste elle s’ouvrira. Pour sélectionner une option il suffira de taper la première lettre ou de vous positionner dessus avec la flèche de direction « bas » pour qu’elle soit sélectionnée. La touche Entrée permet de valider sans utiliser la souris.

**Lien hypertexte** : un lien hypertexte permet d’accéder à l’aide d’un simple clic ou de la combinaison Ctrl+clic à un fichier, une partie de fichier, ou un site internet. Les liens hypertexte sont matérialisés dans un document, quel qu’il soit, par un texte souligné de couleur, souvent bleu. La couleur change lorsque le lien a déjà été utilisé mais elle reprendra sa couleur d’origine à la prochaine ouverture du document. Les liens hypertextes peuvent être utilisés pour accéder rapidement à un autre document dont la consultation peut s’avérer utile en cours de travail comme un texte de loi, un autre classeur, un site internet, etc.

**Onglet** : certaines fenêtres sont composées d'onglets comme ceux d’un intercalaire qui permettent d’avoir accès à d’autres commandes. Le ruban dans lequel se trouvent toutes les commandes est également composé d’onglets.

**Recopie incrémentée** : en positionnant la souris dans le coin inférieur droit d’une cellule et en la faisant glisser vers le bas ou la droite vous pouvez effectuer une recopie incrémentée. Un double clic sur le coin inférieur gauche produira le même effet pour une action vers le bas et c’est bien plus pratique.
Ruban : un ruban est composé d’onglets et les entrées de menu ont été remplacées par des icônes.

**Saisie** : entrée de données à l’aide du clavier.

**Saisie semi-automatique** : dans une liste déroulante, une cellule ou la barre d’adresse du navigateur internet des suggestions sont proposées au fur et à mesure que vous tapez votre texte. Regardez votre écran et non votre clavier pour profiter de cette fonctionnalité et vous éviter de continuer à taper du texte alors que la touche Tabulation ou Entrée peut le faire à votre place.
Il s’agit en principe de copier une donnée pour en faire une série mais on emploi également ce terme pour la copie d’une formule ou d’une donnée sans qu’il y ait nécessairement une série. C’est en fait l’action de copier en tirant la cellule contenant les données.
Après avoir effectué cette action un menu apparaît à droite de la dernière cellule. Il vous propose des options pour affecter les données ou les formats aux cellules qui viennent d’être alimentées par ce procédé.

**Sélection** : lorsque vous sélectionnez une cellule ou un objet il apparaît dans une couleur différente, il est mis en surbrillance. Ainsi, avant d'agir sur une zone assurez-vous qu'elle soit bien sélectionnée.

**Shift ou Maj** : la touche Shift ou Maj est celle qui permet de saisir une majuscule sans verrouiller la fonction majuscule. Ne pas la confondre avec la touche qui permet de verrouiller les majuscules sur laquelle figure un cadenas. Elle est en principe au-dessus de la touche Ctrl côté gauche. Elle est représentée avec une grosse flèche il y en a une seconde juste en face côté droit.

**Touche Entrée** : touche située à droite du clavier alphabétique. Il y en a une autre à droite du clavier numérique.

**Touche Tabulation** : touche située à gauche du clavier. Elle est en principe au-dessus de la touche qui sert à verrouiller les majuscules. Elle est représentée par deux petites flèches inversées, une dirigée vers la droite et une dirigée vers la gauche. Elle est très utile pour se déplacer de droite à gauche. Pour se déplacer vers la droite elle s’utilise seule et pour aller vers la gauche avec la touche Maj simultanément.

:::