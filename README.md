# doc.associationici.fr doc
Vuepress / netlify CMS documentation

## Deploy
[![Deploy to Netlify](https://www.netlify.com/img/deploy/button.svg)](https://app.netlify.com/start/deploy?repository=https://gitlab.com/association-ici1/documentation.git)

### Add Netlify Widget
1. Open Netlify Build and Deploy settings page.
1. Add Snippet injection
``` html
<script src="https://identity.netlify.com/v1/netlify-identity-widget.js"></script>
```

### Setup Netlify Identity
1. Open Netlify Identity Settings page.
1. Enable Identity
1. Select Registration preferences -> Invite only
1. In Services section, Enable Git Gateway

### Invite users
1. Open Identity tab in Netlify site page.
1. Push Invite users
1. Input email address and push 'Send' button.
1. Open invite email and set password.

## Development

```
# Install npm modules
npm install

# Start local dev server
npx vuepress dev docs
```

### Using CMS

To test edit pages using NetlifyCMS, `netlify-cms-proxy-server` must be running:
- `npx netlify-cms-proxy-server`

(this is used to access local filesystem as a remote Git repo using Netlify CMS). More info [here](https://www.netlifycms.org/docs/beta-features/#working-with-a-local-git-repository)

CMS (for content edition) is then accessible via `/admin`


## Contributing

Git flow is inspired by [gitlab-flow](https://about.gitlab.com/blog/2014/09/29/gitlab-flow/)

### Local git configuration

To keep Git history clear, please **configure Git** to [disable fast forward when merging](https://git-scm.com/docs/merge-config#Documentation/merge-config.txt-mergeff):

- in your `~/.gitconfig`
    ```ini
    [merge]
        # I like to keep Git history clear, so I will manually add "--ff" to merge options when I want to fast  forward
        ff = false
    ```

**note**: if you don't want to change your global conf, you can configure it only for this repo using Git [`includeIf` conf key](https://git-scm.com/docs/git-config#_conditional_includes) (only available in [Git > 2.13.0](https://github.blog/2017-05-10-git-2-13-has-been-released/))

### Git branches

- `main`: production branch
    - commits on this branch are deployed to production
- `netlify-dev` -> https://netlify-dev--ici-doc.netlify.com/
    - **dev instance** branch = used by developers to test instable features on Netlify instance.
    - every commits/merge on this branch will trigger a deployment to dev instance
- `netlify-preprod` -> https://netlify-preprod--ici-doc.netlify.com/
    - **preprod instance** branch = used to deploy stable features and let client validate
    - every commits/merge on this branch will trigger a deployment to preprod instance
